<?php
include("simple_html_dom.php");
class GA
{
	//dataset url: http://www.biomart.org/biomart/martservice?type=datasets&mart=ensembl
	var $genome_name = array(
		"human" => array("genus"=>"homo_sapiens", "biomart"=>"GRCh37", "ucsc"=>"hg19", "dataset"=>"hsapiens_gene_ensembl"),
		"mouse" => array("genus"=>"mus_musculus", "biomart"=>"GRCm38", "ucsc"=>"mm10", "dataset"=>"mmusculus_gene_ensembl"),
		"fruitfly" => array("genus"=>"drosophila_melanogaster", "biomart"=>"BDGP5", "ucsc"=>"dm3", "dataset"=>"dmelanogaster_gene_ensembl"),
		"nematode" => array("genus"=>"caenorhabditis_elegans", "biomart"=>"WBcel235", "ucsc"=>"ce10", "dataset"=>"celegans_gene_ensembl"),
		"zebrafish" => array("genus"=>"danio_rerio", "biomart"=>"Zv9", "ucsc"=>"danRer6", "dataset"=>"drerio_gene_ensembl"),
		
		"frog" => array("genus"=>"xenopus_tropicalis", "biomart"=>"JGI_4.2", "ucsc"=>"xenTro2", "dataset"=>"xtropicalis_gene_ensembl"),
		"rat" => array("genus"=>"rattus_norvegicus", "biomart"=>"Rnor_5.0", "ucsc"=>"rn4", "dataset"=>"rnorvegicus_gene_ensembl"),
		"pig" => array("genus"=>"sus_scrofa", "biomart"=>"Sscrofa10.2", "ucsc"=>"susScr2", "dataset"=>"sscrofa_gene_ensembl")
	);
	var $ucsc_genome_url = "http://hgdownload.cse.ucsc.edu/goldenPath";
	var $prefix = "";
	var $jbrowse_path = "/var/www/jb/visualizer/jbrowse";
	var $jbrowse_json_path = "/var/www/jb/visualizer/jbrowse/jbrowse_db/json";
	var $jbrowse_config_genome_path = "?data=jbrowse_db/json";
	function GA()
	{
		
	}
	function download($genome, $path)
	{
		mkdir($path);
		echo "Genome: $genome, PATH: $path\n";
		echo "Get UCSC Genome List...\n";
		$genome_list = $this->get_ucsc_chromosome_list($genome);
		$genome_prefix = $this->get_chromosome_prefix($genome_list);
		echo "Get UCSC Genome List Finish, prefix: $genome_prefix\n";
		foreach($genome_list as $list)
			echo "$list will be download\n";
		$this->download_ucsc_genome($genome, $genome_list, $path);
		
		echo "Merge fasta files...\n";
		$this->merge_fasta_gz($genome, $genome_list, $path);

		echo "Download Ensembl GTF Annotation...\n";
		$filename = $this->download_ensembl_gtf($genome, $path);
		echo "Create Ensembl Jbrowse GFF3 Annotation...\n";
		$this->ensembl_gtf_to_jb_gff3($filename, $path, $genome_prefix);
		
		echo "Download Ensembl Biomart Annotation (BED for pipeline)...\n";
		$anno_all = $this->download_biomart_annotation($genome, $genome_list, $genome_prefix, $path);
		echo "Annotation BED file size: " . strlen($anno_all)."\n";
		file_put_contents("$path/biomart.{$this->genome_name[$genome]['ucsc']}.bed", $anno_all);
		
		echo "Create Ensembl miRNA 3p5p Annotation...\n";
		$anno_mirna3p5p = $this->make_mirna_annotation_3p5p($anno_all);
		echo "Annotation miRNA 3p5p file size: " . strlen($anno_mirna3p5p)."\n";
		file_put_contents("$path/biomart.{$this->genome_name[$genome]['ucsc']}.miRNA-3p5p.bed", $anno_mirna3p5p);
		
		echo "Insert to Jbrowse\n";
		$this->insert_to_jbrowse($genome, $path);
		
		$this->reconfig_jbrowse_genome_setting($genome);
	}
	function reconfig_jbrowse_genome_setting($genome)
	{
		$config_file = "{$this->jbrowse_path}/jbrowse_conf.json";
		$config = file_get_contents($config_file);
		$config_json = json_decode($this->refine_json($config), true);
		
		$ucsc_genome_name = $this->genome_name[$genome]["ucsc"];
		if(!isset($config_json["datasets"][$ucsc_genome_name]))
		{
			$config_json["datasets"][$ucsc_genome_name]["url"] = "{$this->jbrowse_config_genome_path}/$ucsc_genome_name";
			$config_json["datasets"][$ucsc_genome_name]["name"] = "$ucsc_genome_name";
		}
		$config = $this->format_json(json_encode($config_json));
		file_put_contents($config_file, $config);
	}
	function insert_to_jbrowse($genome, $path)
	{
		$jb_path = $this->jbrowse_path;
		$jb_json_path = $this->jbrowse_json_path;
		
		$genus = $Genus = $this->genome_name[$genome]["genus"];
		$Genus[0] = strtoupper($Genus[0]); 
		$biomart = $this->genome_name[$genome]["biomart"];
		
		$ucsc_genome_name = $this->genome_name[$genome]["ucsc"];
		$n_jb_json_path = "$jb_json_path/$ucsc_genome_name";
		$fasta = "$path/$ucsc_genome_name.fa";
		$gff3 = "$path/$Genus.$biomart.75.gtf.gz.gff3";
		
		echo "Make JB $ucsc_genome_name Genome...\n";
		if(!is_dir("$n_jb_json_path/seq"))
		{
			$cmd = "cd $jb_path && bin/prepare-refseqs.pl --fasta $fasta --out $n_jb_json_path";
			shell_exec($cmd);
		}
		else
			echo "$n_jb_json_path/seq dir exists... continue\n";
		
		echo "Make JB $ucsc_genome_name Annotation...\n";
		if(!is_dir("$n_jb_json_path/tracks/Ensembl"))
		{
			$cmd = "cd $jb_path && bin/flatfile-to-json.pl --gff $gff3 --trackLabel Ensembl --key Ensembl --out $n_jb_json_path --type transcript";
			shell_exec($cmd);
		}
		else
			echo "$n_jb_json_path/tracks/Ensembl dir exists... continue\n";
		
		echo "Make JB $ucsc_genome_name Names...\n";
		if(!is_dir("$n_jb_json_path/names"))
		{
			$cmd = "cd $jb_path && bin/generate-names.pl -v --out $n_jb_json_path";
			shell_exec($cmd);
		}
		else
			echo "$n_jb_json_path/names dir exists... continue\n";
		
	}
	function merge_fasta_gz($genome, $lists, $path)
	{
		$ucsc_genome_name = $this->genome_name[$genome]["ucsc"];
		if(file_exists("$path/$ucsc_genome_name.fa"))
		{
			echo "$path/$ucsc_genome_name.fa exists ... Continue...\n";
			return;
		}
		$o_fp = fopen("$path/$ucsc_genome_name.fa", "w");
		foreach($lists as $list)
		{
			if(!file_exists("$path/$list"))
			{
				echo "Error! $path/$list not exists ... Continue...\n";
				continue;
			}
			if(ftell($o_fp) != 0)
			{
				fwrite($o_fp, "\n");
			}
			$fp = gzopen("$path/$list", "r");
			while(!gzeof($fp))
			{
				fwrite($o_fp, gzread($fp, 10000));
			}
			gzclose($fp);
		}
		fclose($o_fp);
	}
	function parse_gtf9(&$text, &$result)
	{
		$items = explode(";", $text);
		foreach($items as &$item)
		{
			$item = explode("\"", $item);
			if(count($item) != 3)
				continue;
			$result[ trim($item[0]) ] = trim($item[1]);
		}
	}
	function ensembl_gtf_to_jb_gff3($filename, $path, $prefix="chr")
	{
		if(file_exists($path."/".$filename.".gff3"))
		{
			echo $path."/".$filename.".gff3 exists continue\n";
			return;
		}
		echo "Create GFF3 file ".$path."/".$filename.".gff3\n";
		$o_fp = fopen($path."/".$filename.".gff3", "w");
		$fp = gzopen($path."/".$filename, "r");
		$i=0;
		while(!gzeof($fp))
		{
			$line = gzgets($fp);
			if($line == "" || $line[0] == "#")
				continue;
			$line = explode("\t", trim($line));
			$tag9 = array();
			$new_tag9 = array();
			$this->parse_gtf9($line[8], $tag9);
			// ==========Prepare========== //
			
			$line[0] = $prefix.$line[0];
			
			$type = $line[2];
			switch($type)
			{
				case "gene":
					$new_tag9["ID"] = $c_gene_id = $tag9["gene_id"];
					$new_tag9["Name"] = $tag9["gene_name"];
					$new_tag9["Note"] = $tag9["gene_biotype"];
					break;
				case "transcript":
				case "mRNA":
					$new_tag9["ID"] = $c_mRNA_id = $tag9["transcript_id"];
					$new_tag9["Name"] = $tag9["transcript_name"];
					$new_tag9["Parent"] = $c_gene_id;
					$new_tag9["Note"] = $tag9["gene_biotype"];
					break;
				case "exon":
					//$new_tag9["ID"] = $c_exon_id = $tag9["exon_id"];
					$new_tag9["Parent"] = $c_mRNA_id;
					break;
				case "CDS":
					//$new_tag9["ID"] = $c_protein_id = $tag9["protein_id"];
					$new_tag9["Parent"] = $c_mRNA_id;
					$cds_mid = ($line[3]+$line[4])/2;
					break;
				case "UTR":
					$new_tag9["Parent"] = $c_mRNA_id;
					$utr_mid = ($line[3]+$line[4])/2;
					if($line[6] == "+")
						$a = 1;
					else
						$a = -1;
					if($utr_mid*$a < $cds_mid*$a)
						$line[2] = "five_prime_UTR";
					else
						$line[2] = "three_prime_UTR";
					break;
				default:
					$new_tag9["Parent"] = $c_mRNA_id;
					break;
			}
			
			$line[8] = "";
			foreach($new_tag9 as $k=>$v)
				$line[8] .= "$k=$v;";
			$line = implode("\t", $line) . "\n";
			fputs($o_fp, $line);

		}
		
		gzclose($fp);
		fclose($o_fp);
	}
	function download_ensembl_gtf($genome, $path)
	{
		$genus = $Genus = $this->genome_name[$genome]["genus"];
		$Genus[0] = strtoupper($Genus[0]); 
		$biomart = $this->genome_name[$genome]["biomart"];
		
		$url = "ftp://ftp.ensembl.org/pub/release-75/gtf/$genus/$Genus.$biomart.75.gtf.gz";
		echo "Downloading $Genus.$biomart.75.gtf.gz\n";
		shell_exec("cd $path && lftp -c \"pget -n 10  $url\"");
		return "$Genus.$biomart.75.gtf.gz";
	}
	function download_biomart_annotation($genome, $genome_list, $prefix, $path, $limit="-1")
	{
		$this->buffer = "";
		$dataset = $this->genome_name[$genome]["dataset"];
		$url = "http://central.biomart.org/martservice/results";
		
		foreach($genome_list as &$list)
		{
			$list = substr($list, strlen($prefix), strlen($list)-strlen($prefix)-6);
		}
		$chr_list = implode(",", $genome_list);
		$query = '<!DOCTYPE Query>
			<Query client="true" processor="TSV" limit="'.$limit.'" header="0">
				<Dataset name="'.$dataset.'" config="gene_ensembl_config">
					<Filter name="chromosome_name" value="'.$chr_list.'" filter_list=""/>
					<Filter name="status" value="KNOWN" filter_list=""/>
					<Filter name="transcript_status" value="KNOWN" filter_list=""/>
					<Attribute name="chromosome_name"/>
					<Attribute name="start_position"/>
					<Attribute name="end_position"/>
					<Attribute name="strand"/>
					<Attribute name="gene_biotype"/>
					<Attribute name="external_gene_id"/>
					<Attribute name="source"/>
				</Dataset>
			</Query>';//<Attribute name="transcript_biotype"/> <Attribute name="status"/>
		$post = array(
			"stream" => true,
			"iframe" => true,
			"uuid" => "results",
			"scope" => "biomart.datasource",
			"query" => $query
		);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
		$result = curl_exec($ch);
		curl_close($ch);
		$result = $this->handler_annotation_data($result, $prefix);
		return $result;
	}
	function handler_annotation_data(&$result, $prefix)
	{
		$results = explode("\n", $result);
		foreach($results as &$line)
		{
			if($line == "") continue;
			$line = explode("\t", $line);
			$line[0] = $prefix . $line[0];
			$line[3] = $line[3]==1?"+":"-";
			$line = implode("\t", $line);
		}
		return implode("\n", $results);
	}
	function make_mirna_annotation_3p5p(&$text)
	{
		$lines = explode("\n", $text);
		$result = "";
		foreach($lines as $line)
		{
			if($line == "")
				continue;
			$line = explode("\t", $line);
			
			if( !($line[4] == "miRNA" || $line[4] == "pre_miRNA" ))
				continue;
				
			$len = ($line[2]-$line[1]+1);
			if($len < 500)
			{
				$mid = (int)($len/2) + $line[1];
				if($line[3] == "+")
				{
					$result .= "$line[0]\t$line[1]\t".($mid-1)."\t$line[3]\t$line[4]-5p\t$line[5]\n";
					$result .= "$line[0]\t$mid\t$line[2]\t$line[3]\t$line[4]-3p\t$line[5]\n";
				}
				else
				{
					$result .= "$line[0]\t$line[1]\t".($mid)."\t$line[3]\t$line[4]-3p\t$line[5]\n";
		            $result .= "$line[0]\t".($mid+1)."\t$line[2]\t$line[3]\t$line[4]-5p\t$line[5]\n";
				}
			}
		}
		return $result;
	}
	function download_ucsc_genome($genome, $lists, $path)
	{
		$ucsc_genome_name = $this->genome_name[$genome]["ucsc"];
		$url = $this->ucsc_genome_url."/$ucsc_genome_name/chromosomes";
		
		foreach($lists as $list)
		{
			if(file_exists("$path/$list"))
			{
				echo "$list Exists conintue\n";
				continue;
			}
			echo "Downloading $list\n";
			shell_exec("cd $path && lftp -c \"pget -n 10  http://hgdownload.soe.ucsc.edu/goldenPath/$ucsc_genome_name/chromosomes/$list\"");
		}
	}
	function get_ucsc_chromosome_list($genome)
	{
		$result = array();
		$ucsc_genome_name = $this->genome_name[$genome]["ucsc"];
		$url = $this->ucsc_genome_url."/$ucsc_genome_name/chromosomes";
		$html = file_get_html($url);
		foreach( $html->find("a") as $link)
		{
			$names = explode(".", $link->href);
			if(count($names) != 3)
				continue;
			$result[] = $link->href;
		}
		$result = $this->filter_ucsc_chromosome_list($result);
		return $result;
	}
	function get_chromosome_prefix(&$lists)
	{
		$prefix_array = array();
		$prefix="";
		for($i=1; $i<count($lists); $i++)
		{
			$pre_list = $lists[$i-1];
			$list = $lists[$i];
			$prefix = "";
			for($w=0; $w < strlen($list) && $w < strlen($pre_list); $w++)
			{
				if($list[$w] == $pre_list[$w])
				{
					$prefix .= $list[$w];
				}
				else
				{
					break;
				}
				if(strlen($prefix) > 0)
				{
					@$prefix_array[$prefix] ++;
				}
			}
			
		}
		arsort($prefix_array);
		$prefix = "";
		$n = current($prefix_array);
		foreach($prefix_array as $k => $v)
		{
			if($v != $n)
				break;
			if(strlen($k) > strlen($prefix))
				$prefix = $k;
		}
		$this->prefix = $prefix;
		return $prefix;
	}
	function filter_ucsc_chromosome_list(&$lists)
	{
		$result = array();
		$prefix = $this->get_chromosome_prefix($lists);
		foreach($lists as $list)
		{
			if(strstr($list, "_"))
				continue;
			if(substr($list, 0, strlen($prefix) ) == $prefix)
				$result[] = $list;
		}
		return $result;
	}
	
	private function refine_json($content)
	{
		$content = explode("\n", $content);
		foreach($content as &$line)
		{
			$is_comm = false;
			for($i=0;$i<strlen($line);$i++)
			{
				if($line[$i] == "/" && $line[$i+1] == "/")
					$is_comm = true;
				if($is_comm)
					$line[$i] = " ";
			}
		}
		$content = implode("\n", $content);
		return $content;
	}
	private function format_json($text)
	{
		$new_text = "";
		$p = 0;
		$is_text = false;
		for($i=0; $i < strlen($text); $i++)
		{
			
			$c = $text[$i];
			
			if($c == "\\")
			{
				$new_text .= $c.$text[++$i];
			}
			else if($c == '"' || $is_text)
			{
				if($c == '"' && $is_text == true)
					$is_text = false;
				else if($c == '"' && $is_text == false)
					$is_text = true;
				$new_text .= $c;
			}
			else if($c == "{" || $c == "[")
			{
				//$new_text .= "\n";
				//for($j=0; $j < $p; $j++) $new_text .= "\t";
				$p++;
				$new_text .= "$c\n";
				for($j=0; $j < $p; $j++) $new_text .= "\t";
				
			}
			else if($c == "}" || $c == "]")
			{
				$p--;
				$new_text .= "\n";
				for($j=0; $j < $p; $j++) $new_text .= "\t";
				$new_text .= "$c";
				
			}
			else if($c == ",")
			{
				$new_text .= "$c\n";
				for($j=0; $j < $p; $j++) $new_text .= "\t";
			}
			else
			{
				//for($j=0; $j < $p; $j++)
				//	$new_text .= "\t";
				$new_text .= $c;
			}
		}
		return $new_text;
	}
};

//if($argc != 3)
//{
//	die("args error!\n");	
//}
$ga = new GA();

//$ga->download($argv[1], $argv[2]);

//$ga->download("human", "/mnt/godzilla/ANNOTATION/ensembl/hg19");
//$ga->download("mouse", "/mnt/godzilla/ANNOTATION/ensembl/mm10");
$ga->download("fruitfly", "/mnt/godzilla/ANNOTATION/ensembl/dm3");
$ga->download("nematode", "/mnt/godzilla/ANNOTATION/ensembl/ce10");
//$ga->download("zebrafish", "/mnt/godzilla/ANNOTATION/ensembl/danRer6");




?>