<?php
include("analysis_handler.php");


$FIELD = array();
///@brief for post method and cmd method
if(isset($_POST["cmd"]))
{
	$FIELD = $_POST;
}
elseif($argc > 1)
{
	// php this_file.php cmd=something value=something
	for($i=1;$i<$argc;$i++)
	{
		$tmp_arg = explode("=" , $argv[$i]);
		$FIELD[$tmp_arg[0]] = $tmp_arg[1];
	}
}

$csaltdock_controller = new Csaltdock_controller($FIELD);


class Csaltdock_controller
{
	// 4 for message, 3 for console log, 2 for warning, 1 for error (die), 0 for slice
	var $debug_level = 1;
	var $OUTPUT_PATH = "../output";
	var $FIELD = array();
	var $CONFIG = array(
		"jbrowse_config_file" => "jbrowse/jbrowse_conf.json"
		,"jbrowse_bookmarks_dir" => "bookmarks"
	);
	
	function Csaltdock_controller($field)
	{
		$this->FIELD = $field;
		$this->check_field();
		$this->controller();
	}
	function controller()
	{
		$method = $this->FIELD["cmd"];
		if(! method_exists($this, $method))
		{
			$this->debug("No CMD method '$method' exist.");
		}
		$this->$method();
	}
	public function get_analysis_table_list()
	{
		//php csaltdock-visualizer.php cmd=get_analysis_table_list ana_type=analysis genome=mm10
		if(! isset($this->FIELD["ana_type"]))
		{
			$this->debug("No analysis type.");
		}
		if(! isset($this->FIELD["genome"]))
		{
			$this->debug("No genome parameter.");
		}
		$ana_type = $this->FIELD["ana_type"];
		$genome = $this->FIELD["genome"];
		$AH = new AnalysisHandler($this->OUTPUT_PATH);
		$AH->get_table_json($genome, $ana_type);
	}
	public function get_table()
	{
		if(! isset($this->FIELD["filename"]))
		{
			$this->debug("No table filename type.");
		}
		$filename = $this->FIELD["filename"];
		$AH = new AnalysisHandler($this->OUTPUT_PATH);
		$table = $AH->get_table_text($filename);
		$table = $AH->remove_column($table, array("SUM_ANNO", "SUM_SEQ", "SUM_LEN"), ", ");
		$table = $AH->remove_raw($table, array("SUM_ANNO", "SUM_SEQ", "SUM_LEN"), ", ");
		echo $table;
	}
	public function get_jbrowse_config()
	{
		$file_path = $this->get_file_path($this->CONFIG["jbrowse_config_file"]);
		$content = file_get_contents($file_path);
		///@brief jbrowse config json 中會有 "//" 註解符號，會讓 js or php 無法解析
		$content = $this->refine_json($content);
		echo $content;
	}
	public function get_sample_list()
	{
		if(! isset($this->FIELD["genome"]))
		{
			$this->debug("No genome parameter.");
		}
		$genome = $this->FIELD["genome"];
		$sample_list = $this->get_dir_content("$this->OUTPUT_PATH/$genome");
		echo json_encode($sample_list);
	}
	public function jb_save_bookmarks()
	{
		if(! isset($this->FIELD["genome"]))
		{
			$this->debug("No genome value.");
		}
		if(! isset($this->FIELD["data"]))
		{
			$this->debug("No data value.");
		}
		$genome = $this->FIELD["genome"];
		$data = $this->FIELD["data"];
		$file_path = $this->get_file_path($this->CONFIG["jbrowse_bookmarks_dir"]) . "/$genome.bookmarks";
		echo $file_path;
		file_put_contents($file_path, $data);
	}
	public function jb_load_bookmarks()
	{
		if(! isset($this->FIELD["genome"]))
		{
			$this->debug("No genome value.");
		}
		$genome = $this->FIELD["genome"];
		
		$file_path = $this->get_file_path($this->CONFIG["jbrowse_bookmarks_dir"]) . "/$genome.bookmarks";
		if(!file_exists($file_path))
			die('{"idx":0, "data":{}}');
		echo file_get_contents($file_path, $data);
	}
	public function add_output_tracks()
	{
		if(! isset($this->FIELD["genome"]))
		{
			$this->debug("No genome");
		}
		$genome =$this->FIELD["genome"];
		
		$oh = new OutputHandler($this->OUTPUT_PATH);
		$oh->jbrowse_db = "/var/www/jb/visualizer/jbrowse/jbrowse_db";
		//$sample = $oh->get_dir_content($this->OUTPUT_PATH);
		$oh->add_bam_to_track($genome);


	}
	private function refine_json($content)
	{
		$content = explode("\n", $content);
		foreach($content as &$line)
		{
			$is_comm = false;
			for($i=0;$i<strlen($line);$i++)
			{
				if($line[$i] == "/" && $line[$i+1] == "/")
					$is_comm = true;
				if($is_comm)
					$line[$i] = " ";
			}
		}
		$content = implode("\n", $content);
		return $content;
	}
	private function get_file_path($path)
	{
		return dirname(__FILE__) . "/../" . $path;
	}
	private function check_field()
	{
		if(count($this->FIELD) == 0)
		{
			$this->debug("No cmds.");
		}
		if(! isset($this->FIELD["cmd"]))
		{
			$this->debug("No CMD control.");
		}
	}
	private function debug($msg, $title="debug", $level=1)
	{
		if($level > $this->debug_level)
			return;
		echo "[CsaltVisualizer][$title][$level] - $msg\n";
		if($level == 1)
			die();
	}
	private function get_dir_content($dir)
	{
		$result = array();
		$d = dir($dir);
		while (false !== ($entry = $d->read())) {
			if($entry == "." || $entry == "..")
				continue;
			$result[] = $entry;
		}
		$d->close();
		return $result;
	}
}



?>