<?php
include("output_handler.php");

class AnalysisHandler extends OutputHandler
{
	var $analysis_table = array();
	
	function AnalysisHandler($output_path)
	{
		$this->output_path = $output_path;
	}
	function add_rec_array(&$array, $list, $idx)
	{
		if($idx >= count($list)-1)
			return;
		if(!isset($array[$list[$idx]]))
			$array[$list[$idx]] = array();
		$this->add_rec_array($array[$list[$idx]], $list, $idx+1);
	}
	function make_analysis_table($genome, $ana_type = "analysis")
	{
		$this->genome = $genome;
		$sample_list = $this->get_sample_list();
		foreach($sample_list as $sample)
		{
			$this->analysis_table[$sample] = array();
			$file_list = $this->get_file_list_from_sample($sample, $ana_type);
			sort($file_list);
			foreach($file_list as $filename)
			{
				//Table.lendist.Tail.prefix.GMPM.miRNA.detail.2.1.0.0.45.LEN.ANNOvsSEQ.ppm.tsv
				//  0      1     2      3    4     5     6    7 8 9 10 11 12  13   14  15  16
				$file = explode(".", $filename);
				
				// $file = Table.lendist.Tail.prefix.GMPM.miRNA.detail.2.1.0.0
				// $tmp = 45.LEN.ANNOvsSEQ.ppm.tsv
				//$tmp = array_splice($file, 11);
				
				//$file[] = $tmp[1];
				//$file[] = $tmp[2];
				//$file[] = $tmp[0];
				//$file[] = $tmp[3];
				//$file[] = $tmp[4];
				
				$this->add_rec_array($this->analysis_table[$sample], $file, 0);
			}
		}
	}
	function get_table_json($genome, $ana_type = "analysis")
	{
		$tmp_file = "tmp/$genome.tmp_table_$ana_type.json";
		if(file_exists($tmp_file))
		{
		//	echo file_get_contents($tmp_file);
		//	return;
		}
		$this->make_analysis_table($genome, $ana_type);
		
		$text = json_encode($this->analysis_table, true);
		file_put_contents($tmp_file, $text);
		echo $text;
	}	
	function remove_column($text, $field_name, $di = "\t")
	{
		$new_text = "";
		$texts = explode("\n", $text);
		$field_idx = 9999;
		for($i=0;$i<count($texts); $i++)
		{
			$line = explode($di, $texts[$i]);
			if($i==0)
			{
				for($j=0;$j<count($line);$j++)
				{
					if( array_search($line[$j], $field_name) !== FALSE)
					{
						$field_idx = $j;
						break;
					}
				}
				if($field_idx == 9999)
					return $text;
				else
				{
					array_splice($line, $field_idx, 1);
					$new_text .= implode($di, $line)."\n";
				}
			}
			else
			{
				array_splice($line, $field_idx, 1);
				$new_text .= implode($di, $line)."\n";
			}
		}
		return substr($new_text, 0, -2);
	}
	function remove_raw($text, $field_name, $di = "\t")
	{
		$new_text = "";
		$texts = explode("\n", $text);
		for($i=0;$i<count($texts); $i++)
		{
			$line = explode($di, $texts[$i]);
			if( array_search($line[0], $field_name) !== FALSE)
				continue;
			$new_text .= implode($di, $line)."\n";
		}
		return substr($new_text, 0, -2);
	}
	function get_table_text($filename)
	{
		$text = "";
		$table_path = "{$this->output_path}/$filename";
		$contents = FILE($table_path);
		for($i=3;$i<count($contents)-1;$i++)
		{
			if($contents[$i][strlen($contents[$i])-2] == "\t")
				$contents[$i] = substr($contents[$i], 0, -2)."\n";
			$text .= str_replace("\t", ", ", $contents[$i]);
		}	
		return $text;
	}
}
/*
$AH = new AnalysisHandler("../output");

if(isset($_POST["filename"]) || isset($_GET["filename"]))
{
	$filename = $_POST["filename"];
	$table = $AH->get_table_text($filename);
	//echo $table;
	$table = $AH->remove_column($table, array("SUM_ANNO", "SUM_SEQ", "SUM_LEN"), ", ");
	//echo $table;
	$table = $AH->remove_raw($table, array("SUM_ANNO", "SUM_SEQ", "SUM_LEN"), ", ");
	echo $table;
}
else
{
	$AH->get_table_json();
}
*/
?>