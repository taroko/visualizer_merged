<?php
class Jbrowse_track_manager
{
	var $json_file = NULL;
	var $json_encode = NULL;
	var $json_decode = NULL;
	var $tracks = NULL;
	
	
	function Jbrowse_track_manager($track_json_file)
	{
		$this->json_encode = file_get_contents($track_json_file);
		$this->json_decode = json_decode($this->json_encode, true);
		$this->tracks = $this->json_decode["tracks"];
		$this->json_file = $track_json_file;
		$this->backup_json();
	}
	function add_faced_track_selector()
	{
		$config = array();
		$config["type"] = "Faceted";
		$config["displayColumns"] = array("key", "sample", "track_type","file_type", "filter", "anno_db", "annotation", "description");
		$config["selectableFacets"] = array("sample", "track_type", "file_type", "filter", "anno_db", "annotation");
		$config["renameFacets"] = array("key" => "Name");
		$this->json_decode["trackSelector"] = $config;
		
	}
	function add_file_list($fpath, $tpath, $sample)
	{
		$d = dir($fpath);
		while (false !== ($entry = $d->read()))
		{
			if(strstr($entry, ".bai"))
			{
				continue;
			}
			elseif(strstr($entry, ".bam"))
			{
				$key_name = basename($entry, ".bam");
				
				$track_type = "bam_coverage";
				$label_name = "{$sample}-{$key_name}-{$track_type}";
				$description = $label_name;
				
				$this->add_track($track_type, $sample, $key_name, $label_name, $description, "$tpath/$entry" );
				
				$track_type = "bam_alignment";
				$label_name = "{$sample}-{$key_name}-{$track_type}";
				$description = $label_name;
				
				$this->add_track($track_type, $sample, $key_name, $label_name, $description, "$tpath/$entry" );
			}
			elseif(strstr($entry, ".bwg"))
			{
				$key_name = basename($entry, ".bwg");
				
				$track_type = "bwg_xyplot";
				$label_name = "{$sample}-{$key_name}-{$track_type}";
				$description = $label_name;
				
				$this->add_track($track_type, $sample, $key_name, $label_name, $description, "$tpath/$entry" );
				
				$track_type = "bwg_density";
				$label_name = "{$sample}-{$key_name}-{$track_type}";
				$description = $label_name;
				$this->add_track($track_type, $sample, $key_name, $label_name, $description, "$tpath/$entry" );
			}
		}
	}
	function is_exist_track($label, &$config)
	{
		foreach($this->tracks as &$track)
		{
			if($track["label"] == $label)
			{
				$track = $config;
				return true;
			}
		}
		return false;
	}
	function handle_metadata($key_name, &$metadata)
	{
		$name = explode(".", $key_name);
		
		// filter
		switch($name[2])
		{
			case 0:
				$name[2] = "non-filtered";
				break;
			case 1:
				$name[2] = "filtered";
				break;
			case -1:
				$name[2] = "total";
				break;
		}
		
		// anno_db 
		switch($name[3])
		{
			case 0:
				$name[3] = "ensemble";
				break;
			case 1:
				$name[3] = "ensemble mirna";
				break;
		}
		
		// annotation
		switch($name[5])
		{
			case "_":
				$name[5] = "total";
				break;
		}
		$metadata["file_type"] = $name[0];
		$metadata["sys"] = $name[1];
		$metadata["filter"] = $name[2];
		$metadata["anno_db"] = $name[3];
		$metadata["depth"] = $name[4];
		$metadata["annotation"] = $name[5];
	}
	function add_track($type, $sample, $key_name, $label_name, $description, $filename, $filename2 = NULL)
	{
		echo "Add track config: $key_name ";
		
		
		
		$config = array(
			 "key"           => $label_name
			,"label"         => $label_name
			,"urlTemplate"   => $filename
			,"metadata"      => array(
				 "sample"    => $sample
				,"track_type"  => $type
				,"description" => $description
			)
		);
		$this->handle_metadata($key_name, $config["metadata"]);
		if($filename2 != NULL)
		{
			$config["histograms"]["storeClass"] = "JBrowse/Store/SeqFeature/BigWig";
			$config["histograms"]["urlTemplate"] = $filename2;
		}
		$typename = "add_$type";
		$config = $this->$typename($config);
		
		if($this->is_exist_track($label_name, $config))
		{
			echo ", But this track have been existed, OVERRIDE... \n";
			return;
		}
		echo "\n";
		$this->tracks[] = $config;
	}
	function backup_json()
	{
		$backup_filename = basename($this->json_file) ."_". date("Y-m-d-H:i:s");
		copy($this->json_file, dirname($this->json_file)."/".$backup_filename);
	}
	function save_json()
	{
		$this->json_decode["tracks"] = $this->tracks;
		//echo json_encode($this->json_decode). "\n\n";
		//echo $ll = $this->format_json( json_encode($this->json_decode) ) . "\n\n";
		file_put_contents($this->json_file, $this->format_json( json_encode($this->json_decode) ) );
		//file_put_contents($this->json_file, json_encode($this->json_decode) );
	}
	function delete_by_key($key_name)
	{
		$delete_array = array();
		foreach($this->tracks as $idx=>$config)
		{
			if($config["key"] == $key_name)
			{
				echo "Delete: ".$config["key"]."\n";
				$delete_array[] = $idx;
			}
		}
		foreach ($delete_array as $idx)
		{
			unset($this->tracks[$idx]);
		}
	}
	function add_bam_coverage($config)
	{
		$config["storeClass"] = "JBrowse/Store/SeqFeature/BAM";
		$config["type"] = "JBrowse/View/Track/SNPCoverage";
		return $config;
	}
	function add_bam_alignment($config)
	{
		$config["storeClass"] = "JBrowse/Store/SeqFeature/BAM";
		$config["type"] = "JBrowse/View/Track/Alignments2";
		return $config;
	}
	function add_bwg_xyplot($config)
	{
		$config["storeClass"] = "JBrowse/Store/BigWig";
		$config["type"] = "JBrowse/View/Track/Wiggle/XYPlot";
		$config["min_score"] = 0;
		$config["autoscale"] = true;
		$config["variance_band"] = true;
		return $config;
	}
	function add_bwg_density($config)
	{
		$config["storeClass"] = "JBrowse/Store/BigWig";
		$config["type"] = "JBrowse/View/Track/Wiggle/Density";
		return $config;
	}
	function format_json($text)
	{
		$new_text = "";
		$p = 0;
		$is_text = false;
		for($i=0; $i < strlen($text); $i++)
		{
			
			$c = $text[$i];
			
			if($c == "\\")
			{
				$new_text .= $c.$text[++$i];
			}
			else if($c == '"' || $is_text)
			{
				if($c == '"' && $is_text == true)
					$is_text = false;
				else if($c == '"' && $is_text == false)
					$is_text = true;
				$new_text .= $c;
			}
			else if($c == "{" || $c == "[")
			{
				//$new_text .= "\n";
				//for($j=0; $j < $p; $j++) $new_text .= "\t";
				$p++;
				$new_text .= "$c\n";
				for($j=0; $j < $p; $j++) $new_text .= "\t";
				
			}
			else if($c == "}" || $c == "]")
			{
				$p--;
				$new_text .= "\n";
				for($j=0; $j < $p; $j++) $new_text .= "\t";
				$new_text .= "$c";
				
			}
			else if($c == ",")
			{
				$new_text .= "$c\n";
				for($j=0; $j < $p; $j++) $new_text .= "\t";
			}
			else
			{
				//for($j=0; $j < $p; $j++)
				//	$new_text .= "\t";
				$new_text .= $c;
			}
		}
		return $new_text;
	}
	function __destruct()
	{
		$this->save_json();
	}
};



//$track_json_file = "/var/www/jb/visualizer/jbrowse/jbrowse_db/json/mm9/trackList.json";
//$jt = new Jbrowse_track($track_json_file);

//$jt->add_file_list("/var/www/jb/visualizer/jbrowse/jbrowse_db/raw/mm9/sample/bam", "../../raw/mm9/sample/bam", "sample");

//$type, $category, $key_name, $description, $filename, $filename2
//$jt->add_track("bam_coverage", "Ctest", "bam_coverage_1", "bam_aligment_1", "desc test", "../../raw/mm9/sample/tmp.bam" );
//$jt->add_track("bam_alignment", "Ctest", "bam_aligment_2", "bam_aligment_2", "desc test", "../../raw/mm9/sample/tmp.bam" );
//$jt->add_track("bwg_xyplot", "Ctest", "ssss", "desc test", "desc test", "../../raw/mm9/sample/tmp.bwg" );
//$jt->add_track("bwg_density", "Ctest", "ssss", "desc test", "desc test", "../../raw/mm9/sample/tmp.bwg" );
//$jt->delete_by_key("bam_coverage_1");

/*
{
   "tracks" : [

   ],
   "names" : {
      "url" : "names/",
      "type" : "Hash"
   },
   "formatVersion" : 1
}
*/

/* tracks.conf

[trackSelector]
renameFacets.key = Name
type=Faceted
displayColumns =
 + key
 + sample
 + track_type
 + file_type
 + filter
 + anno_db
 + annotation
 + description
selectableFacets = 
 + sample
 + track_type
 + file_type
 + filter
 + anno_db
 + annotation

[tracks . Genome]
storeClass = JBrowse/Store/Sequence/StaticChunked
chunkSize = 20000
urlTemplate = seq/{refseq_dirpath}/{refseq}-
label = DNA
key=Genome
type = SequenceTrack
category = Reference sequence
metadata.Name=DNA

[tracks . Ensembl]
autocomplete=all
style.className=feature
key=Ensembl
storeClass=JBrowse/Store/SeqFeature/NCList
urlTemplate=tracks/Ensembl/{refseq}/trackData.json
compress=0
label=Ensembl
type=FeatureTrack
metadata.Name=Ensembl
*/

?>
