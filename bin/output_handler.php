<?php
include("jbrowse_track_manager.php");


class OutputHandler
{
	var $debug = true;
	var $output_path = "";
	var $genome = "mm9";
	var $jbrowse_db = "/var/www/jb/visualizer/jbrowse/jbrowse_db";
	var $track_config_json = ""; // /var/www/jb/visualizer/jbrowse/jbrowse_db/json/mm9/trackList.json
	
	function OutputHandler($output_path="../output")
	{
		$this->output_path = $output_path;
	}
	
	function get_sample_list()
	{
		return $this->get_dir_content($this->output_path . "/" . $this->genome);
	}
	
	///@brief $type maybe "analysis", "bam", "bwg", "heterogeneity", "mappability"
	function get_file_list_from_sample($sample, $type)
	{
		return $this->get_dir_content("{$this->output_path}/{$this->genome}/{$sample}/{$type}");
	}
	
	///@breif add link to jbrowse_db, in order to let jbrowse can read bam file or bwg file
	function add_output_link($sample, $target)
	{
		$source_sample_path = dirname(__FILE__)."/{$this->output_path}/{$this->genome}/$sample";
		if( is_dir($target) )
			return true;
		if($this->debug) echo "make link: $source_sample_path -> $target\n";
		symlink($source_sample_path, $target);
	}
	
	///@brief add bam and bwg to jbrowse track list
	function add_bam_to_track($genome)
	{
		$this->genome = $genome;
		$this->track_config_json = "{$this->jbrowse_db}/json/{$this->genome}/trackList.json";
		
		$jt = new Jbrowse_track_manager($this->track_config_json);
		//$jt->add_faced_track_selector();
		
		$sample_list = $this->get_sample_list($genome);
		
		foreach($sample_list as $sample)
		{
			$target_path = "{$this->jbrowse_db}/raw/{$this->genome}/{$sample}";
			$this->add_output_link($sample, $target_path);
			if($this->debug) echo "make bam track {$sample}\n";
			//bam to track
			$jt->add_file_list("{$target_path}/bam", "../../raw/$this->genome/{$sample}/bam", "{$sample}");
			
			if($this->debug) echo "make bwg track {$sample}\n";
			//bwg to track
			$jt->add_file_list("{$target_path}/bwg", "../../raw/$this->genome/{$sample}/bwg", "{$sample}");	
		}
	}
	
	
	function get_dir_content($dir)
	{
		$result = array();
		$d = dir($dir);
		while (false !== ($entry = $d->read())) {
			if($entry == "." || $entry == "..")
				continue;
			$result[] = $entry;
		}
		$d->close();
		return $result;
	}
}

//$OH = new OutputHandler("../output");

/* test */
/*
print_r( $OH->get_sample_list() );
print_r( $OH->get_file_list_from_sample("sample-A", "bam") );
*/

//$OH->add_bam_to_track();

?>