function resizeall() {
	spreadsheet.DoOnResize();
}

// ------------------------- SocialCalc --------------------
function DoEdit(s, t)
{
	cs.sc.do_edit(s, t);
}

function DoFormat(s, t)
{
	cs.sc.do_format(s, t);
}


$().ready(function() {
	require(["cxdlib/libconsole", "cxdlib/libjb", "cxdlib/libfmt", "cxdlib/libjob", "cxdlib/libjque", "d3_graph/js/d3", "cxdlib/libmenu", "cxdlib/libcontrol_panel", "cxdlib/libtheme", "cxdlib/libd3controler", "cxdlib/libanalyzer", "cxdlib/libfileio", "cxdlib/libd2iterator", "cxdlib/libsccontroller", "cxdlib/libjobcontroller"], function(Console, JB, Format, Job, JobQueue, D3, Menu, CP, Theme, D3Controler, Analyzer, FileIO, D2Iterator, SCController, JobController) {
		
		///@brief 製作預設基本 menu
		var menu = new Menu($("#menu"));
		
		///@brief 設定點選某些dialog外的物件，dialog視窗不消失 （原本點擊dialog外面的地方，dialog會消失）
		menu.dialog_dont_close_obj = {"header-control":"Control Panel", "jobQ":"New Job"};
		
		///@brief grobal control panel
		///@brief ControlPanel 為製作 control panel json (在menu裡面) 的class
		control_panel = new ControlPanel();
		
		menu.create_menu( $("#control_panel_general"), control_panel.make_control_panel_config());
		
		var theme = new Theme();
		
		cs = new Console();
		cs.ez_windowize();
		cs.menu = menu;


		cs.fileio = new FileIO();
	
		var jb = new JB("jBrowseContainer");
		cs.jb = jb;
		jb.open();
	
		fmt = new Format();
		cs.fmt = fmt;
		cs.jobq = new JobQueue();
		cs.jobq.init_ui();
		
		cs.sc = new SCController("SocialCalcContainer");
		spreadsheet = cs.sc.spreadsheet;
		cs.sc.open();

		cs.d3 = new D3Controler();
		
		cs.analyzer = new Analyzer();

		cs.jobs = new JobController();
		cs.sp = new Snapshot(cs, "snapshot_list", cs.global_config.current_genome);

		cs.schedule_snapshot_routine();

		cs.jb.set_title(cs.global_config.current_genome);
	});
});
