// TODO: Set the size using by resize to Div instead of Window
//		 Store the original version into the GetViewportInfoOld
SocialCalc.GetViewportInfoOld = SocialCalc.GetViewportInfo;
SocialCalc.GetViewportInfo = function () {

	var result = {};
	var target = spreadsheet.parentNode;

	if (target.parentNode.tagName != "DIV") {
		result = SocialCalc.GetViewportInfoOld();
		return result;
	}

	if (window.innerWidth) { // all but IE
		result.width = parseFloat(target.style.width) + 9;
		result.height = parseFloat(target.style.height) + 9;
		result.horizontalScroll = window.pageXOffset;
		result.verticalScroll = window.pageYOffset;
	}
	else {
		/* Skip implementation
		   if (document.documentElement && document.documentElement.clientWidth) {
		   result.width = document.documentElement.clientWidth;
		   result.height = document.documentElement.clientHeight;
		   result.horizontalScroll = document.documentElement.scrollLeft;
		   result.verticalScroll = document.documentElement.scrollTop;
		   }
		   else if (document.body.clientWidth) {
		   result.width = document.body.clientWidth;
		   result.height = document.body.clientHeight;
		   result.horizontalScroll = document.body.scrollLeft;
		   result.verticalScroll = document.body.scrollTop;
		   }
		   */
	}
	return result;
}

// TODO: Set the relative path to ImagePrefix constant
SocialCalc.Constants.defaultImagePrefix = "socialcalc/" + SocialCalc.Constants.defaultImagePrefix;

SocialCalc.Keyboard.focusTableCopy = SocialCalc.Keyboard.focusTable;

SocialCalc.Vis = SocialCalc.Vis || {};

SocialCalc.Vis.blur = function() {
	if (!SocialCalc.Keyboard.focusTableCopy)
		SocialCalc.Keyboard.focusTableCopy = SocialCalc.Keyboard.focusTable;

	SocialCalc.Keyboard.focusTable = null;
}

SocialCalc.Vis.focus = function() {
	SocialCalc.Keyboard.focusTable = SocialCalc.Keyboard.focusTableCopy;
}


SocialCalc.SettingsControlSave = function(target) {
	var range, cmdstr;
	var s = SocialCalc.GetSpreadsheetControlObject();
	var sc = SocialCalc.SettingsControls;
	var panelobj = sc.CurrentPanel;
	var attribs = SocialCalc.SettingsControlUnloadPanel(panelobj);

	SocialCalc.SetTab("edit"); // return to edit tab
	SocialCalc.KeyboardFocus();

	if (target=="sheet") {
		cmdstr = s.sheet.DecodeSheetAttributes(attribs);
	}
	else if (target=="cell") {
		if (s.editor.range.hasrange) {
			range = SocialCalc.crToCoord(s.editor.range.left, s.editor.range.top) + ":" +
				SocialCalc.crToCoord(s.editor.range.right, s.editor.range.bottom);
		}
		cmdstr = s.sheet.DecodeCellAttributes(s.editor.ecell.coord, attribs, range);
	}
	else { // Cancel
	}
	if (cmdstr) {
		s.editor.EditorScheduleSheetCommands(cmdstr, true, false);
	}
}
