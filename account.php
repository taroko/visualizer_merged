<?php

$pw_file = ".readme";

function pw_verify($id, $pw)
{
	global $pw_file;
	$output = shell_exec("htpasswd -vb $pw_file $id $pw 2>&1");

	// return false when failed; integer otherwise
	return strpos($output, "correct");
}

function pw_check($pw1, $pw2)
{
	return strcmp($pw1, $pw2) == 0;
}

function pw_modify($id, $new_pw)
{
	global $pw_file;
	$output = shell_exec("htpasswd -b $pw_file $id $new_pw 2>&1");

	// return false when failed; integer otherwise
	return strpos($output, "Updating");
}

function pw_main($id, $current_pw, $new_pw, $cnew_pw)
{
	$msg = "";

	if ($current_pw == "")
	{
		$msg = "Current password is needed.";
	}
	else if (!pw_verify($id, $current_pw))
	{
		$msg = "Current password is incorrect.";
	}
	else if (!pw_check($new_pw, $cnew_pw))
	{
		$msg = "New passwords do not match.";
	}
	else if ($new_pw == "")
	{
		$msg = "New password cannot be empty.";
	}
	else
	{
		pw_modify($id, $new_pw);
		$msg = "Success!";
	}

	echo $msg;
}

if (count($_POST) == 4)
{
	echo pw_main(@$_POST["id"], @$_POST["cpw"], @$_POST["npw"], @$_POST["cnpw"]);
}
else
{
	echo "Access deny!";
}


?>
