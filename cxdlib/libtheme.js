define(["dojo/_base/declare"], function(declare) {
	return declare("Theme", null, {
		layout_button_class: ".menu_layout_buttons"
		,constructor: function()
		{
			console.log("Theme", $("#menu_layout_btn_2_w_v_sc_d3"));
			this.layouts();
		}
		,layouts: function()
		{
			this.layouts_btn_window();
		}
		,layouts_btn_window: function(){
			$(this.layout_button_class).each(function(){
				var id = $(this).attr("id");
				var id_array = id.split("_");
				
				///@brief 分析 id 名稱，判斷window長什麼樣子
				///@brief 再遇到分隔前，window 前後各有幾個
				var window_content = {0:[], 1:[]};
				var window_type = "";
				var window_idx = 0;
				for(var i=3; i < id_array.length; ++i)
				{
					var obj_name = id_array[i];
					if(obj_name == "a") continue;
					if(obj_name == "h" || obj_name == "v")
					{
						window_type = obj_name;
						window_idx ++;
						continue;
					}
					var obj = {"name": obj_name, "height": 1, "width": 1};
					window_content[window_idx].push(obj);
				}
				///@brief 實做 html
				var html = "";
				for(var key in window_content)
				{
					for(var i=0; i < window_content[key].length; i++)
					{
						if(window_type == "h")
						{
							window_content[key][i].height = 2;
							window_content[key][i].width = window_content[key].length;
						}
						else if(window_type == "v")
						{
							window_content[key][i].height = window_content[key].length;
							window_content[key][i].width = 2;
						}
						var html_class = "layout_btn_window" 
						html_class += " layout_btn_window_h" + window_content[key][i].height + "_w"+ window_content[key][i].width;
						html_class += " layout_btn_window_" + window_content[key][i].name ;
						html += "<div class='"+html_class+"' >";
						html += "</div>";
					}
				}
				$(this).html(html);
			});
		}
	});
});
