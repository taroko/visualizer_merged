define(["dojo/_base/declare"], function(declare) {
	return declare("D3Controler", null, 
	{
		///@brief d3 museum colume number, 0 = auto calc
		museum_frame_column_num: 0
		,museum_frame_raw_num: 0
		,museum_frame_max_num: 4
		,d3_graph_objs: {}
		,current_selected: ""
		,constructor: function() {
			this.museum_init();
		}
		,museum_id: "D3Museum"
		,dialog_title_embed: function(div_id, event, obj)
		{
			///@brief 增加縮小按鈕
			var btn = $("#"+div_id).prev().find("button").clone(false).appendTo($("#"+div_id).prev());
			btn.removeClass("ui-state-focus").css("right", "2em");
			btn.find("span.ui-icon").css("background-position-x", "-64px").css("background-position-y", "-126px");
			btn.bind("click", function(){
				cs.d3.move_to_museum(event, obj);
				$("#"+div_id).dialog("close");
			});
		}
		,rename_container_title: function(new_name)
		{
			if(new_name)
			{
				$("#D3Container").dialog({ "title": "Plotter - "+new_name });
			}
			else
			{
				$("#D3Container").dialog({ "title": "Plotter"});
			}
		}
		,rename_museum_title: function(d3_id, new_name)
		{
			var g_number = d3_id.split("_").pop();
			var sc_id = "sh_job_" + g_number;
			var job_id = "job_" + g_number;
			$("#"+sc_id).attr("title", new_name );
			$("#"+d3_id).attr("title", new_name );
		}
		,remove_dialog: function()
		{
			//TODO: remove dialog, sc sheet and job...
		}
		,auto_calculate_all_d3_legend_size: function()
		{
			//for(var key in cs.d3.d3_graph_objs)
			//	cs.d3.auto_calculate_d3_legend_size(key);
		}
		,auto_calculate_d3_legend_size: function(g_id)
		{
			//重新計算 legend 長度，避免字擠在一起
			//var d3_obj = cs.d3.d3_graph_objs[g_id];
			//d3_obj.resize_words()
			//d3_obj.resize_legend();
		}
		,move_to_museum_by_job_id: function(job_id)
		{
			var $target = $("#graph_" + job_id);
			cs.d3.move_to_museum_routine($target);
		}
		,move_to_museum: function(event, obj) {
			var $target = $(event.target)//.clone(true);	// div graph
			cs.d3.move_to_museum_routine($target);
		}
		,move_to_museum_routine: function($target) {
			//$(event.target).remove();
			$target = $target.removeAttr("style").removeClass().addClass("sort_item");
			$target.appendTo($("#D3Museum"));
			var job_id = $target.attr("id").split("_").splice(1,2).join("_");
			cs.d3.rename_museum_title($target.attr("id"), cs.job_list[job_id].get_mouseover_title());
			//$target.attr("title", cs.job_list[job_id].get_mouseover_title() );
			cs.d3.d3_graph_objs[ $target.attr("id") ].redraw(); // for sequence logo...
			$("#D3Museum").sortable("refresh");
			// Can not use this.cs.museum_reorder, can not find "this"
			cs.d3.museum_reorder();
			cs.d3.museum_select($target.attr("id"));
			
			//賦予事件點兩下變成 dialgo
			//cs.d3.museum_dblclick_out(event, $target);
			//cs.d3.museum_click_select_SC(event, $target);
			cs.d3.museum_dblclick_out(null, $target);
			cs.d3.museum_click_select_SC(null, $target);

			$("#itips").hide();
		}
		,museum_dblclick_out: function(event, target)
		{
			var thisA = this;
			target.unbind("dblclick").bind("dblclick", function(){
				thisA.museum_out_handler(event, target);
			});
		}
		,museum_click_select_SC: function(event, target)
		{
			target.unbind("click").bind("click", function(){
				cs.d3.museum_select( target.attr("id"));
			});
		}
		,museum_select: function(d3_id)
		{
			if (!d3_id) return;
			var g_number = d3_id.split("_").pop();
			var sc_id = "sh_job_" + g_number;
			var job_id = "job_" + g_number;
			//console.log("A", d3_id, cs.d3.current_selected);
			
			if (!cs.job_list[job_id])
			{
				console.log("(museum select) job is not exist", job_id);
				return;
			}


			if (d3_id != cs.d3.current_selected)
			{
				//console.log(d3_id, cs.d3.current_selected);
				//置換 sc sheet
				cs.sc.resume_sheet_tab(job_id);
				cs.d3.rename_container_title(cs.job_list[job_id].get_window_title());
				cs.sc.set_title(cs.job_list[job_id].get_window_title());
			}
			$("#"+cs.d3.current_selected).removeClass("current_selected");
			cs.d3.current_selected = d3_id;
			$("#" + cs.d3.current_selected).addClass("current_selected");
		}
		,delete_d3_obj: function(d3_id)
		{
			$("#"+d3_id).remove();

			if (!cs.d3.d3_graph_objs[d3_id])
			{
				console.log("this d3 obj is not exist");
				return;
			}

			delete cs.d3.d3_graph_objs[d3_id];
			
			var new_current = Object.keys(cs.d3.d3_graph_objs)[0];
			if(new_current)
			{
				//還有物件，選擇剩下的物件
				this.museum_select(new_current);
			}
			else
			{
				//已經沒有物件了
				cs.sc.resume_sheet_tab("main");
			}
		}
		,museum_init: function()
		{
			$("#D3Museum").sortable().droppable({
				accept: "#D3Museum > div",
				out: this.museum_out_handler, 
				over: this.museum_over_handler, 
				tolerance: 'touch',
				activeClass: "museum_highlight",
			});

			$("#itips").show();
		}
		,museum_out_handler: function(event, ui)
		{
			///@brief d3 meseum 拖移出去，變成 dialog
			var $target = {};
			if(ui.helper)
				$target = $(ui.helper[0]);
			else
				$target = ui;
			
			//舊的 target 會被移除，回傳新的 target
			var $new_target = cs.d3.museum_out_routine($target);
			cs.d3.museum_select($new_target.attr("id"));
			cs.d3.museum_click_select_SC(event, $new_target);
		},
		museum_out_routine: function($target)
		{
			var div_id = $target.attr("id");
			var job_id = "job_" + div_id.split("_")[2];
			
			$target.attr("id", "temp_item");
			$("#D3Container").append('<div id="'+div_id+'"></div>');
			var conf = {
				open: function(event, obj){
					cs.d3.dialog_title_embed(div_id, event, obj);
				},
				//close: cs.d3.move_to_museum,
				close: cs.d3.remove_dialog,
				position: {
					my: "center",
					at: "center",
					of: window,	
				},
				resizeStop: cs.d3.auto_calculate_all_d3_legend_size
			};
	
			cs.windowize(div_id, conf, cs.job_list[job_id].get_window_title() );
			$target.children()/*.clone()*/.appendTo($("#"+div_id));
			
			$target.remove();//.dialog("destroy");
			//cs.d3.d3_graph_objs[div_id].redraw(); // for sequence logo...
			// Can not use this.museum_reorder, can not find "this"
			cs.d3.museum_reorder(-1);

			if (cs.d3.is_museum_empty())
			{
				$("#itips").show();
			}

			return $("#"+div_id);
		},
		museum_resume: function($target)
		{
			var job_id = $target.attr("id");

			// Cut graph_ of graph_job_n
			job_id = job_id.substr(6);

			if (cs.d3.is_in_museum(job_id))
			{
				cs.d3.museum_out_routine($target);
			}
		},
		museum_over_handler: function(event, ui)
		{
			
		}
		,museum_reorder_by_column: function(num)
		{
			this.museum_frame_column_num = num;
			this.museum_reorder();
		}
		,museum_reorder_by_raw: function(num)
		{
			this.museum_frame_raw_num = num;
			this.museum_reorder();
		}
		,do_museum_reorder: function(type, value)
		{
			//console.log("museum_reorder", type, value);
			if(type == "c" && (this.museum_frame_column_num < this.museum_frame_max_num || this.museum_frame_column_num > 0))
			{
				this.museum_frame_column_num += value;
			}
			else if(type == "r" && (this.museum_frame_raw_num < this.museum_frame_max_num || this.museum_frame_raw_num > 0))
			{
				this.museum_frame_raw_num += value;
			}
			if(this.museum_frame_column_num > this.museum_frame_max_num) this.museum_frame_column_num = this.museum_frame_max_num;
			if(this.museum_frame_raw_num > this.museum_frame_max_num) this.museum_frame_raw_num = this.museum_frame_max_num;
			if(this.museum_frame_column_num < 1) this.museum_frame_column_num = 1;
			if(this.museum_frame_raw_num < 1) this.museum_frame_raw_num = 1;
			console.log(this.museum_frame_column_num, this.museum_frame_raw_num);
			this.museum_reorder();
			//this.museum_reorder();
		}
		,museum_reorder: function(fixed_item_number)
		{
			//console.log("museum_reorder museum_reorder")
			///@brief d3 dialog 按下 X 按鈕，把圖縮回 d3 museum後，對 d3 museum 重新排版
			///@brief d3 meseum 拖移出去後，原本的物件會減少，所以要重算 (fixed_item_number = -1)
			if(!fixed_item_number) fixed_item_number = 0;
			$sort_items = $("#D3Museum > div");
			$sort_items.removeClass();
			
			var column_num = this.museum_frame_column_num;
			var raw_num = this.museum_frame_raw_num;
			
			if(column_num == 0) column_num = this.auto_calc_frame_num($sort_items.length + fixed_item_number);
			if(raw_num == 0) raw_num = this.auto_calc_frame_num($sort_items.length + fixed_item_number);
			//console.log(column_num, raw_num);
			//console.log($sort_items.length, column_num, raw_num);
			$sort_items.addClass("sort_item");
			$sort_items.addClass("sort_item_w"+column_num);
			$sort_items.addClass("sort_item_h"+raw_num);
			$("#D3Museum").sortable("refresh");
			
			//重新計算 legend 長度，避免字擠在一起
			cs.d3.auto_calculate_all_d3_legend_size();
		}
		,auto_calc_frame_num: function(num)
		{
			for (var i = 1; i <= this.museum_frame_max_num; i++) {
				if (num <= i*i)
					return i;
			}
			return 4;
		}
		,d3_graph_right_click_impl: function(e, name, d3_obj)
		{
			var jD3_obj = $(d3_obj.config.window[0]);
			var jD3_div = jD3_obj.parent().parent();
			var job_id = jD3_div.attr("id").split("_").splice(1,2).join("_");
			var job = cs.job_list[job_id];
			var job_fns = job.file_name.split(".");
			// Table.lendist.Tail.prefix.GMPM.miRNA.detail.2.1.0.0.LEN.ANNOvsSEQ.15.read_count.tsv
			// 0      1        2    3     4    5      6             11  12       13    14
			
			if(job_fns[5] == "miRNA")
			{
				var track_name = job.sample+"-BAM.2.1.0.0.miRNA.sorted-bam_coverage";
				cs.jb.jbrowse.searchNames(name);
			}
			else// if(job_fns[5] == "biotype")
			{
				var track_name = job.sample+"-BAM.2.1.0.0."+name+".sorted-bam_coverage";
			}
			
			if(cs.jb.jbrowse.trackConfigsByName[track_name])
				cs.jb.jbrowse.showTracks(track_name);
			
			//console.log( name, job_id, job.file_name, track_name, job);
		}
		,d3_graph_x_axis_right_click: function(e, name, d3_obj)
		{
			cs.d3.d3_graph_right_click_impl(e, name, d3_obj);
			e.preventDefault();
			return false;
		}
		,d3_graph_legend_right_click: function(e, name, d3_obj)
		{
			cs.d3.d3_graph_right_click_impl(e, name, d3_obj);
			e.preventDefault();
			return false;
		},
		get_snapshot: function()
		{
			return "";
		},
		set_snapshot: function(snapshot)
		{
			cs.d3.reset();

			var job
			for (job in cs.job_list)
			{
				if (cs.job_list[job].state == "show")
				{
					var savestr = cs.sc.extract_savestr(cs.sc.sheet_tabs[job].savestr);
					var data_csv = cs.sc.get_savestr_by_format("csv", savestr);
					var sheet_info = cs.get_sheet_info(data_csv);
					var graph_type = cs.job_list[job].d3_graph_type;
					var job_id = job;
					cs.graph_join_sc(sheet_info, graph_type, job_id);
				}
			}
			cs.d3.museum_select("graph_" + job);
		},
		reset: function()
		{
			for (var d3 in cs.d3.d3_graph_objs)
			{
				$("#" + d3).remove();
				delete cs.d3.d3_graph_objs[d3];
			}
		},
		is_in_museum: function(job_id)
		{
			return $("#graph_" + job_id).parent().attr("id") == this.museum_id;
		},
		is_museum_empty: function()
		{
			return $("#D3Museum").children("div").length == 0;
		}
	});
});
