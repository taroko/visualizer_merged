define(["dojo/_base/declare", "dojo/_base/lang", "cxdlib/libsheettab", "cxdlib/libd2iterator"], function(declare, lang, SheetTab, D2Iterator) {
	return declare("SCController", null, {

		container_id: null,
		sheet_tabs_id: "sheet_tabs",
		sheet_tab_btns_id: "sheet_tab_btns",
		spreadsheet: null,
		$sheet_tab_container: null,  // A jquery object of sheet_tab_container
		sheet_tabs: {}, // Objects of SheetTab with $element point to HTML element of SheetTab
		sheet_tab_width: 80,
		statusline_max_width: 270,
		sequence_num : 1,
		_pz_bgcolor: "rgb(220, 255, 220)",
		_pz_ytitle_bgcolor: "rgb(180, 180, 180)",
		_max_display_number_v: 25,
		_max_display_number_h: 10,
		_find_result: [],
		_find_index: 0,

		constructor: function(container_id)
		{
			this.container_id = container_id;
			this.$container = $("#" + this.container_id);
			this.spreadsheet = new SocialCalc.SpreadsheetControl();
			this.panel_tab_init();

			this.spreadsheet.sheet.is_value_changed = false;
		},
		open: function()
		{
			// Initialize the Spreadsheet Control and display it
			this.spreadsheet.InitializeSpreadsheetControl(this.container_id);
			
			this.spreadsheet.ExecuteCommand('redisplay', '');

			this.spreadsheet.requestedWidth = $("#" + this.container_id).width();
			this.spreadsheet.requestedHeight = $("#" + this.container_id).height();
			this.spreadsheet.DoOnResize();
			this.spreadsheet.requestedWidth = this.spreadsheet.requestedHeight = undefined;

			this.sheet_tab_init();
			this.$sheet_tab_container = $("#" + this.sheet_tabs_id);

			this.insert_sheet_tab("main", "main");
			this.current_sheet_tab = this.sheet_tabs.main;
			this.sheet_swap("main");
			this.update_sheet_tab_ui();

			this.buttons_init();

			// Use the edit tab while first loading
			this.change_sctab("edit");
		},
		resize: function(is_force)
		{
			if (is_force == true)
			{
				this.spreadsheet.requestedWidth = 0;
				this.spreadsheet.requestedHeight = 0;
				this.spreadsheet.DoOnResize();
			}

			this.spreadsheet.requestedWidth = $("#" + this.container_id).width();
			this.spreadsheet.requestedHeight = $("#" + this.container_id).height() - 10;
			this.spreadsheet.DoOnResize();

			this.$sheet_tab_container.width(
				this.spreadsheet.requestedWidth - this.statusline_max_width - $("#" + this.sheet_tab_btns_id).width() - 10
			);
		},
		reset: function()
		{
			for (var sheet_tab in this.sheet_tabs)
			{
				if (this.sheet_tabs[sheet_tab].get_id() != "main")
				{
					this.remove_sheet_tab(this.sheet_tabs[sheet_tab].get_id());
				}
			}

			this.current_sheet_tab = this.sheet_tabs["main"];
			this.clean();
		},
		update_sheet_tab_ui:function()
		{
			var acc = 0;
			var xpos = 0;

			for (tab in this.sheet_tabs) {
				this.sheet_tabs[tab].$element.removeClass().addClass("sc_tabs");
				
				if (this.sheet_tabs[tab] == this.current_sheet_tab) {
					this.sheet_tabs[tab].$element.removeClass().addClass("sc_tabs_selected");
					xpos = acc;
				}

				// 20px is for padding width
				acc += this.sheet_tabs[tab].$element.width() + 20;
			}

			// Set the x-axis scroll of sheet_tabs
			this.$sheet_tab_container.scrollLeft(xpos);
		},
		edit_lock: function()
		{
			this.spreadsheet.editor.noEdit = true;
		},
		edit_unlock: function()
		{
			this.spreadsheet.editor.noEdit = false;
		},
		insert_sheet_tab: function(job_id, sheet_tab_name)
		{
			sheet_tab_name = 
				(sheet_tab_name) || ("new " + this.get_sequence_num());

			if (this.sheet_tabs[job_id] != undefined)
			{
				return this.sheet_tabs[job_id];
			}

			var new_sheet = new SheetTab(
											job_id,
											sheet_tab_name,
											this.$sheet_tab_container,
											lang.hitch(this, this.sheet_tab_click_handler)
			);

			this.sheet_tabs[job_id] = new_sheet;
			return new_sheet;
		},
		remove_sheet_tab: function(job_id)
		{
			var sheet_tab = this.sheet_tabs[job_id];
			if (!sheet_tab)
			{
				console.log("this sheet tab is not exist");
				return;
			}

			sheet_tab.remove();
			delete this.sheet_tabs[job_id];
		},
		hide_sheet_tab: function(job_id)
		{
			var sheet_tab = job_id ? this.sheet_tabs.job_id : this.current_sheet_tab;

			if (sheet_tab.job_id == "main")
				return;

			$("#sh_" + sheet_tab.job_id).remove();

			this.sheet_swap("main");
		},
		resume_sheet_tab: function(job_id)
		{
			if (this.sheet_tabs[job_id] == undefined)
			{
				console.log("Can not resume a not exist sheet tab");
			}
			else
			{
				this.sheet_tabs[job_id].resume();
				this.sheet_swap(job_id);
			}
		},
		remote_load: function(sheet_tab)
		{
			cs.fileio.file_load("sheetfiles/" + sheet_tab.get_filename(), lang.hitch(this, this.set_savestr));
		},
		remote_save: function(sheet_tab)
		{
			var filename = "sheetfiles/" + sheet_tab.get_filename();
			var content = this.get_savestr();

			cs.fileio.file_save(filename, content);
		},
		local_save: function(sheet_tab)
		{
			// Save the current contend of SC into the target sheet_tab
			if (sheet_tab.get_savestr() != this.get_savestr())
			{
				sheet_tab.set_savestr(this.get_savestr());
			}
		},
		local_load: function(sheet_tab)
		{
			var savestr = sheet_tab.get_savestr();

			if (savestr)
			{
				this.set_savestr(savestr);
			}
			else
			{
				this.clean();
			}
		},
		set_savestr: function(savestr)
		{
			if (savestr == "")
			{
				this.clean();
			}
			else
			{
				var parts = this.spreadsheet.DecodeSpreadsheetSave(savestr);
				if (parts)
				{
					if (parts.sheet)
					{
						this.spreadsheet.sheet.ResetSheet();
						this.spreadsheet.ParseSheetSave(savestr.substring(parts.sheet.start, parts.sheet.end));
					}
					if (parts.edit)
					{
						this.spreadsheet.editor.LoadEditorSettings(savestr.substring(parts.edit.start, parts.edit.end));
					}
				}

				if (this.spreadsheet.editor.context.sheetobj.attribs.recalc=="off")
				{
					this.spreadsheet.ExecuteCommand('redisplay', '');
				}
				else
				{
					this.spreadsheet.ExecuteCommand('recalc', '');
				}
			}
		},
		get_savestr: function()
		{
			return this.spreadsheet.CreateSpreadsheetSave();
		},
		set_savestr_by_format: function(savestr, format)
		{
			format = format || "csv";

			savestr = SocialCalc.ConvertOtherFormatToSave(savestr, format, false);
			this.spreadsheet.sheet.ResetSheet();
			this.spreadsheet.sheet.ParseSheetSave(savestr);
			this.spreadsheet.ExecuteCommand('redisplay', '');
		},
		get_savestr_by_format: function(format, savestr)
		{
			format = format || "csv";

			if (!savestr)
			{
				savestr = this.spreadsheet.CreateSheetSave();
			}

			savestr = SocialCalc.ConvertSaveToOtherFormat(savestr, format, false);

			// To trim out the " AA"," BB" -> AA,BB
			// To trim out the " symbols
			//savestr = savestr.replace(/" |"/g, '');
			savestr = savestr.replace(/\"/g, '');

			return savestr;
		},
		extract_savestr: function(savestr)
		{
			savestr = savestr.split("--");
			savestr = savestr[2].substr(savestr[2].search("version"));

			return savestr;
		},
		get_snapshot: function()
		{
			var snapshot = {};

			snapshot["sheet_tabs"] = [];

			this.local_save(this.current_sheet_tab);
			
			for (var sheet_tab in this.sheet_tabs)
			{
				snapshot["sheet_tabs"].push(this.sheet_tabs[sheet_tab].get_snapshot());
			}

			return JSON.stringify(snapshot);
		},
		set_snapshot: function(snapshot)
		{
			snapshot = JSON.parse(snapshot);

			// clean all sheet tabs
			this.reset();

			// remove main sheet tab
			this.remove_sheet_tab("main");
			this.current_sheet_tab = null;

			for (var i in snapshot["sheet_tabs"])
			{
				var sheet_tab = this.insert_sheet_tab("tmp", "tmp");
				sheet_tab.set_snapshot(snapshot["sheet_tabs"][i]);
				this.sheet_tabs[sheet_tab.get_id()] = sheet_tab;
				delete this.sheet_tabs["tmp"];
			}

			delete snapshot;

			this.sheet_swap("main");
			this.sheet_tabs["main"].is_plot_mode = false;

			return true;
		},
		get_snapshot_DEPRECATED: function()
		{
			var snapshot = {};

			snapshot["sheet_tabs"] = [];
			
			for (var sheet_tab in this.sheet_tabs)
			{
				if (this.sheet_tabs[sheet_tab].get_id() != "main")
				{
					snapshot["sheet_tabs"].push(this.sheet_tabs[sheet_tab].get_snapshot());
				}
			}

			return JSON.stringify(snapshot);
		},
		set_snapshot_DEPRECATED: function(snapshot)
		{
			this.sheet_swap("main");

			snapshot = JSON.parse(snapshot);

			// clean all sheet tabs
			this.reset();

			for (var i in snapshot["sheet_tabs"])
			{
				var sheet_tab = this.insert_sheet_tab("tmp", "tmp");
				sheet_tab.set_snapshot(snapshot["sheet_tabs"][i]);
				this.sheet_tabs[sheet_tab.get_id()] = sheet_tab;
				delete this.sheet_tabs["tmp"];
			}

			delete snapshot;

			return true;
		},
		insert_graph: function(sheet_tab)
		{
			this.update_graph(sheet_tab);
		},
		update_graph: function(sheet_tab, redraw_mode)
		{
			sheet_tab = sheet_tab || this.current_sheet_tab;

			if (!sheet_tab) return;

			// ! The main sheettab has no d3 graph
			if (sheet_tab.get_id() == "main") return;

			var job_id = sheet_tab.job_id;
			var plot_zone = sheet_tab.plot_zone;

			var data_csv = this.get_savestr_by_format("csv");

			var data_array = cs.fmt.csv2array(data_csv);

			var data_array_trimed = this.filter_plot_zone(data_array, plot_zone);
			var data_jjson = cs.fmt.array2jjson(data_array_trimed);

			sheet_tab.update_graph(data_jjson, redraw_mode);
		},
		sheet_tab_click_handler: function(event_obj)
		{
			this.sheet_swap($(event_obj.target).data("job_id"));
			cs.d3.museum_select("graph_" + this.current_sheet_tab.get_id());
		},
		sheet_swap: function(job_id)
		{
			// Before change to target SheetTab, needs to reset `is_plot_mode` and save the SheetTab
			
			if (this.current_sheet_tab)
			{
				if (this.current_sheet_tab.get_id() == job_id)
				{
					this.local_save(this.current_sheet_tab);
					return;
				}

				if (this.current_sheet_tab.is_plot_mode)
				{
					this.current_sheet_tab.is_plot_mode = false;
					this.edit_unlock();
				}
				else
				{
					this.local_save(this.current_sheet_tab);
				}
			}


			// Start to change the current_sheet_tab to target SheetTab.

			this.current_sheet_tab = this.sheet_tabs[job_id];
			
			this.local_load(this.current_sheet_tab);

			if (this.current_sheet_tab.get_id() == "main")
			{
				$("#SocialCalc-edittab").click();
			}
			else
			{
				// ! If the SheetTab has no savestr, it is new SheetTab with content not ready.
				// ! Do not change to plot mode, and lets Job swapper control it.
				if (this.current_sheet_tab.get_savestr())
				{
					$("#SocialCalc-plottab").click();
				}
			}

			this.update_sheet_tab_ui();
		},
		set_title: function(title_name)
		{
			if (title_name)
			{
				this.$container.dialog({ "title": "DataSheet  - " + title_name });
			}
			else
			{
				this.$container.dialog({ "title": "DataSheet" });
			}

			this.update_sheet_tab_ui();
		},
		get_sequence_num: function()
		{
			return this.sequence_num++;
		},
		clean_bgcolor: function()
		{
			// reset all bgcolor
			for (var cell in spreadsheet.sheet.cells)
			{
				if (spreadsheet.sheet.cells[cell].bgcolor)
				{
					delete spreadsheet.sheet.cells[cell].bgcolor;
				}
			}
		},
		get_range_text: function(rowFr, colFr, rowTo, colTo) {
			var len_row = rowTo - rowFr;
			var len_col = colTo - colFr;
			data = Array();
			for (var r = 0; r < len_row; r++) {
				data.push(Array());
				for (var c = 0; c < len_col; c++) {
					var rr = rowFr + r + 1;
					var cc = colFr + c + 1;
					data[r].push(
						cs.sc.sheet.GetAssuredCell(SocialCalc.crToCoord(cc, rr)).datavalue
					);
				}
			}
			return data;
		},
		set_range_text: function(row, col, data) { // 0-based
			var paste_old = SocialCalc.Clipboard.clipboard;
			var paste = "version:1.5\n";

			for (var r = 0; r < data.length; r++) {
				for (var c = 0; c < data[r].length; c++) {
					var rr = row + r + 1; // 1-based
					var cc = col + c + 1; // 1-based

					paste = paste + "cell:" + SocialCalc.crToCoord(cc, rr) + ":t:" + data[r][c] + "\n";
				}
			}
			paste = paste + "copiedfrom:A1:" + SocialCalc.crToCoord(data[0].length, data.length);
			SocialCalc.Clipboard.clipboard = paste;
			cs.sc.sheet.ScheduleSheetCommands("paste A1 all", true);

			function recopy()
			{
				SocialCalc.Clipboard.clipboard = paste_old;
				paste_old = null;
			}

			setTimeout(recopy, 500);
		},
		set_bgcolor: function(left, top, right, bottom, color) {
			// left, top, right, bottom

			var start = SocialCalc.crToCoord(left, top);
			var end = SocialCalc.crToCoord(right, bottom);

			// for example: "set A1:D6 bgcolor rgb(0, 255, 0)"
			var cmdstr = 'set ' + start + ':' + end + ' bgcolor ' + color;
			this.exec_command_bypass(cmdstr, false);
		},
		set_cell_color: function(d2iterator, saveundo, color)
		{
			// SocialCalc Schedule Bypass
			var sci = new SocialCalc.SheetCommandInfo(this.spreadsheet.sheet);
			sci.saveundo = saveundo;

			if (sci.sheetobj.statuscallback)
			{
				// notify others if requested
				this.spreadsheet.sheet.statuscallback(sci, "cmdstart", "", sci.sheetobj.statuscallbackparams);
			}
			if (sci.saveundo)
			{
				sci.sheetobj.changes.PushChange(""); // add a step to undo stack
			}

			var cmdstr;
			while (d2iterator.has_next())
			{
				var it = d2iterator.next();
				cmdstr = "set " + SocialCalc.crToCoord(it.c, it.r) + " bgcolor " + color;
				sci.parseobj = new SocialCalc.Parse(cmdstr);
				SocialCalc.ExecuteSheetCommand(sci.sheetobj, sci.parseobj, sci.saveundo);
			}

			if (sci.sheetobj.statuscallback)
			{
				// notify others if requested
				this.spreadsheet.sheet.statuscallback(sci, "cmdend", "", sci.sheetobj.statuscallbackparams);
			}
		},
		get_cells_by_color: function(bgcolor_num)
		{
			var cells = this.spreadsheet.sheet.cells;
			var retval = [];

			for (var c in cells)
			{
				if (cells[c].bgcolor == bgcolor_num)
				{
					retval.push(c);
				}
			}

			return retval
		},
		rectangle_justify: function(cells)
		{
			var data_horizontal = [];
			var data_vertical = [];
			var tmp_horizontal = {};
			var tmp_vertical = {};
			
			var cr;
			for (var c in cells)
			{
				cr = SocialCalc.coordToCr(cells[c]);
				tmp_vertical[cr.row] = true;
				tmp_horizontal[cr.col] = true;
			}

			// The colum 1 and row 1 is y-title and x-title respectly, select next row/column instead
			if (tmp_vertical[1] == true)
			{
				tmp_vertical[2] = true;
				delete tmp_vertical[1];
			}

			if (tmp_horizontal[1] == true)
			{
				tmp_horizontal[2] = true;
				delete tmp_horizontal[1];
			}

			for (var h in tmp_horizontal)
			{
				if (tmp_horizontal[h] == true)
				{
					data_horizontal.push(h);
				}
			}

			for (var v in tmp_vertical)
			{
				if (tmp_vertical[v] == true)
				{
					data_vertical.push(v);
				}
			}

			return { horizontal: data_horizontal, vertical: data_vertical };
		},
		exec_command_bypass: function(cmdstr, saveundo)
		{
			// SocialCalc Schedule Bypass
			var sci = new SocialCalc.SheetCommandInfo(this.spreadsheet.sheet);
			sci.parseobj = new SocialCalc.Parse(cmdstr);
			sci.saveundo = saveundo;

			if (sci.sheetobj.statuscallback)
			{
				// notify others if requested
				spreadsheet.sheet.statuscallback(sci, "cmdstart", "", sci.sheetobj.statuscallbackparams);
			}
			if (sci.saveundo)
			{
				sci.sheetobj.changes.PushChange(""); // add a step to undo stack
			}

			SocialCalc.ExecuteSheetCommand(sci.sheetobj, sci.parseobj, sci.saveundo);

			if (sci.sheetobj.statuscallback)
			{
				// notify others if requested
				this.spreadsheet.sheet.statuscallback(sci, "cmdend", "", sci.sheetobj.statuscallbackparams);
			}
		},
		do_edit: function(spreadsheet, text)
		{
			// Do something when Edit tab be clicked
			//if (this.current_sheet_tab.is_plot_mode == true)
			//{
			//	this.remote_load(this.current_sheet_tab);
			//	this.current_sheet_tab.is_plot_mode = false;
			//	this.edit_unlock();
			//}
		},
		do_plot: function(spreadsheet, text)
		{
			this.local_save(this.current_sheet_tab);

			this.current_sheet_tab.is_plot_mode = true;
			this.edit_lock();

			this.satsify_plot_constrain(this.current_sheet_tab.plot_zone);
			this.display_plot_zone(this.current_sheet_tab.plot_zone);
		},
		undo_plot: function(spreadsheet, text)
		{
			// ! The save/load function of SheetTab depends on `is_plot_mode`
			// ! so when undo_plot, needs to release `is_plot_mode` then load the ture savestr.

			this.current_sheet_tab.is_plot_mode = false;
			this.edit_unlock();

			this.local_load(this.current_sheet_tab);
		},
		do_find: function(spreadsheet, text)
		{
			// For find tab
			
			$("#find_keyowrd").focus();
		},
		undo_find: function(spreadsheet, text)
		{
			// For find tab undo
		},
		do_format: function(spreadsheet, text)
		{
			this.resize(true);
		},
		get_d3_graph_type: function()
		{
			var job_id = cs.sc.current_sheet_tab.get_id();
			if (cs.job_list[job_id])
			{
				return cs.job_list[job_id].d3_graph_type;
			}
			else
			{
				return undefined;
			}
		},
		filter_plot_zone: function(data_arr, plot_zone)
		{
			// data_arr : 0-based data
			// plot_zone : 1-based data

			if (!data_arr)
			{
				// ! Data data_arr is incorrect
				console.error("Data format is incorrect");
			}
			
			
			if (!data_arr)
			{
				// Data format is incorrect
				return;
			}
			
			var data_arr_trimed = [];

			var rr, cc;
			
			// x-title
			rr = plot_zone.xtitle.vertical[0] - 1;
			var row_array = [];
			row_array.push(data_arr[0][0]); 
			for (var c in plot_zone.xtitle.horizontal)
			{
				cc = plot_zone.xtitle.horizontal[c] - 1;
				row_array.push(data_arr[rr][cc]);
			}
			data_arr_trimed.push(row_array);
			
			// data
			for (var r in plot_zone.data.vertical)
			{
				row_array = [];
				rr = plot_zone.data.vertical[r] - 1;
				cc = plot_zone.ytitle.horizontal[0] -1;
				if (data_arr[rr][cc])
					row_array.push(data_arr[rr][cc]);

				for (var c in plot_zone.data.horizontal)
				{
					cc = plot_zone.data.horizontal[c] - 1;
					if (data_arr[rr][cc])
						row_array.push(data_arr[rr][cc]);
				}
				data_arr_trimed.push(row_array);
			}
			
			return data_arr_trimed;
		},
		set_plot_zone: function(sc)
		{
			sc = sc || this;

			var editor = sc.spreadsheet.editor;
			if (editor.range.hasrange)
			{
				sc.set_bgcolor(editor.range.left, editor.range.top, editor.range.right, editor.range.bottom, sc._pz_bgcolor);
			}
			else
			{
				sc.set_bgcolor(editor.ecell.col, editor.ecell.row, editor.ecell.col + 0, editor.ecell.row + 0, sc._pz_bgcolor);
			}

			sc.calc_plot_zone(sc.current_sheet_tab.plot_zone);
			sc.update_graph(sc.current_sheet_tab, "all");
		},
		unset_plot_zone: function(sc)
		{
			sc = sc || this;

			var editor = sc.spreadsheet.editor;
			if (editor.range.hasrange)
				sc.set_bgcolor(editor.range.left, editor.range.top, editor.range.right, editor.range.bottom, "rgb(255, 255, 255)");
			else
				sc.set_bgcolor(editor.ecell.col, editor.ecell.row, editor.ecell.col + 0, editor.ecell.row + 0, "rgb(255, 255, 255)");

			sc.calc_plot_zone(sc.current_sheet_tab.plot_zone);
			sc.update_graph(sc.current_sheet_tab, "all");
		},
		calc_plot_zone: function(plot_zone)
		{
			var cells = this.get_cells_by_color(this.get_color_num(this._pz_bgcolor));
			var dim2 = this.rectangle_justify(cells);

			plot_zone.data.horizontal = dim2.horizontal;
			plot_zone.data.vertical = dim2.vertical;

			plot_zone.xtitle.horizontal = dim2.horizontal;
			plot_zone.xtitle.vertical = [1];

			plot_zone.ytitle.horizontal = [1];
			plot_zone.ytitle.vertical = dim2.vertical;

			this.satsify_plot_constrain(plot_zone);

			this.display_plot_zone(plot_zone);
		},
		init_plot_zone: function(data_arr)
		{
			var pz = {};
			pz.data = {};
			pz.xtitle = {};
			pz.ytitle = {};

			if (data_arr.length > this._max_display_number_v)
			{
				pz.data.vertical = _.range(2, this._max_display_number_v + 2);
			}
			else
			{
				pz.data.vertical = _.range(2, data_arr.length + 1);
			}

			if (data_arr[0].length > this._max_display_number_h)
			{
				pz.data.horizontal = _.range(2, this._max_display_number_h + 2);
			}
			else
			{
				pz.data.horizontal = _.range(2, data_arr[0].length + 1);
			}
			
			pz.xtitle.horizontal = pz.data.horizontal;
			pz.xtitle.vertical = [1];

			pz.ytitle.horizontal = [1];
			pz.ytitle.vertical = pz.data.vertical;

			this.satsify_plot_constrain(pz);

			return pz;
		},
		satsify_plot_constrain: function(plot_zone)
		{
			var graph_type = this.get_d3_graph_type();

			switch(graph_type)
			{
				case "BarChart":
				case "PieChart":
					plot_zone.data.horizontal.length = 1;
					plot_zone.xtitle.horizontal = plot_zone.data.horizontal;
			}

			return plot_zone;
		},
		display_plot_zone_for_title: function(plot_zone)
		{
			this.set_cell_color(
				new D2Iterator([1], [1]),
				false, 
				"rgb(200, 200, 200)"
			);

			this.set_cell_color(
				new D2Iterator([1], plot_zone.xtitle.horizontal),
				false, 
				"rgb(200, 200, 200)"
			);

			this.set_cell_color(
				new D2Iterator(plot_zone.ytitle.vertical, [1]),
				false, 
				this._pz_ytitle_bgcolor
			);
			this.spreadsheet.ExecuteCommand('redisplay', '');
		},
		display_plot_zone: function(plot_zone)
		{
			this.clean_bgcolor();

			this.set_cell_color(
				new D2Iterator(plot_zone.data.vertical, plot_zone.data.horizontal),
				false, 
				this._pz_bgcolor
			);
			
			this.display_plot_zone_for_title(plot_zone);
		},
		clean: function()
		{
			// ResetSheet() will not reset editor.ecell, so reset here by itself.
			this.spreadsheet.editor.MoveECell("A1");

			this.spreadsheet.sheet.ResetSheet();

			this.spreadsheet.ExecuteCommand('redisplay', '');
		},
		import_sheet_DEPRECATED: function(thisSC, csvstr)
		{
			var job_id = "jobs_" + cs.rand();
			thisSC.insert_sheet_tab(
				job_id,
				"ins " + thisSC.get_sequence_num()
			);

			thisSC.sheet_swap(job_id);
			thisSC.set_savestr_by_format(csvstr, "csv");
		},
		import_sheet: function(thisSC, data, meta)
		{
			var subject = meta.name.split(".")[0];
			var file = "import";
			var description = "Imported data sheet";
			var d3_graph_type = "StackedBarChart";

			var job = new Job(cs, subject, file, description, d3_graph_type);
			job.init();
			job.insert = data;
			cs.jobq.add_job(job, job.loader);
		},
		export_sheet: function()
		{
			var csvstr = this.get_savestr_by_format("csv");
			var sheet_name = this.current_sheet_tab.sheet_name;
			var filename = sheet_name + ".csv";

			cs.fileio.file_export(filename, csvstr);
		},
		get_color_num: function(colorstr)
		{
			return this.spreadsheet.sheet.colors.indexOf(colorstr);
		},
		sheet_tab_init: function()
		{
			var thisSC = this;

			var injectee = '<div id="' + this.sheet_tabs_id + '" style="float: left; height: 20px; padding: 2px 0px"></div>';
			var $sc_statusline = $("#SocialCalc-statusline");
			$sc_statusline.css("float", "right");

			var html_tabs_btns = '<div id="sc_tab_btns">';
			var html_btn_less = '<input type="button" id="sc_tab_less" style="background-image: url(jquery-ui/css/dark-hive/images/ui-icons_222222_256x240.png); background-position: -96px -16px; background-color: white;  height: 20px;width: 20px;">';
			var html_btn_more = '<input type="button" id="sc_tab_more" style="background-image: url(jquery-ui/css/dark-hive/images/ui-icons_222222_256x240.png); background-position: -32px -16px; background-color: white; height: 20px;width: 20px;">';

			// ! SC sheet tab add is replaced by `New Job`
			// ! SC sheet tab remove is unnecessary

			//var html_btn_add = '<input type="button" id="sc_tab_add" style="background-image: url(jquery-ui/css/dark-hive/images/ui-icons_222222_256x240.png); background-position: -16px -128px; background-color: white; height: 20px;width: 20px;">';
			//var html_btn_remove = '<input type="button" id="sc_tab_remove" style="background-image: url(jquery-ui/css/dark-hive/images/ui-icons_222222_256x240.png); background-position: -80px -128px; background-color: white; height: 20px;width: 20px;">';

			//html_tabs_btns = html_tabs_btns + html_btn_less + html_btn_more + html_btn_add + html_btn_remove + '</div>';
			html_tabs_btns = html_tabs_btns + html_btn_less + html_btn_more + '</div>';

			$sc_statusline.parent().append(html_tabs_btns);

			$("#sc_tab_less").click(lang.hitch(this, this.left_scroll));
			$("#sc_tab_more").click(lang.hitch(this, this.right_scroll));

			// ! SC sheet tab add/remove is unnecessary

			//$("#sc_tab_add").click(function() {
			//	thisSC.insert_sheet_tab("job_" + cs.rand(), "new " + thisSC.get_sequence_num());
			//});

			//$("#sc_tab_remove").click(function() {
			//	thisSC.hide_sheet_tab();
			//});

			$sc_statusline.parent().append(injectee);
		},
		left_scroll: function()
		{
			var tabs = $("#" + this.sheet_tabs_id);
			tabs.scrollLeft(tabs.scrollLeft() - 50);
		},
		right_scroll: function()
		{
			var tabs = $("#" + this.sheet_tabs_id);
			tabs.scrollLeft(tabs.scrollLeft() + 50);
		},
		panel_tab_init: function()
		{
			var thisSC = this;

			// Front-end panel tab

			for (var tab in this.spreadsheet.tabnums)
			{
				this.spreadsheet.tabnums[tab] += 1;
			}

			this.spreadsheet.tabnums.plot = 0;//this.spreadsheet.tabs.length;
			this.spreadsheet.tabs.unshift({name: "plot", text: '<span style="color: yellow">Plot</span>', html:
				  '<div id="%id.plottools" style="display:none;">'+
				  '  <table cellspacing="0" cellpadding="0"><tr>'+
				  '   <td style="vertical-align:top;padding-right:30px;">'+
				  '    <div style="%tbt.">%loc! Read only and exclusively for plot area setting</div>'+
				  '   </td>'+
				  '	 </tr>'+
				  '  <tr>'+
				  '   <td>'+
				  '	   <button type="button" id="simple_set">Set</button>'+
				  '	   <button type="button" id="simple_unset">Unset</button>'+
				  '	   <button type="button" id="simple_clean">Clear</button>'+
				  '   </td>'+
				  '  </tr></table>'+
				  '</div>',
				view: "sheet",
				onclick: lang.hitch(this, this.do_plot),
				onunclick: lang.hitch(this, this.undo_plot),
				onclickFocus: false
			});

			// Back-end panel tab

			this.spreadsheet.tabnums.find = this.spreadsheet.tabs.length;
			this.spreadsheet.tabs.push({name: "find", text: 'Find', html:
				  '<div id="%id.findtools" style="display:none;">'+
				  '  <table cellspacing="0" cellpadding="0"><tr>'+
				  '   <td style="vertical-align:top;padding-right:30px;">'+
				  '    <div style="%tbt.">Find what you want here</div>'+
				  '   </td>'+
				  '	 </tr>'+
				  '  <tr>'+
				  '   <td>'+
				  '	   <div style="%tbt.">'+
				  '	   Find: <input type="text" id="find_keyowrd" />'+
				  '	   <input type="checkbox" id="find_csen" /> case sensitivity '+
				  '	   <button type="button" id="find_previous">Previous</button>'+
				  '	   <button type="button" id="find_next">Next</button>'+
				  '    <span id="find_msg"></span>'+
				  '    <span id="find_current"></span>'+
				  '    </div>'+
				  '   </td>'+
				  '  </tr></table>'+
				  '</div>',
				view: "sheet",
				onclick: lang.hitch(this, this.do_find),
				//onunclick: lang.hitch(this, this.undo_find),
				onclickFocus: false
			});

		},
		panel_tab_init_DEPRECATED: function()
		{
			var thisSC = this;

			this.spreadsheet.tabnums.plot = this.spreadsheet.tabs.length;
			this.spreadsheet.tabs.push({name: "plot", text: '<span style="color: yellow">Plot</span>', html:
				  '<div id="%id.plottools" style="display:none;">'+
				  '  <table cellspacing="0" cellpadding="0"><tr>'+
				  '   <td style="vertical-align:top;padding-right:30px;">'+
				  '    <div style="%tbt.">Please set the plot zone. Warning: in this mode, modification will be ignored!!'+
				  '	   <button type="button" id="simple_set">Set</button>'+
				  '	   <button type="button" id="simple_unset">Unset</button>'+
				  '	   <button type="button" id="simple_clean">Clean</button>'+
				  '   </td>'+
				  '  </tr></table>'+
				  '</div>',
				view: "sheet",
				onclick: lang.hitch(this, this.do_plot),
				onclickFocus: true,
			});
		},
		buttons_init: function()
		{
			thisSC = this;

			var import_onchang = function()
			{
				cs.fileio.file_import("sc_import", thisSC, thisSC.import_sheet);
			};

			$("#sc_import").change(import_onchang);

			$("#simple_set").click(function() {
				thisSC.set_plot_zone();
			});
			$("#simple_unset").click(function() {
				thisSC.unset_plot_zone();
			});
			$("#simple_clean").click(function() {
				thisSC.clean_bgcolor();
				thisSC.calc_plot_zone(thisSC.current_sheet_tab.plot_zone);
				thisSC.update_graph(thisSC.current_sheet_tab, "all");
			});

			$("#find_csen").change(function () {
				$("#find_keyowrd").blur();
			});

			$("#find_keyowrd").focus(function() {
				// ! Prevent SC capture the keyboard event
				SocialCalc.Keyboard.passThru = true;
			});

			$("#find_keyowrd").click(function()
			{
				// ! Prevent SC capture the keyboard event
				// ! Prevent: input click -> cell click -> input click, SC capture the keyword event
				$(this).focus();
			});

			$("#find_keyowrd").blur(function() {
				var keyword = $("#find_keyowrd").val();

				if (keyword == "")
				{
					$("#find_msg").html("");
					thisSC._find_result = [];
					thisSC._find_index = -1;
					return;
				}

				var is_case_sensitivity = $("#find_csen").is(":checked");

				thisSC._find_result = thisSC.find(keyword, is_case_sensitivity);
				thisSC._find_index = -1;

				if (thisSC._find_result.length >= 2)
					$("#find_msg").html("Total " + thisSC._find_result.length + " results");
				else
					$("#find_msg").html("Total " + thisSC._find_result.length + " result");
			});

			$("#find_previous").click(function() {
				if (thisSC._find_result.length <= 0)
				{
					$("#find_msg").html("No results");
					return;
				}

				thisSC._find_index -= 1;
				if (thisSC._find_index < 0)
				{
					thisSC._find_index = thisSC._find_result.length - 1;
				}

				var cell = thisSC._find_result[thisSC._find_index];;
				thisSC.spreadsheet.editor.MoveECell(cell);
			});

			$("#find_next").click(function() {
				if (thisSC._find_result.length <= 0)
				{
					$("#find_msg").html("No results");
					return;
				}

				thisSC._find_index += 1;
				if (thisSC._find_index >= thisSC._find_result.length)
				{
					thisSC._find_index = 0;
				}

				var cell = thisSC._find_result[thisSC._find_index];;
				thisSC.spreadsheet.editor.MoveECell(cell);
			});
		},
		change_sctab: function(name)
		{
			SocialCalc.SetTab(name);
		},
		find: function(keyword, is_case_sensitivity)
		{
			var targets = [];
			is_case_sensitivity = is_case_sensitivity || 0;

			var my_find_test = is_case_sensitivity ? this.find_test_csen : this.find_test;

			for (var c in this.spreadsheet.sheet.cells)
			{
				var is_find = my_find_test(this.spreadsheet.sheet.cells[c].datavalue, keyword);
				if (is_find)
				{
					targets.push(this.spreadsheet.sheet.cells[c].coord);
				}
			}

			return targets;
		},
		find_test: function(content, keyword)
		{
			// A case-insensitive find test function

			content = content.toString();
			keyword = keyword.toString();

			var retval = content.toLowerCase().indexOf(keyword.toLowerCase());

			return retval != -1;
		},
		find_test_csen: function(content, keyword)
		{
			// A case-sensitive find test function

			content = content.toString();
			keyword = keyword.toString();

			var retval = content.indexOf(keyword);

			return retval != -1;
		}
		
	});
});
