define(["dojo/_base/declare", "dojo/_base/lang", "cxdlib/libfmt"], function(declare, lang, Format) {
	return declare("SheetTab", null, {
		$element: null,
		job_id: null,
		sheet_name: null,
		is_plot_mode: false,
		savestr: null,
		plot_zone: { data: { horizontal: [], vertical: [] }, xtitle: { horizontal: [], vertical: [] }, ytitle: { horizontal: [], vertical: [] } },

		constructor: function(job_id, sheet_name, $container, click_handler)
		{
			this.job_id = job_id;
			this.sheet_name = sheet_name;
			this.$parent = $container;
			this.click_handler = click_handler;

			this.init(this.job_id, this.sheet_name);
		},
		init: function(job_id, sheet_name)
		{
			var tab_html = Format().tab_html(this.job_id, this.sheet_name);
			this.$parent.append(tab_html);
			this.$element = $('#sh_' + job_id);
			this.$element.data("job_id", job_id);
			this.$element.click(this.click_handler);
		},
		resume: function()
		{
			if (this.$parent.find("#sh_" + this.job_id).length == 0)
			{
				this.init(this.job_id, this.sheet_name);
			}
		},
		set_name: function(sheet_name)
		{
			this.sheet_name = sheet_name;
			this.$element.html(sheet_name);
		},
		get_id: function()
		{
			return this.job_id;
		},
		set_id: function(id)
		{
			this.job_id = id;
			this.$element.attr("id", "sh_" + id);
			this.$element.data("job_id", id);
		},
		get_filename: function()
		{
			return this.job_id + ".sheet";
		},
		get_snapshot: function()
		{
			var snapshot = {};
			
			snapshot["job_id"] = this.job_id;
			snapshot["sheet_name"] = this.sheet_name;
			snapshot["plot_zone"] = JSON.stringify(this.plot_zone);
			snapshot["is_plot_mode"] = this.is_plot_mode;
			snapshot["savestr"] = this.savestr;

			return JSON.stringify(snapshot);
		},
		set_snapshot: function(snapshot)
		{
			snapshot = JSON.parse(snapshot);

			this.set_id(snapshot["job_id"]);
			this.set_name(snapshot["sheet_name"]);
			this.plot_zone = JSON.parse(snapshot["plot_zone"]);
			//this.is_plot_mode = snapshot["is_plot_mode"];
			// ! When set_snapshot, the sheettab is close (main open only) so is_plot_mode is false.
			this.is_plot_mode = false;
			this.savestr = snapshot["savestr"];

			return true;
		},
		set_savestr: function(savestr)
		{
			// When its in plot mode, the savestr is stored temporarily in savestr_plot.
			// This function depends on `is_plot_mode`
			
			if (this.is_plot_mode)
				this.savestr_plot = savestr;
			else
				this.savestr = savestr;
		},
		get_savestr: function()
		{
			// When its in plot mode, the savestr is stored temporarily in savestr_plot.
			// This function depends on `is_plot_mode`
			
			return this.is_plot_mode ? this.savestr_plot : this.savestr;
		},
		update_graph: function(data_jjson, mode)
		{
			var d3_obj = cs.d3.d3_graph_objs["graph_" + this.job_id];

			if (d3_obj)
			{
				d3_obj.redraw(data_jjson, mode);
			}
		},
		remove: function()
		{
			this.$element.unbind("click");
			this.$element.remove();
		}
	});
});
