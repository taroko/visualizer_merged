define(["dojo/_base/declare", "dojo/_base/lang"], function(declare, lang) {
	return declare("JobQueue", null, {
		jobs: [],
		threads: [],
		_interv: null,
		_max: 2,
		_full: 10,
		_jobq_id: "jobQ",
		_jobq_isopened: false,
		_jobq_config: {
			height: 26,
			resizable: false,
			animate: true,
			beforeClose: function(event, ui) {
				return false;
			},
			focus: function(event, ui) {
				//cs.global_focus("JB");
				cs.window_inner_resize();
			},
			resizeStop: function(event, ui){
				cs.window_inner_resize();
			}
		},
		
		constructor: function() {
			// Initial threads
			for (var i = 0; i < this._max; i++) {
				var thisA = this;
				thisA.threads.push({job: null, timer: null});
				thisA.threads.timer = setInterval(thisA.run, 1000, i, thisA);
			}
			// Initial job dispatcher
			this.interv = setInterval(lang.hitch(this, this.dispatch), 1000);
		},
		add_job: function(obj, func) {
			if (this.jobs.length < this._full) {
				this.jobs.push(lang.hitch(obj, func));
			} else {
				console.log("Job full");
			}
		},
		get_job: function() {
			return this.jobs.shift();
		},
		dispatch: function() { // for main thread
			// Dispatch job to threads if there is any job
			for (var i = 0; i < this._max; i++) {
				if (this.threads[i].job == null && this.jobs.length) {
					this.threads[i].job = this.get_job();
				}
			}
		},
		run: function(tid, thisA) { // for fork threads
			// Check if job assignment, execute job
			if (thisA.threads[tid].job == null) return;
			thisA.threads[tid].job();
			thisA.threads[tid].job = null;
		},
		destroy: function() {
			// Clean all timer of setInterval()
			for (var i = 0; i < this._max; i++) {
				clearInterval(this.threads[i].timer);
			}
			clearInterval(this.interv);
		},
		init_ui: function() {
			this._job_isopened = false;

			$("#"+this._jobq_id).dialog(this._jobq_config);
			$("#"+this._jobq_id).parent().css({
				minHeight: 0,
				minWidth: 0,
				height: 26,
				width: 100,
				top: "5%",
				left: "70%",
				zIndex: 104, // for always on top
			}).children(".ui-dialog-content").hide();
			
			$("#"+this._jobq_id+" ul").sortable();
			$("#"+this._jobq_id+" ul").disableSelection();

			var thisA = this;
			$("#"+this._jobq_id).siblings(".ui-dialog-titlebar").children("button").click(function() {
				if (thisA._jobq_isopened) {
					thisA.set_close();
				} else {
					thisA.set_open();
				}
			});
		},
		set_close: function() {
			thisA = $("#"+this._jobq_id).parent();
			thisA.animate({
				height: 26,
			}, 500, "easeOutQuad").animate({
				width: 90,
			}, 500, "easeOutQuad", function() {
			});

			thisA.children(".ui-dialog-content").hide();

			$("#"+this._jobq_id).siblings(".ui-dialog-titlebar").children("span").html("Jobs");

			this._jobq_isopened = false;
		},
		set_open: function() {
			thisA = $("#"+this._jobq_id).parent();
			thisA.animate({
				width: 340,
			}, 500, "easeOutQuad").animate({
				height: this.get_height(),
			}, 500, "easeOutQuad");

			thisA.children(".ui-dialog-content").show();

			$("#"+this._jobq_id).siblings(".ui-dialog-titlebar").children("span").html("Jobs Queue");

			$("#"+this._jobq_id).css({height: "auto"});

			this._jobq_isopened = true;
		},
		refresh: function() {
			var thisA = this;
			$("#"+this._jobq_id).parent().animate({
				height: thisA.get_height(),
			});
			$("#"+this._jobq_id).css({height: "auto"});
		},
		get_height: function() {
			var h = 70;
			var n = $("#jobs li").length;
			return h * n + 26;
		},
		reset: function()
		{
			$("#jobs").children().remove();
			this.refresh();
		}
	});
});

