define(["dojo/_base/declare", "dojo/_base/lang", "cxdlib/libfmt"], function(declare, lang, fmt) {
	return declare("Job", [fmt], {
		pack_id: null,
		subject: null,
		description: null,
		information: null,
		file: null, // "sample-all/analysis/Table.lendist.Tail.full.PM.miRNA.detail.2.1.0.0.ANNO.SEQvsLEN.MIR30A.read_count.tsv"
		file_name: null, // "Table.lendist.Tail.full.PM.miRNA.detail.2.1.0.0.ANNO.SEQvsLEN.MIR30A.read_count.tsv"
		l_name: null,
		s_name: null,
		d3_graph_type: "BarChart",
		_cs: null,
		_container: "#jobs",
		serial_number: 0,
		state: null,
		ready_data: null,
		
		constructor: function(cs, subject, file, desc, d3_gtype) {
			this._cs = cs;
			this.file = file;
			this.subject = subject;
			this.description = desc;
			this.d3_graph_type = d3_gtype;
			this.serial_number = cs.get_job_serial_number();
			this.pack_id = "job_" + this.serial_number;
		},
		get_snapshot: function()
		{
			var snapshot = {};

			for (var item in this)
			{
				if (this.hasOwnProperty(item) && typeof(this[item]) != typeof({}))
				{
					snapshot[item] = this[item];
				}
			}

			snapshot.ready_data = JSON.stringify(this.ready_data);
			
			return JSON.stringify(snapshot);
		},
		set_snapshot: function(snapshot)
		{
			snapshot = JSON.parse(snapshot);

			if (snapshot.state == "ready")
			{
				this.ready_data = JSON.parse(snapshot.ready_data);
			}

			this.set_parameter(snapshot);

			return true;
		},
		init: function(conf) {
			if (conf)
			{
				// ! This section should be placed in set_snapshot(...)

				this.pack_id = conf.pack_id;
				this.subject = conf.subject;
				this.description = conf.description;
				this.state = conf.state;
			}


			// create DOM
			$(this._container).append(this.job_html2(this.pack_id, this.subject, this.description));	

			cs.job_list[this.pack_id] = this;

			if (conf)
			{
				// ! Add a Job of snapshot.
				// ! And a batch of Jobs will be added so change UI later.
				if (this.state == "show")
				{
					this.update_info(conf);
					this.ready();
					this.swapper_bypass();
				}
				else if (this.state == "ready")
				{
					this.ready(this.ready_data);
				}
			}
			else
			{
				// ! Add a normal Job from user, so open Jobqueue.
				this.state = "init";
				cs.jobq.set_open();
			}
		},
		get_window_title: function(){
			return this.subject + " (" + this.description + ")";
		},
		get_mouseover_title: function(){
			return this.s_name;
		},
		get_subject: function(){
			return this.subject;
		},
		set_parameter: function(conf)
		{
			var is_change = false;
			for(var key in conf)
			{
				if(this[key] != conf[key]) 
					is_change = true;
				this[key] = conf[key];
			}
			return is_change;
		},
		loader: function()
		{
			var thisA = this;
			
			var done = function(data_csv) {
				if (data_csv)
				{
					//var data_arr = thisA.csv2array(data_csv);
					//var sheet_info = {row: data_arr.length, col: data_arr[0].length, data_arr: data_arr, data_csv: data_csv};
					var sheet_info = cs.get_sheet_info(data_csv);
					thisA.ready(sheet_info);
				}
			}
			// for add new empty job
			if (this.file == "")
			{
				var sheet_info = {row: 0, col: 0, data_arr: [[]], data_csv: ""};
				thisA.ready(sheet_info);
				return;
			}
			if (this.file == "import")
			{
				var sheet_info = cs.get_sheet_info(this.insert);
				thisA.ready(sheet_info);

				delete this.insert;
				return;
			}
			thisA._cs.async_exec(cs.csaltdock, {"cmd":"get_table", "filename": this.file}, done); // by andy
		},
		ready: function(sheet_info)
		{
			var btn_status = "#" + this.pack_id + "_status";
			var btn_remove = "#" + this.pack_id + "_del";
			var btn_copy = "#" + this.pack_id + "_copy";
			var btn_info = "#" + this.pack_id + "_info";

			this.$btn_status = $(btn_status);
			this.$btn_remove = $(btn_remove);
			this.$btn_copy = $(btn_copy);

			$(btn_status).attr("disabled", false).html("Ready");
			$(btn_status).click(lang.hitch(this, this.swapper, sheet_info));
			$(btn_remove).click(lang.hitch(this, this.remover));
			$(btn_copy).click(lang.hitch(this, this.clone));
			$(btn_copy).css("background-color", "#666");
			$(btn_info).click(lang.hitch(this, this.make_info_dialog, $(btn_info)));

			this.state = "ready";
			this.ready_data = sheet_info;
		},
		make_info_dialog: function(jInfo)
		{
			var thisA = this;
			var dialog = $("#jMenu_dialog");
			
			//防止已經開啟的視窗還開著，會出現錯誤顯示
			if( dialog.data("display_type") == "opened")
			{
				console.log("window already opened..., destroy it...");
				dialog.dialog("destroy");
				dialog.data("display_type", "closed");
			}
			$("#new_job").prev(".jMenu_dialog_button").trigger( "click" );
			
			// document click 會自動讓 window 消失，但是再開啟時會同時觸發，導致視窗馬上毀滅，所以先設成 closed 先不觸發點擊消失
			dialog.data("display_type", "closed");
			
			//重設 window 屬性
			dialog.dialog({
				title: "Job info: " + thisA.subject
				,minWidth: 350
				,minHeight: 400
			})
			.parent()
			.position({
				my: "right top",
	            at: "left top",
	            of: jInfo.parents("#jobQ")
			});
			
			// dialog element 賦予值，填入目前 job 的設定值
			// job_info_subject, job_info_description
			$(".job_info_subject").last().val(thisA.subject);
			$(".job_info_description").last().val(thisA.description);
			$(".job_info_information").last().text(thisA.information);
			$(".job_info_graph_type option[value='"+thisA.d3_graph_type+"']").last().attr("selected", "selected");
			
			$(".job_info_create").hide();
			// 使用者按下 OK 扭，呼叫更新 parameter function
			$(".job_info_update").show().unbind("click").bind("click", function(e){
				console.log("update info");
				thisA.update_info({
					"subject": $(".job_info_subject").last().val(),
					"description": $(".job_info_description").last().val(),
					"information": $(".job_info_information").last().val(),
					"d3_graph_type": $(".job_info_graph_type").last().val(),
				});
				$("#jMenu_dialog").data("display_type", "closed").dialog("close");
				// 不做 document click 事件
				e.stopPropagation();
			});
		},
		update_info: function(conf)
		{
			var is_d3_redraw = conf.d3_graph_type == this.d3_graph_type ? false : true;
			var is_change = this.set_parameter(conf);
			
			// d3圖已經畫出來了，如果有改設定，要重劃 d3圖
			// ?? 可能的 bug 已經隱藏的 legend and x-axis ?? ---> maybe ok
			// ?? 重劃後的 legend words 緊密是否有重新計算過 ?? ---> ok
			if(is_d3_redraw && this.$btn_status && this.$btn_status.html() == "Show")
			{
				var d3_obj = cs.d3.d3_graph_objs["graph_" + this.pack_id];
				d3_obj.strategy = d3_obj.draw_type_strategies(this.d3_graph_type);
				d3_obj.redraw(undefined, "all");
			}
			// 如果內容有被改變，所有顯示的名稱都要改變 (subject and description)
			if(is_change)
				this.update_all_element_name();
		},
		update_all_element_name: function()
		{
			//D3Container
			if(cs.d3.current_selected == "graph_"+this.pack_id)
			{
				cs.d3.rename_container_title( this.get_window_title() );
				cs.sc.set_title( this.get_window_title() );
			}
			//d3 window title
			cs.d3.rename_museum_title("graph_"+this.pack_id, this.get_mouseover_title());
			//d3 museum title
			// No need to change
			
			//SocialCalcContainer ---> ok
			//sc tab subject
			if (cs.sc.sheet_tabs[this.pack_id])
			{
				cs.sc.sheet_tabs[this.pack_id].set_name(this.subject);
			}
			//sc tab mouseover title ---> ok
			
			//jque subject
			var btn_subject = "#" + this.pack_id + "_subject";
			$(btn_subject).html(this.subject);
			//jque description
			var btn_description = "#" + this.pack_id + "_description";
			$(btn_description).html(this.description);
		},
		clone: function()
		{
			var new_job = new Job(cs, "", this.file, this.description, this.d3_graph_type);
			new_job.subject = this.subject + "(Copy_" + new_job.serial_number + ")";
			new_job.information = this.information;
			new_job.init();

			cs.sc.sheet_swap(this.pack_id);

			var data_csv = cs.sc.get_savestr_by_format("csv");
			//data_csv = data_csv.replace(/\"/g, "");//sp bug

			var data_arr = new_job.csv2array(data_csv);
			var sheet_info = {row: data_arr.length, col: data_arr[0].length, data_arr: data_arr, data_csv: data_csv};
			console.log(sheet_info);
			new_job.ready(sheet_info);
		},
		swapper: function(sheet_info)
		{
			this.state = "show";
			this.first_hit = true;
			this.$btn_status.html("Show");
			this.$btn_copy.attr("disabled", false).css("background-color", "black");


			var sheet_name = this.pack_id;
			if (cs.sc.sheet_tabs[this.pack_id] != undefined)
			{
				cs.sc.resume_sheet_tab(this.pack_id);
				cs.d3.museum_out_handler(undefined, $("#graph_" + this.pack_id));
			}
			else
			{
				var new_sheet = cs.sc.insert_sheet_tab(this.pack_id, this.subject);
				cs.sc.sheet_swap(this.pack_id);
				cs.sc.set_savestr_by_format(sheet_info.data_csv, "csv");
				new_sheet.plot_zone = cs.sc.init_plot_zone(sheet_info.data_arr);
				cs.graph_join_sc(sheet_info, this.d3_graph_type, this.pack_id);
				cs.d3.museum_select("graph_"+this.pack_id);

				// Change SCtab to plot in the first time loadding
				$("#SocialCalc-plottab").click();

				delete sheet_info;
				this.ready_data = null;
			}
		},
		swapper_bypass: function(sheet_info)
		{
			this.state = "show";
			this.first_hit = true;
			this.$btn_status.html("Show");
			this.$btn_copy.attr("disabled", false).css("background-color", "black");
		},
		remover: function() {
			var d3_id = "graph_" + this.pack_id;
			
			// remove sc element
			cs.sc.remove_sheet_tab(this.pack_id);
			// remove sc obj
			
			// remove d3 element
			// remove d3 obj
			cs.d3.delete_d3_obj(d3_id);
			
			// remove job element
			this.remove_job();
		},
		remove_job: function(is_not_refresh)
		{
			var btn_remove = "#" + this.pack_id + "_del";
			$(btn_remove).parent().parent().remove();

			if (!is_not_refresh)
				cs.jobq.refresh();
			
			delete cs.job_list[this.pack_id];
		}
	});
});
