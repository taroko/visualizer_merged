define(["dojo/_base/declare"], function(declare) {
	return declare("SimpleList", null, {
		container_id: null,
		buttons: [ "remove", "modify" ],
		controls: {},
		unique: 1,
		$list: null,

		constructor: function(container_id)
		{
			this.container_id = container_id;
			this.$list = this.init_html(container_id);
		},
		add_DEPRECATED: function(display_text)
		{
			var key = this.unique++;
			
			var $items = this.add_item_html(key, display_text);

			// Register all native buttons click handlers.
			var thisA = this;
			$items.remove.on("click", function(e) { thisA.remove(key); });
			$items.modify.on("click", function(e) { thisA.modify(key); });
			$items.touch.on("click", function(e) { thisA.touch(key); });
		
			return key;
		},
		add: function(display_text, key)
		{
			// If key is not initial, just give a default unique number.
			if (key == undefined)
			{
				key = this.unique++;
			}
			
			var $items = this.add_item_html(key, display_text);

			// Register all native buttons click handlers.
			var thisA = this;
			$items.remove.on("click", function(e) { thisA.remove(key); });
			$items.modify.on("click", function(e) { thisA.modify(key); });
			$items.touch.on("click", function(e) { thisA.touch(key); });
		
			return key;
		},
		remove: function(key)
		{
			// Using remove will close the upper level windows
			$("." + this.get_unique_name(key)).hide(500);

			if (this.controls["remove"])
			{
				this.controls.remove(key);
			}
		},
		modify: function(key)
		{
			// If using jQueryUI, this list will be copied, find class instead of id.
			// Everytime to find the node will get two more targets while using jQueryUI,
			// so using last() to get the visible one.
			var new_text;

			var $targets = $("." + this.get_unique_name(key));
			$targets = $targets.children(".simple_list_touch");
			var $display = $targets.last();
			$display.hide();
			var val = $display.html();
			var $input = $display.parent().append('<input type="text" value="' + val +'" />').children().last();
			$input.focus();

			var thisA = this;

			// FIXME: Must take care about when user close dialog directly, that would not trigger blur event
			$input.on("blur", function() { 
				new_text = $(this).val();
				$targets.html(new_text);
				$display.show();
				$(this).remove();


				if (thisA.controls["modify"])
				{
					thisA.controls.modify(key, new_text);
				}
			});

		},
		rename: function(key, new_text)
		{
			var $displays = $("." + this.get_unique_name(key)).children(".simple_list_touch");
			$displays.html(new_text);
		},
		touch: function(key)
		{
			if (this.controls["touch"])
			{
				this.controls.touch(key);
			}
		},
		init_html: function(container_id)
		{
			var $list = $("#" + this.container_id);
			$list = $list.append('<ul class="simple_list_ul simple_list_' + container_id + '"></ul>').children().last();

			return $list;
		},
		add_item_html: function(key, display)
		{
			var $items = {};

			var $item = this.$list.append('<li class="simple_list ' + this.get_unique_name(key) + '"></li>').children().last();

			$items["remove"] = $item.append('<div class="img_div_btn simple_list_remove"></div>').children().last();
			$items["modify"] = $item.append('<div class="img_div_btn simple_list_modify"></div>').children().last();
			$items["touch"] = $item.append('<div class="simple_list_touch">' + display + '</div>').children().last();

			return $items;
		},
		get_unique_name: function(key)
		{
			return this.container_id + "_" + key;
		},
		update_key: function(key)
		{
			this.unique = key;
		},
		reset: function()
		{
			this.unique = 1;
			this.$list.children().remove();
		},
		readonly: function(key)
		{
			$target = $("." + this.get_unique_name(key));
			$target.children(".simple_list_modify").unbind("click");
			$target.children(".simple_list_remove").unbind("click");
		}
	});
});
