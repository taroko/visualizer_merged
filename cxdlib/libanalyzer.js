

define(["dojo/_base/declare", "cxdlib/libjob"], function(declare, Job) {
	return declare("Analyzer", null, {
		constructor: function() 
		{
			this.ana_summary_load();
			this.ana_quick_make_selection();
			this.ana_lendist_make_selection();
		}
		,config: {
			"auto_make_sample_option_jSelect": ".ana_sample select"
			,"auto_make_graph_type_jSelect": ".ana_graph select"
			,"analysis_mirna_heterogeneity_sample_jSelect": ".ana_mirna_heterogeneity_sample select"
			,"analysis_mirna_heterogeneity_graph_type_jSelect": ".ana_mirna_heterogeneity_graph_type select"
			,"analysis_mirna_sequence_logo_sample_jSelect": ".ana_mirna_sequence_logo_sample select"
			,"analysis_mirna_sequence_logo_mirna_jSelect": ".ana_mirna_sequence_logo_mirna_list select"
			,"analysis_mirna_sequence_logo_matrix_jSelect": ".ana_mirna_sequence_logo_matrix select"
			,"d3_plot_type_list": 
				["BarChart", "GroupedBarChart", "NormalizedBarChart", "StackedBarChart", "PieChart", "SequenceLogo"]
			,"ana_plot_selection_fields_name": 
				["Sample", "Table", "Analyzor", "AnaSeqType", "LengthType", "ReadCountType", "AnnoBiotype", "GeneNameDetail"
				, "SysType", "Filter", "Database", "DB_field", "Z-Type", "XvsYType", "Z", "CountType"]
			,"summary_container_id": "summary_content"
			,"analysis_plot_selection_id": "plot_selection"
			//,"analysis_exec_filename": "bin/analysis_handler.php"
			,"analysis_exec_filename": "bin/csaltdock-visualizer.php"

		}
		/* prepare Summary content */
		,ana_make_information: function(file_name, field_name, rename_rule, exclude_field)
		{
			function rename(id, key)
			{
				if(rename_rule[id] && rename_rule[id][key])
					key = rename_rule[id][key];
				return key;
			}
			var tmp_file_name = file_name.split(".");
			var text = "";
			for(var key in tmp_file_name)
			{
				if(! field_name[key] || $.inArray(field_name[key], exclude_field) != -1)
					continue;
				text += field_name[key] + ": " + rename( field_name[key], tmp_file_name[key] ) + "\n";
			}
			return text;
		}
		,ana_summary_load: function()
		{
			var thisA = this;
			var menu_config = { "config":{"child_type": "accordion"}};
			
			///@brief sample list
			$.post(cs.csaltdock, {"cmd": "get_sample_list", "genome": cs.global_config.current_genome}, function(d)
			{
				var sample_list = $.parseJSON(d);
				var callback = function(check_idx)
				{
					cs.menu.create_menu($("#"+thisA.config.summary_container_id), menu_config);
				}
				var idx = 0;
				for(var key in sample_list)
				{
					var url = "jbrowse/jbrowse_db/raw/"+ cs.global_config.current_genome +"/"+ sample_list[key] +"/mappability/Table.mappability.tsv";
					//讓裡面sample可以接到key個別變數，不然在callback後裡面sample會直接指向最後一個 key (sample全都相同)
					(function(sample){
						$.get(url , function(d){
							var content = "<pre style='background:none;'>"+d+"</pre>";
							menu_config[sample] = {
								"config": {"child_type": "section"}
							};
							menu_config[sample][sample] = {"config": {"title": "", "content": content}};
							idx++;
							if(idx == sample_list.length) callback(idx);
						});
					})(sample_list[key]);
				}
			});
		}
		,ana_lendist_option_table: {}
		/* prepare Plot selection content */
		,ana_lendist_select_rename: {
			"ReadCountType": 
			{
				"GMPM": "Perfact+Prefix Length"
				, "GM": "Perfact Length"
				, "PM": "Prefix Length"
			}
			,"AnnoBiotype":
			{
				"biotype": "Biotype detail"
				, "merged": "Merged"
			}
			,"SysType":
			{
				"0": "Summary"
				, "2": "More detail"
			}
			,"Filter":
			{
				"-1": "Total (No Filter Rule)"
				, "1": "Filter out garbage"
				, "0": "show garbage"
			}
			,"Database":
			{
				"0": "Ensemble"
				, "1": "Mirna 3P, 5P"
			}
			,"DB_field":
			{
				"0": "Biotype"
				, "1": "GeneName"
			}
			,"Z-Type":
			{
				"NoSeq": "No key"
				, "Merged": "*** Detail ***"
			}
		}
		,ana_lendist_selected_record: {}
		,ana_lendist_table_callback: function()
		{
			$(this.config["auto_make_sample_option_jSelect"])
				.html( cs.fmt.make_option_html(this.ana_lendist_option_table, "key", "key") );
			$(this.config["auto_make_graph_type_jSelect"])
				.html( cs.fmt.make_option_html(this.config.d3_plot_type_list, "value", "value", "StackedBarChart") );
			this.ana_quick_make_selection_z_option();
			this.ana_sequence_logo_make_selection_z_option();
		}
		,ana_lendist_make_selection: function()
		{
			if(!cs.global_config.current_genome)
				return;
			var genome = cs.global_config.current_genome;
			
			var thisA = this;
			var select_field = thisA.config["ana_plot_selection_fields_name"];
			var ana_lendist_selected_record = thisA.ana_lendist_selected_record;
			
			var plot_selection_id = "#" + thisA.config.analysis_plot_selection_id;
			var first_field_name = select_field[0];
			//var exec_filename = thisA.config.analysis_exec_filename;
			var exec_filename = cs.csaltdock;
			
			///@brief 選擇 chart plot 複製
			//$(plot_selection_id).append($("#sel_graph").clone());
			//$(plot_selection_id).children("select").last().attr("id", "").attr("class", "sel_graph");


			$(plot_selection_id).append('<div class="ana_graph sel_graph">Select draw type: <select></select></div>');
			
			///@brief dialog 會有兩份，所以一份改了，另外一份也要改變
			$(".sel_graph select").bind("change", function(){
				console.log($(this).val());
				$(".sel_graph select option[selected]").removeAttr("selected");
				$(".sel_graph select" + " option[value='"+$(this).val()+"']").attr("selected",true);
			});
			
			$(plot_selection_id).append("<br />");
			
			///@brief 建立空的 select
			for(var key in select_field)
			{
				var html = "<div class='plot_selection_title'>" + select_field[key] + ":</div>";
				html +=  "<select class='plot_sel_"+ select_field[key] +"'>";
				html += "</select><br />";
				var jSel = $(plot_selection_id).append(html).children("select").last();
				
				///@brief 賦予改變事件
				jSel.data("field_idx", key ).bind("change", function(){
					var field_idx = $(this).data("field_idx");
					var field = select_field[ field_idx ];
					//$(".plot_sel_Sample option[value='sample-T']").attr("selected",true); // $(this).removeAttr("selected");
					ana_lendist_selected_record[field] = $(this).val();
					thisA.ana_lendist_make_selection_rec(0, thisA.ana_lendist_option_table, field_idx);
					
					// 當改變值的時候，也一起改變背景 select 的值
					var class_name = $(this).attr("class");
					$("."+class_name + " option[selected]").removeAttr("selected");
					$("."+class_name + " option[value='"+$(this).val()+"']").attr("selected",true);
					//console.log("class_name", class_name, $(this).val());
				});
			}
			
			///@brief 讓不常用的選項隱藏
			$("select.plot_sel_Table").hide().prev().hide().prev().hide();
			$("select.plot_sel_Analyzor").hide().prev().hide().prev().hide();
			$("select.plot_sel_SysType").hide().prev().hide().prev().hide();
			$("select.plot_sel_Database").hide().prev().hide().prev().hide();
			$("select.plot_sel_DB_field").hide().prev().hide().prev().hide();
			$("select.plot_sel_GeneNameDetail").hide().prev().hide().prev().hide();
			$("select.plot_sel_Filter").hide().prev().hide().prev().hide();
			
			//var load_table = function(file){
			//	console.log(file);
			//	var job = new Job(cs, "Subject", file, "");
			//	job.d3_graph_type = $(".sel_graph").val();
			//	job.init();
			//	cs.jobq.add_job(job, job.loader);
			//};
			//
			///@brief 建立執行按鈕，讀取table然後輸出到 excel and d3
			var html = "<input type='button' class='exec_plot' value='Draw' />";
			var jSel = $(plot_selection_id).append(html).children("input").last();
			jSel.bind("click", function(){
				var sample = ana_lendist_selected_record[first_field_name];
				var table_file_name = "";
				for(var key in ana_lendist_selected_record)
				{
					if(key == first_field_name) continue; // table 名字沒有 sample， sample 是資料夾
					table_file_name += ana_lendist_selected_record[key] + ".";
				}
				table_file_name += "tsv";
				//var full_get_table_url = exec_filename + "?file=" + table_file_name + "&sample=" + sample;
				var filename = genome + "/" + sample + "/analysis/" + table_file_name;
				var job = thisA.ana_load_table(filename, $(".sel_graph select").val());
				thisA.ana_lendist_make_job_name(job, filename);
			});
			
			///@brief 讀取plot table資料結構
			$.post(exec_filename, {"cmd":"get_analysis_table_list", "ana_type":"analysis", "genome": genome}, function(d){
				thisA.ana_lendist_option_table = $.parseJSON(d);
				console.log(thisA.ana_lendist_option_table);
				thisA.ana_lendist_make_selection_html(first_field_name, thisA.ana_lendist_option_table);
				thisA.ana_lendist_make_selection_rec(0, thisA.ana_lendist_option_table);
				thisA.ana_lendist_table_callback();
			});
		}
		
		,ana_lendist_make_job_name: function(job, file)
		{
			var t = file.split("/");
			var sample = t[1];
			var tmp = t.pop();
			var file_name = tmp;
			tmp = tmp.split("."); // Table.lendist.Tail.prefix.GMPM.miRNA.detail.2.1.0.0.LEN.ANNOvsSEQ.15.read_count.tsv
							      // 0      1        2    3     4    5      6             11  12       13    14
			// long name:  Table.Tail.miRNA.LEN(15).ANNOvsSEQ.prefix-len.GMPM-count.read_count
			// short name: Tail.LEN(15).ANNOvsSEQ.prefix.GMPM.read_count
			var l_name = sample + "."+tmp[2]+"."+tmp[5]+"."+tmp[11]+"("+tmp[13]+")."+tmp[12]+"."+tmp[3]+"-len."+tmp[4]+"-count."+tmp[14];
			var s_name = sample + "."+tmp[2]+"."+tmp[11]+"("+tmp[13]+")."+tmp[12]+"."+tmp[4]+"."+tmp[14];
			
			var info_exclude_field = ["Table", "SysType", "Database", "DB_field", "GeneNameDetail"];
			var conf = {
				"sample": sample,
				"file_name": file_name,
				"l_name": l_name,
				"s_name": s_name,
				"subject": "LEN_" + job.serial_number,
				"description": s_name,
				"information": this.ana_make_information(file_name, this.config.ana_plot_selection_fields_name.slice(1), this.ana_lendist_select_rename, info_exclude_field)
			};
			job.update_info(conf);
			//job.set_parameter(conf);
		}
		,ana_lendist_make_selection_rec: function(idx, list, sp_idx)
		{
			var thisA = this;
			var select_field = thisA.config["ana_plot_selection_fields_name"];
			var ana_lendist_selected_record = thisA.ana_lendist_selected_record;
			
			///@brief sp_idx 是給使用者選擇的第N個select欄位(change)，沒有sp_idx則表示為第一次自動建立
			
			if(idx >= select_field.length) return;
			
			var field = select_field[idx];
			var field_id = ".plot_sel_"+field;
			
			///@brief 如果idx小於等於sp_idx，value使用已經記錄的，因為只有idx大於sp_idx的欄位有可能會改變，會重建 select（如果改變，則使用第一個option值）
			if(idx <= sp_idx)
				var value = ana_lendist_selected_record[field];
			else
				var value = $(field_id).val();
			
			///@brief 如果 idx >= sp_idx， value 已經隨著selection重建而改變，所以必須存回 ana_lendist_selected_record
			if(sp_idx == undefined || idx >= sp_idx )
			{
				ana_lendist_selected_record[field] = value;
				this.ana_lendist_make_selection_html(select_field[idx+1], list[value]);
			}
			this.ana_lendist_make_selection_rec(idx+1, list[value], sp_idx);
			
		}
		,ana_lendist_make_selection_html: function(id, list)
		{
			var thisA = this;
			
			function rename(id, key)
			{
				var rename_rule = thisA.ana_lendist_select_rename;
				if(rename_rule[id] && rename_rule[id][key])
					key = rename_rule[id][key];
				return key;
			}
			$(".plot_sel_"+id).html("");
			var content_number = 0;
			var html = "";
			for(var key in list)
			{
				html += "<option value='"+key+"'>";
				html += rename(id, key);
				html += "</option>";
				content_number++;
			}
			$(".plot_sel_"+id).html(html);
			
			if(content_number <= 1)
				$(".plot_sel_"+id).attr("disabled", true);
			else
				$(".plot_sel_"+id).attr("disabled", false);
			
			///如果新的選項裡面還有就的選項，就改成原本的選項
			var origin_option = $(".plot_sel_"+ id + " option[value='" + thisA.ana_lendist_selected_record[id] + "']");
			if(origin_option.length > 0)
				origin_option.attr("selected", true);
		}
		,ana_quick_draw: function(event, obj)
		{
			var plot_default_config = {
				"Sample":"sample-G" //x
				,"Table":"Table" //x
				,"Analyzor":"lendist" //x 
				,"AnaSeqType":"Normal" // level1, level 3
				,"LengthType":"full" // level 3
				,"ReadCountType":"GMPM" //level 3
				,"AnnoBiotype":"biotype" // level 2
				,"GeneNameDetail":"no_detail"  // level 2
				,"SysType":"2"  // x
				,"Filter":"1"  // x
				,"Database":"0" // x
				,"DB_field":"0" // x
				,"Z-Type":"SEQ" // level 1, 2
				,"XvsYType":"ANNOvsLEN" // level 1, 2
				,"Z":"Merged" // level 3
				,"CountType":"ppm" // x
			}
			var thisA = $(this);
			
			if(obj != undefined)
				thisA = obj;
			
			// copy default config
			var plot_config = jQuery.extend(true, {}, plot_default_config);
			var parent_obj_l2 = thisA.parents(".quick_ana_l2");
			var plot_type = "StackedBarChart";
			
			plot_config["Sample"] = thisA.parents(".menu_quick_selection").find(".ana_sample select").val();
			
			// level 1
			if(thisA.parents(".biotype_summary, .biotype_sequence_feature").length == 1)
			{
				plot_config["AnnoBiotype"] = "biotype";
				plot_config["GeneNameDetail"] = "no_detail";
			}
			if(thisA.parents(".mirna_summary, .mirna_sequence_feature").length == 1)
			{
				plot_config["AnnoBiotype"] = "miRNA";
				plot_config["GeneNameDetail"] = "detail";
			}
			
			// level 2
			if(thisA.parents(".ratio").length != 0)
			{
				plot_type = "PieChart";
				plot_config["Z-Type"] = "SEQ";
				plot_config["XvsYType"] = "ANNOvsLEN";
			}
			if(thisA.parents(".lendist").length != 0)
			{
				plot_type = "StackedBarChart";
				plot_config["Z-Type"] = "SEQ";
				plot_config["XvsYType"] = "LENvsANNO";
			}
			if(thisA.parents(".d_ratio").length != 0)
			{
				plot_type = "PieChart";
				plot_config["Z-Type"] = "ANNO";
				plot_config["XvsYType"] = "SEQvsLEN";
			}
			if(thisA.parents(".d_lendist").length != 0)
			{
				plot_type = "StackedBarChart";
				plot_config["Z-Type"] = "ANNO";
				plot_config["XvsYType"] = "LENvsSEQ";
			}
			if(thisA.parents(".d_lenseq").length != 0)
			{
				plot_type = "StackedBarChart";
				plot_config["Z-Type"] = "LEN";
				plot_config["XvsYType"] = "ANNOvsSEQ";
			}
			
			// 特定狀況
			if(thisA.parents(".biotype_summary, .ratio").length == 2)
			{
				plot_type = "StackedBarChart";
			}
			
			// 取得 select value
			// level 3
			if(parent_obj_l2.find(".read_count_type").length != 0)
			{
				plot_config["ReadCountType"] = parent_obj_l2.find(".read_count_type select").val();
				if(plot_config["ReadCountType"] == "TailingRatio")
					plot_config["CountType"] = "tailing_ratio";
			}
			if(parent_obj_l2.find(".length_type").length != 0)
			{
				plot_config["LengthType"] = parent_obj_l2.find(".length_type select").val();
			}
			if(parent_obj_l2.find(".z_value").length != 0)
			{
				plot_config["Z"] = parent_obj_l2.find(".z_value select").val();
			}
			
			if(parent_obj_l2.find(".ana_seq_type").length != 0)
			{
				plot_config["AnaSeqType"] = parent_obj_l2.find(".ana_seq_type select").val();
			}
			if(obj == undefined)
			{
				var genome = cs.global_config.current_genome;
				var sample = plot_config["Sample"];
				var table_file_name = "";
				for(var key in plot_config)
				{
					if(key == "Sample") continue; // table 名字沒有 sample， sample 是資料夾
					table_file_name += plot_config[key] + ".";
				}
				table_file_name += "tsv";
				//var full_get_table_url = exec_filename + "?file=" + table_file_name + "&sample=" + sample;
				var filename = genome + "/" + sample + "/analysis/" + table_file_name;
				var job = cs.analyzer.ana_load_table(filename, plot_type);
				cs.analyzer.ana_lendist_make_job_name(job, filename);
			}
			return plot_config;
		}
		,ana_quick_make_selection: function()
		{
			// Level 3
			var select_read_count_type = {
				"config":
				{
					"child_type": "select"
					,"title":"" //section title not to show, show in content
					,"content": "ReadCountType: "
					,"attr": {"class": "read_count_type quick_ana_l3"}
				}
				,"Prefix + Perfect": "GMPM"
				,"Only Perfect": "GM"
				,"Only Prefix": "PM"
			};
			
			// if seq type is Normal, there is tailing ratio option
			var select_read_count_type2 = jQuery.extend(true, {}, select_read_count_type);
			select_read_count_type2["Tailing Ratio"] = "TailingRatio";
			
			var select_length_type = {
				"config":
				{
					"child_type": "select"
					,"title":""
					,"content": "LengthType: "
					,"attr": {"class": "length_type quick_ana_l3"}
				}
				,"Full length": "full"
				,"Prefix length": "prefix"
			};
			var select_ana_seq_type = {
				"config":
				{
					"child_type": "select"
					,"title":""
					,"content": "AnaSeqType: "
					,"attr": {"class": "ana_seq_type quick_ana_l3"}
				}
				,"First-1nt": "First-1nt"
				,"First-2nt": "First-2nt"
				,"Last-1nt": "Last-1nt"
				,"Last-2nt": "Last-2nt"
				,"Tail": "Tail"
				,"seed": "seed"
			};
			var select_z_value = {
				"config":
				{
					"child_type": "select"
					,"title":""
					,"content": "Z: "
					,"attr": {"class": "z_value quick_ana_l3"}
				}
			};
			var button_draw_and_show = {
				"config":
				{
					"child_type": "button"
					,"attr": {"class":"quick_draw_button"}
					,"title": ""
				}
				,"Draw and Show":{
					"config":{
						"bind": {
							"click": this.ana_quick_draw
						}
					}
				}
			};
			
			// Level 2
			var ratio = {
				"config":
				{
					"child_type": "section"
					,"attr": {"class": "ratio quick_ana_l2"}
				}
				,"ReadCountType": select_read_count_type2
				,"Draw and Show": button_draw_and_show
			};
			var lendist = {
				"config":
				{
					"child_type": "section"
					,"attr": {"class": "lendist quick_ana_l2"}
				}
				,"ReadCountType": select_read_count_type2
				,"LengthType": select_length_type
				,"Draw and Show": button_draw_and_show
			}
			var d_ratio = {
				"config":
				{
					"child_type": "section"
					,"attr": {"class": "d_ratio quick_ana_l2"}
				}
				,"AnaSeqType": select_ana_seq_type
				,"ReadCountType": select_read_count_type
				,"Species": select_z_value
				,"Draw and Show": button_draw_and_show
			};
			var d_lendist = {
				"config":
				{
					"child_type": "section"
					,"attr": {"class": "d_lendist quick_ana_l2"}
				}
				,"AnaSeqType": select_ana_seq_type
				,"ReadCountType": select_read_count_type
				,"LengthType": select_length_type
				,"Species": select_z_value
				,"Draw and Show": button_draw_and_show
			}
			var d_lenseq = {
				"config":
				{
					"child_type": "section"
					,"attr": {"class": "d_lenseq quick_ana_l2"}
				}
				,"AnaSeqType": select_ana_seq_type
				,"ReadCountType": select_read_count_type
				,"LengthType": select_length_type
				,"Length": select_z_value
				,"Draw and Show": button_draw_and_show
			}
			
			// Level 1
			var biotype_summary = {
				"config":
				{
					"child_type": "accordion"
					,"attr": {"class": "biotype_summary quick_ana_l1 "}
				}
				,"Subtype Expression Overview": ratio
				,"Subtype Length-specific Expression": lendist
			}
			var miRNA_summary = {
				"config":
				{
					"child_type": "accordion"
					,"attr": {"class": "mirna_summary quick_ana_l1"}
				}
				,"Species Expression Overview": ratio
				,"Species Length-specific Expression": lendist
			}
			var biotype_sequence_feature = {
				"config":
				{
					"child_type": "accordion"
					,"attr": {"class": "biotype_sequence_feature quick_ana_l1 "}
				}
				,"Sequence Feature Expression Overview": d_ratio
				,"Sequence Feature Length-specific Expression": d_lendist
				,"Sequence Feature Annotation-specific Expression": d_lenseq
			}
			var miRNA_sequence_feature = {
				"config":
				{
					"child_type": "accordion"
					,"attr": {"class": "mirna_sequence_feature quick_ana_l1"}
				}
				,"Sequence Feature Expression Overview": d_ratio
				,"Sequence Feature Length-specific Expression": d_lendist
				,"Sequence Feature Annotation-specific Expression ": d_lenseq
			}
			
			cs.menu.create_menu($(".ana_biotype_summary"), biotype_summary);
			cs.menu.create_menu($(".ana_microRNA_summary"), miRNA_summary);
			cs.menu.create_menu($(".ana_biotype_sequence_feature"), biotype_sequence_feature);
			cs.menu.create_menu($(".ana_mirna_sequence_feature"), miRNA_sequence_feature);
			
			//console.log($(".menu_quick_selection").find(".quick_draw_button"));
			
			var thisA = this;
			// change事件觸發，此時 plot table 已經讀取完畢，不需要放進 callback 執行
			$(".menu_quick_selection select").bind("change", function(){
				if($(this).parents(".z_value").length == 0)
					thisA.ana_quick_make_selection_z_option();
			});
			
		}
		// quick selection 製作 Z value 的選單
		,ana_quick_make_selection_z_option: function()
		{
			var thisA = this;
			var get_plot_table_value = function(obj, config, idx, toKey){
				var key = thisA.config["ana_plot_selection_fields_name"][idx];
				//["Sample", "Table", "Analyzor", "AnaSeqType", "LengthType", "ReadCountType", "AnnoBiotype", "GeneNameDetail"
				//, "SysType", "Filter", "Database", "DB_field", "Z-Type", "XvsYType", "Z", "CountType"]
				if(toKey == key)
					return obj;
				if(obj[config[key]] == undefined)
				{
					console.error("Not found! "+ key + ":" + config[key] + " in ", obj);
					return {"No such file": "No such file"};
					//return obj;
				}
				return get_plot_table_value(obj[config[key]], config, idx+1, toKey);
			}
			// 製作基本的 Z value select 選項，必須等 plot table 讀完
			$(".menu_quick_selection").find(".quick_draw_button").each(function(){
				var config = thisA.ana_quick_draw(undefined, $(this));
				var zvalue_obj = $(this).parents(".quick_ana_l2").find(".z_value select");
				zvalue_obj.html("");
				if(zvalue_obj.length != 0)
				{
					var z_list = get_plot_table_value(thisA.ana_lendist_option_table, config, 0, "Z")
					var html = "";
					for(var key in z_list)
					{
						html += "<option value='"+ key +"'>";
						html += key;
						html += "</option>";
					}
					zvalue_obj.append(html);
				}
			});
		}
		,ana_load_table: function(file, graph_type)
		{
			console.log(file);
			if(!file) console.error("Error! file is not set");
			if(!graph_type) console.error("Error! graph_type is not set");
			
			var job = new Job(cs, "Subject", file, "");
			if(graph_type) 
				job.d3_graph_type = graph_type;
			job.init();
			cs.jobq.add_job(job, job.loader);
			return job;
		}
		// heterogeneity
		,ana_miRheterogenity_draw: function()
		{
			var genome = cs.global_config.current_genome;
			var sample = $(this.config.analysis_mirna_heterogeneity_sample_jSelect).last().val();
			var graph_type = $(this.ana_mirna_heterogeneity_graph_type_jSelect).val();
			var filename = genome + "/" + sample + "/heterogeneity/Table.heterogeneity.2.-1.1.0.tsv";
			//this.ana_load_table(filename, $(this.config.analysis_mirna_heterogeneity_graph_type_jSelect).val());
			var job = this.ana_load_table(filename, $(this.config.analysis_mirna_heterogeneity_graph_type_jSelect).val());
			this.ana_miRheterogenity_make_job_name(job, filename);
		}
		,ana_miRheterogenity_make_job_name: function(job, file)
		{
			var t = file.split("/");
			var sample = t[0];
			var tmp = t.pop();
			var file_name = tmp;
			tmp = tmp.split("."); // Table.heterogeneity.2.1.1.0.tsv
							      // 0          1
			// long name:  Table.Sample-A.Heterogeneity
			// short name: Table.Sample-A.Heterogeneity
			var l_name = sample + ".miRNA.Heterogeneity";
			var s_name = l_name;
			
			var conf = {
				"sample": sample,
				"file_name": file_name,
				"l_name": l_name,
				"s_name": s_name,
				"subject": "HET_" + job.serial_number,
				"description": s_name,
				"information": "Heterogeneity"
			};
			job.update_info(conf);
			//job.set_parameter(conf);
		}
		,ana_sequence_logo_option_table: {}
		,ana_sequence_logo_make_selection_z_option: function()
		{
			if(!cs.global_config.current_genome)
				return;
			var genome = cs.global_config.current_genome;
			
			var thisA = this;
			//sequence_logo_selection
			$.post(cs.csaltdock, {"cmd":"get_analysis_table_list", "ana_type":"sequence_logo", "genome": genome}, function(d){
				var sample = $(thisA.config.analysis_mirna_sequence_logo_sample_jSelect).val();
				thisA.ana_sequence_logo_option_table = $.parseJSON(d);
				//console.log(thisA.ana_sequence_logo_option_table, sample, thisA.config.analysis_mirna_sequence_logo_sample_jSelect);
				if(thisA.ana_sequence_logo_option_table[sample] && thisA.ana_sequence_logo_option_table[sample]["Table"])
				{
					var list = thisA.ana_sequence_logo_option_table[sample]["Table"]["SequenceLogo"]["First-15nt"]["miRNA"]["detail"][2][1][0][0];
					$(thisA.config.analysis_mirna_sequence_logo_mirna_jSelect).html( cs.fmt.make_option_html(list, "key", "key") );
				}
			});
		}
		,ana_sequence_logo_draw: function()
		{
			var genome = cs.global_config.current_genome;
			//Table.SequenceLogo.First-15nt.miRNA.detail.2.1.0.0.MIR101-1.height_matrix
			var sample = $(this.config.analysis_mirna_sequence_logo_sample_jSelect).last().val();
			var mirna = $(this.config.analysis_mirna_sequence_logo_mirna_jSelect).last().val();
			var matrix = $(this.config.analysis_mirna_sequence_logo_matrix_jSelect).last().val();
			var filename = genome + "/" + sample + "/sequence_logo/Table.SequenceLogo.First-15nt.miRNA.detail.2.1.0.0." + mirna + "." + matrix;
			console.log(filename);
			var job = this.ana_load_table(filename, "SequenceLogo");
			this.ana_sequence_logo_make_job_name(job, filename);
		}
		,ana_sequence_logo_make_job_name: function(job, file)
		{
			var t = file.split("/");
			var sample = t[0];
			var tmp = t.pop();
			var file_name = tmp;
			tmp = tmp.split("."); // Table.SequenceLogo.First-15nt.miRNA.detail.2.1.0.0.MIR22HG.pwm
								  //   0          1         2        3       4  5 6 7 8  9       10
			var l_name = sample + "." + tmp[9] + ".miRNA.SequenceLogo";
			var s_name = l_name;
			
			var conf = {
				"sample": sample,
				"file_name": file_name,
				"l_name": l_name,
				"s_name": s_name,
				"subject": "LOGO_" + job.serial_number,
				"description": s_name,
				"information": tmp[9] + ".SequenceLogo"
			};
			job.update_info(conf);
		}
		,add_new_job: function()
		{
		
			$(".job_info_update").hide();
			$(".job_info_create").show().unbind("click").bind("click", function(e){
				var subject = $(".job_info_subject").last().val();
				if(subject == "")
				{
					subject = "NoName";
				}
				var description = $(".job_info_description").last().val();
				var information = $(".job_info_information").last().val();
				var d3_graph_type = $(".job_info_graph_type").last().val();
				
				var job = new Job(cs, subject, "", description, d3_graph_type);
				job.information = information;
				job.init();
				cs.jobq.add_job(job, job.loader);
				
				$("#jMenu_dialog").data("display_type", "closed").dialog("close");
				e.stopPropagation();// 不做 document click 事件
			});
			
			
		}
	});
});
