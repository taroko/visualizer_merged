define(["dojo/_base/declare", "cxdlib/libcontrol_panel_item"], function(declare, Cp_item) {
	return declare("ControlPanel", null,
	{
		data: 
		{
			"selection_detail_pairs": {}
			, "config": {}
		}
		,parent_obj_sel_name: "#jMenu_dialog"
		,name: 
		{
			"control_selection": "Control Selection"
			,"control_detail": "Control Detail"
		}
		,constructor: function(parent_obj_sel_name)
		{
			if(parent_obj_sel_name) this.parent_obj_sel_name = parent_obj_sel_name;
			//console.log("ControlPanel init, parent_obj_id ", this.parent_obj_sel_name);
			this.add_selection_pair("Conf-1", "cp_d_conf_1");
			this.add_selection_pair("Conf-2", "cp_d_conf_2");
			
			this.add_control_item("cp_d_conf_1", "itemid_1", "text", {"attr": {"type":"text", "value": "TEST2"}});
		}
		///@brief name=顯示出來的名字，detail_id=要顯示的內容dom (div)
		,add_selection_pair: function(name, detail_id)
		{
			this.data.selection_detail_pairs[name] = detail_id;
			this.data.config[detail_id] = {};
		}
		,add_control_item: function(detail_id, item_id, item_type, conf)
		{
			//if(!conf.attr) return;
			//if(!conf.attr.id) return;
			//var item_id = conf.attr.id;
			this.data.config[detail_id][item_id] = new Cp_item(detail_id, item_id, item_type, conf);
			console.log(this.data.config[detail_id][item_id]);
		}
		,make_control_panel_config: function()
		{
			var div_selection_name = "<div>"+ this.name.control_selection +"</div>";
			var div_detail_name = "<div>"+ this.name.control_detail +"</div>";
			var obj = {};
			obj["config"] = 
			{
				"child_type": "section"
			}
			obj[div_selection_name] =
			{
				"config":
				{
					"t_attr":
					{
						"class": "control_panel_selection control_panel_left"
					}
					,"child_type": "selectable"
				}
			}
			obj[div_detail_name] =
			{
				"config":
				{
					"t_attr":
					{
						"class": "control_panel_detail control_panel_right"
					}
					,"child_type": "selectable"
				}
			}
			
			for(var key in this.data.selection_detail_pairs)
			{
				var selection_name = key;
				var selection_id = this.data.selection_detail_pairs[key];
				obj[div_selection_name][ selection_name ] = { "config":{ "attr":{} } };
				obj[div_selection_name][ selection_name ]["config"]["attr"]["control_panel_detail_target"] = selection_id;
			}
			
			return obj;
			// 為了產生向下列 config
			//var obj = 
			//{
			//	"config":
			//	{
			//		"child_type": "section"
			//	}
			//	,"<div>"+ this.name.control_selection +"</div>":
			//	{
			//		"config":
			//		{
			//			"t_attr":
			//			{
			//				"class": "control_panel_left"
			//			}
			//			,"child_type": "selectable"
			//			
			//		}
			//		//,"Conf-1": { "config":{ "attr":{"control_panel_detail_target": "target_name"} }}
			//		//,"Conf-2": {}
			//		//,"Conf-3": {}
			//		//,"Conf-4": {}
			//		//,"Conf-5": {}
			//		//,"Conf-6": {}
			//		//,"Conf-7": {}
			//	}
			//	,"<div>Control Detail</div>":
			//	{
			//		"config":
			//		{
			//			"t_attr":
			//			{
			//				"class": "control_panel_right"
			//			}
			//			,"child_type": "section"
			//		}		
			//	}
			//}
			
		}
		,control_panel_general_select_event: function()
		{
			///@brief 選擇自定義 control_panel_selection 裡面可選取物件（左邊選取欄）
			var selectable = $(this.parent_obj_sel_name).find(".control_panel_selection .selectable");
			///@brief 賦予選擇事件，改變右邊欄位內容
			selectable.each(function() {
				$(this).selectable({
					selected: function() {
						var target_id = $(this).find(".ui-selected").last().attr("control_panel_detail_target");
						//console.log( target_id )
						var control_panel_detail_item = $(this).parents(".control_panel_selection").next().find(".section_content");
						control_panel_detail_item.html( $("#"+target_id).html() );
						//console.log( $(this).parent(".control_panel_selection") );
						//console.log(control_panel_detail_item);
					}
				});
			});
		}
		///@brief 讓使用者點擊 menu 中 control 按鈕，就出現設定視窗
		,header_control_click_event: function()
		{
			var thisA = this;
			$("#header-control").bind("click", function(){
				//$("#control_panel").hide();
				$("#control_panel").find(".jMenu_dialog_button").bind("click", function(){
					control_panel.control_panel_general_select_event();
				});
				var dialog = $("#jMenu_dialog");
				
				// 如果正常按鈕已經開起來dialg，然後使用者在按Control button (上方大按鈕)，就不點擊 button，只把dialbo位置移動到大按鈕下面
				if(dialog.data("display_type") != "opened")
					$("#control_panel").find(".jMenu_dialog_button").first().click();
				// 移動 dialgo 位置
				$("#jMenu_dialog").dialog({
					position:
					{
						my: "right top",
						at: "right bottom",
						of: $("#header-control")	
					}
				});
				// control_panel 為 golbal 變數，設定選擇control事件，會改變右邊 detail 內容
				thisA.control_panel_general_select_event();
			});
		}
		
	});
});















