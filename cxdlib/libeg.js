define(["dojo/_base/declare", "dojo/dom"], function(declare, dom) {
	return declare("Editgrid", null, {
		homename: null,
		grid: null,
		book: null,
		main_sheet: null,
		layout: null,
		sessionKey: 'SThKIlZf8itt7jMEilwHLHBljuI',

		constructor: function(name) {
			this.homename = name;
		},
		open: function() {
			this.grid = new editgrid.Grid({ sessionKey: this.sessionKey, suppressSessionKeyWarning: 1 });
			this.layout = this.grid.getStandardLayout();
			this.layout.doLayout(dom.byId(this.homename), this.grid);
			this.grid.openBook({ path: '/user/thestyle/demo' });
		},
		delay_init: function() {
			this.book = this.grid.getWorkbook();
			this.main_sheet = this.book.getSheets()[0];
			this._grid = this.grid.getCursor()._grid;
		},
		get_grid: function() {
			return this.grid;
		},
		get_sheet: function() {
			// Same as cs.eg.grid.getActiveSheet(), but has no global `grid` bug
			return this.grid.view.sheetView.sheet;
		},
		data2sheet: function(data, sheet) {
			var row = data.length - 1;
			var col = data[0].length - 1;
			var range = sheet.getRange([0, 0, row, col]);
			range.setTexts(data);
			return range;
		},
		highlight: function(row, col, sel_col, color, sheet) {
			// TODO: Highlight the row, correspoding to the d3 chart, in sheet and highlight the specify cell (row, col)

			// editgrid is a gloal variable

			// check the sheet is on the top view. If is not, then we can ignore highlight.
			if (sheet != this.get_sheet()) return;
			var color_line = color;
			var color_cell = (color_line == null)? null : color - 0x005050;
			sheet.applyStyle(editgrid.RangeRef.fromArray([row+1, sel_col.start, row+1, sel_col.end]), editgrid.Style.fromHash({backColor: color_line}));
			// +1 because the cell (0, 0) in d3 is the cell (1, 1) in sheets. In sheet, row-0 notes names, col-0 notes items
			if (col != undefined)
				sheet.applyStyle(editgrid.RangeRef.fromArray([row+1, col+1, row+1, col+1]), editgrid.Style.fromHash({backColor: color_cell}));
		},
		freeze_control: function() {
			this.grid.getCursor().moveTo("A2");
			this.grid.view.toggleFreezePanes();
		}
	});
});


/* example code temporary space */
/*
** set text to cell
eg.sheet.getCell("B2").setText("CC");

** listen to event
eg.sheet.addOnValueChangeListener(
"J1", function() {
cs.jb.jbrowse.plugins.JHH.jhh_navi(eg.sheet.getCell("J1").getText());
});

** marco-like action
eg.grid.addAction('egtest', {'script': function() {alert("egtest");}});
*/
