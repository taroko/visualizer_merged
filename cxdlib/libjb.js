define(["JBrowse/Browser", "dojo/io-query", "dojo/json", "dojo/_base/declare", 'JBrowse/View/InfoDialog'], function(Browser, ioQuery, JSON, declare, InfoDialog) {
	return declare("JB", null, {
		container: null,
		constructor: function(name) {
			this.container = name;
		}
		,bookmarks: {
			"idx": 0,
			"data": {}
		}
		,genome_changer_container_id: "#genome_changer"
		,genome_changer: function()
		{
			var thisA = this;
			
			
			//$.post("bin/csaltdock-visualizer.php",{"cmd":"get_jbrowse_config"}, function(d){
			//	var obj = JSON.parse(d);
			//	if(!obj.datasets) return;
				var html = "";
				var obj = thisA.jbrowse.config;
				cs.genome_info = obj.datasets;

				for(var key in obj.datasets)
				{
					html += '<a class="genome_changer_link" onclick="cs.change_genome(\'' + key + '\')" href="#">'+obj.datasets[key].name+"</a><br />";
					//html += "<a class='genome_changer_link' href='"+obj.datasets[key].url+"'>"+obj.datasets[key].name+"</a><br />";
				}
				$(thisA.genome_changer_container_id).html(html);
			//})
			//.fail(function() {console.log( "Error, connect!" );})
			//.always(function(){});;
		}
		,open: function() {
			var queryParams = ioQuery.queryToObject( window.location.search.slice(1) );
			var user = "";
			if(queryParams["user"]) user = queryParams["user"];
			
			var config = {
				containerID: this.container,
				refSeqs: "jbrowse/" + queryParams.data + "/seq/refSeqs.json",
				nameUrl: "jbrowse/" + queryParams.data + "/names/meta.json",
				dataRoot: queryParams.data,
				baseUrl: "jbrowse/",
				queryParams: queryParams,
				location: queryParams.loc,
				forceTracks: queryParams.tracks,
				initialHighlight: queryParams.highlight,
				show_nav: queryParams.nav,
				show_tracklist: queryParams.tracklist,
				show_overview: queryParams.overview,
				stores: { url: { type: "JBrowse/Store/SeqFeature/FromConfig", features: [] } },
				makeFullViewURL: function( browser ) {
				
				// the URL for the 'Full view' link
				// in embedded mode should be the current
				// view URL, except with 'nav', 'tracklist',
				// and 'overview' parameters forced to 1.
					return browser.makeCurrentViewURL({ nav: 1, tracklist: 1, overview: 1 });
				},
				updateBrowserURL: true,
				datasets: {
					"mm10":{
						"url":"?data=jbrowse_db/json/mm10&user="+user,
						"name":"mm10"
					},
					"hg19":{
						"url":"?data=jbrowse_db/json/hg19&user="+user,
						"name":"hg19"
					},
					"danRer6":{
						"url":"?data=jbrowse_db/json/danRer6&user="+user,
						"name":"danRer6"
					},
					"dm3":{
						"url":"?data=jbrowse_db/json/dm3&user="+user,
						"name":"dm3"
					},
					"ce10":{
						"url":"?data=jbrowse_db/json/ce10&user="+user,
						"name":"ce10"
					}
				}
			};
		   //if there is ?addFeatures in the query params,
		   //define a store for data from the URL
		   if( queryParams.addFeatures ) {
			   config.stores.url.features = JSON.parse( queryParams.addFeatures );
		   }

		   // if there is ?addTracks in the query params, add
		   // those track configurations to our initial
		   // configuration
		   if( queryParams.addTracks ) {
			   config.tracks = JSON.parse( queryParams.addTracks );
		   }

		   // if there is ?addStores in the query params, add
		   // those store configurations to our initial
		   // configuration
		   if( queryParams.addStores ) {
			   config.stores = JSON.parse( queryParams.addStores );
		   }

		   // create a JBrowse global variable holding the JBrowse instance
		   this.jbrowse = new Browser( config );
		   
		   this.load_bookmarks();
		   this.genome_changer();
		},
		redraw: function() {
			//this.jbrowse.view.setPosition(this.jbrowse.view.getPosition());
			this.jbrowse.view.elem.scrollLeft = this.jbrowse.view.x;
		},
		get_view_tracks: function() {
			var visible_tracks = this.jbrowse.view.tracks; // array type
			var tracks = [];
			
			for (t in visible_tracks) {
				if (visible_tracks[t].config) {
					var track = {};
					track[visible_tracks[t].config.key] = visible_tracks[t];
					tracks.push(track);
				}
			}
			
			return tracks;
		},
		hide_all_tracks: function() {
			var vistkobj = this.jbrowse.view.visibleTracks();
			var hidetkobj = [];

			for (var i = 0; i < vistkobj.length; i++) {
				hidetkobj.push(vistkobj[i].config);
			}

			this.jbrowse.view.hideTracks(hidetkobj);
		},
		show_tracks: function(tracksToShow) {
			this.jbrowse.showTracks(tracksToShow);
		},
		tell_accessor: function() {
			// XXX DEPRECATED, using get_snapshot(...) instead

			var stat = {};
			var queryParams = ioQuery.queryToObject( window.location.search.slice(1) );

			stat.loc = queryParams.loc;
			stat.tracks = queryParams.tracks;

			return stat;
		},
		recall_accessor: function(stat) {
			// XXX DEPRECATED, using get_snapshot(...) instead

			// These order is important. After recoverying tracks then navigating.
			this.hide_all_tracks();
			this.show_tracks(stat.tracks);

			//this.jbrowse.navigateTo(stat.loc);//can not work ?????
			var ref = stat.loc.split(":")[0];
			var start = stat.loc.split(":")[1].split("..")[0];
			var end = stat.loc.split(":")[1].split("..")[1];
			this.jbrowse.navigateToLocation({"ref":ref, "start":start, "end":end} );
		},
		get_snapshot: function()
		{
			var snapshot = {};

			var queryParams = ioQuery.queryToObject( window.location.search.slice(1) );
			
			snapshot.loc = queryParams.loc;
			snapshot.tracks = queryParams.tracks;

			return JSON.stringify(snapshot);
		},
		set_snapshot: function(snapshot)
		{
			//this.hide_all_tracks();
			
			if (!snapshot)
				return;

			snapshot = JSON.parse(snapshot);
			
			this.show_tracks(snapshot.tracks);

			var loc_info = snapshot.loc.split(":");
			loc_info[1] = loc_info[1].split("..");

			var ref = loc_info[0];
			var start = loc_info[1][0];
			var end = loc_info[1][1];
			this.jbrowse.navigateToLocation({"ref":ref, "start":start, "end":end} );
		},
		goto_bookmark: function(stat){
			var current_stat = this.tell_accessor();
			var current_tracks = current_stat.tracks.split(",");
			var tracks = stat.tracks.split(",");
			for(var i in tracks)
			{
				// 如果現在 track 沒有顯示，就顯示 track
				if(current_tracks.indexOf(tracks[i]) == -1)
					this.show_tracks(tracks[i]);
			}
			if(current_stat.loc != stat.loc) // 有 bug 記錄和顯示會略有不同
			{
				console.log(current_stat.loc , stat.loc);
				var ref = stat.loc.split(":")[0];
				var start = stat.loc.split(":")[1].split("..")[0];
				var end = stat.loc.split(":")[1].split("..")[1];
				this.jbrowse.navigateToLocation({"ref":ref, "start":start, "end":end} );
			}
		},
		add_bookmark: function(){
			var thisA = this;
			var stat = thisA.tell_accessor();
			
			var callback = function(name) {
				stat.name = name;
				thisA.bookmarks.data["Bookmark_"+thisA.bookmarks.idx] = JSON.stringify(stat);
				thisA.save_bookmarks();
				thisA.update_bookmarks_html();
				thisA.bookmarks.idx++;
			}
				
			new InfoDialog(
			{
				title: 'Success!',
				content: '<span class="locString"><br />Add bookmark '+stat.loc+'</span><br />Name: <input class="new_name" type="text" value="Bookmark_'+thisA.bookmarks.idx+'"/>',
				className: 'notfound-dialog',
				onClick:function(){
					if(this.get('open')) return;
					var new_name = $(this.containerNode).find("input.new_name").val();
					callback(new_name);
				}
			}).show();
		},
		delete_bookmarks: function(idx) {
			delete this.bookmarks.data[idx];
			this.save_bookmarks();
			this.update_bookmarks_html();
			console.log("delete", idx);
		},
		save_bookmarks: function(){
			var bookmark_data = JSON.stringify(this.bookmarks);
			$.post(cs.csaltdock, {"cmd":"jb_save_bookmarks", "genome": cs.global_config.current_genome, "data": bookmark_data}, function(d){
				console.log("save bookmarks success");
			});
		},
		load_bookmarks: function(){
			var thisA = this;
			$.post(cs.csaltdock, {"cmd":"jb_load_bookmarks", "genome": cs.global_config.current_genome}, function(d){
				thisA.bookmarks = JSON.parse(d);
				thisA.update_bookmarks_html();
				thisA.bookmarks.idx++;
				console.log("load bookmarks success", thisA.bookmarks);
			});
		},
		rename_bookmarks: function(key, name)
		{
			var obj = JSON.parse(this.bookmarks.data[key]);
			obj.name = name;
			this.bookmarks.data[key] = JSON.stringify(obj);
			console.log(this.bookmarks.data[key], key, name);
			this.save_bookmarks();
			this.update_bookmarks_html();
		},
		update_bookmarks_html: function()
		{
			var thisA = this;
			var bookmark_id = "#bookmark_list";
			$(bookmark_id).html("");
			
			for(var key in thisA.bookmarks.data)
			{
				var obj = JSON.parse(thisA.bookmarks.data[key]);
				
				///@brief 如果 o.name 沒有設定，那就使用預設 key
				var bookmark_name = obj.name ? obj.name : key;
				
				var jItem = $(bookmark_id).append("<div class='bookmark_list bookmark_"+key+"'></div>").children().last();
				
				var item = {};
				
				///@brief bookmark delete item 賦予click事件，點擊會刪除
				item = jItem.append("<div class='img_div_btn bookmark_list_delete'></div>").children().last();
				(function(name){
					item.bind("click", function() {
						thisA.delete_bookmarks(name);
						$(".bookmark_"+name).hide(500);
					});
				})(key);
				
				///@brief bookmark delete item 賦予click事件，點擊重新命名
				item = jItem.append("<div class='img_div_btn bookmark_list_rename'></div>").children().last();
				(function(o, k){
					item.bind("click", function() {
						///@brief 如果 o.name 沒有設定，那就使用預設 key
						var bm_name = o.name ? o.name : k;
						
						$(".bookmark_list").find("input").hide();
						if($(".bookmark_"+k).find("input").length != 0)
						{
							$(".bookmark_"+k).find("input").show();
							return;
						}
						var rn_br = $(".bookmark_"+k).append("<br />").children().last();
						var rn_text = $(".bookmark_"+k).append("<input type='text' value='"+bm_name+"' style='margin-left:10px;' />").children().last();
						var rn_btn = $(".bookmark_"+k).append("<input type='button' value='Rename'/>").children().last();
						rn_btn.bind("click", function(){
							console.log("Rename", k, rn_text.val());
							
							if(rn_text.val())
							{
								thisA.rename_bookmarks(k, rn_text.val());
								//暫存更改，dialog 是暫存物件
								$(".bookmark_"+k).find(".bookmark_list_link").html(rn_text.val()+" ("+o.loc+")");
								o.name = rn_text.val();
							}
							$(".bookmark_list").find("input").hide();
						});
					});
				})(obj, key);
				
				///@brief bookmark main and click event 賦予click事件，點擊會改變 jb location and tracks
				item = jItem.append("<div class='bookmark_list_link' title='"+obj.loc+"'>"+bookmark_name+" ("+obj.loc+")</div>").children().last();
				(function(o){
					item.bind("click", function() {
						thisA.goto_bookmark(o);
					});
				})(obj);
				
			}
		},
		click_track_select_tab: function()
		{
			$(".faceted_tracksel_on_off").first().click();
		},
		set_title: function(title)
		{
			var $container = $("#" + this.container);
			$container.dialog({ "title": "GenomeBrowser - " + title });
		},
		append_db_select: function()
		{
			if (!cs.user)
				return;

			if ($(".jbrowseStandaloneDatasetSelector").length > 0)
			{
				$items = $(".jbrowseStandaloneDatasetSelector").children("ul").last().children();
				$items.each(function() {
					var $a = $(this).children("a");

					var new_href = $a.attr("href") + "&user=" + cs.user;
					$a.attr("href", new_href);
				});
			}
		}
	});
});
