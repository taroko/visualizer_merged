

define(["dojo/_base/declare", "dojo/_base/lang", "d3_graph/src/jquery.md5.js"], function(declare, lang, md5) {
	return declare("Menu", null,
	{
		constructor: function(jMenu, items)
		{
			this.items = this.default_config;
			if(jMenu)
			{
				this.jMenu = jMenu;
			}
			if(items)
			{
				merge_config(this.items, items);
			}
			this._create_menu(jMenu, this.items);
			
		}
		,dialog_auto_hide: true
		,dialog_dont_close_obj: []
		,create_menu: function(jMenu, items)
		{
			this._create_menu(jMenu, items);
		}
		,default_config :
		{
			"config": 
			{
				"child_type": "tabs"
			}
			// default analysis
			// quick selection
			,"<span class='header-obj-text'><img class='l1_icons' src='css/icons/png/analysis.png'>Analysis</span>":
			{
				"config":
				{
					"attr":
					{
						"id": "menubar_l1_analysis"
						,"class": "menubar_level1"
					}
					,"child_type": "dialog"
				}
				,"Basic analysis":
				{
					"config":
					{
						"attr":
						{
							"id": "menubar_l2_global"
							,"class": "menu_quick_selection"
						}
						,"child_type": "section"
					}
					,"Select Sample":
					{
						"config":
						{
							"child_type": "select"
							,"attr": 
							{
								"class": "ana_sample"
							}
							,"title":""
							,"content":"Select Sample: "
						}
					}
					,"Select Type":
					{
						"config":
						{
							"child_type": "accordion"
							,"attr": 
							{
								"id": "menu_sample"
							}
							,"title": ""

						}
						,"Biotype Summary Analysis":
						{
							"config":
							{
								"child_type": "accordion"
								,"attr": {
									"class": "ana_biotype_summary"
								}
							}
						}
						,"Biotype Sequence Feature Specific Analysis":
						{
							"config":
							{
								"attr": 
								{
									"class": "ana_biotype_sequence_feature"
								}
								,"child_type": "section"
							}
						}
						,"microRNA Summary Analysis":
						{
							"config":
							{
								"attr": 
								{
									"class": "ana_microRNA_summary"
								}
								,"child_type": "section"
							}
						}
						,"microRNA Sequence Feature Specific Analysis":
						{
							"config":
							{
								"attr": 
								{
									"class": "ana_mirna_sequence_feature"
								}
								,"child_type": "section"
							}
						}
					}
				}
				,"Advanced analysis":
				{
					"config":
					{
						"attr":
						{
							"id": "menubar_l2_analysis"
							,"class": "menubar_level2"
						}
						,"child_type": "accordion"
					}
					,"Length Distribution":
					{
						"config":
						{
							"attr":{}
							,"child_type": "section"
						}
						,"Analysis for Length distribution":
						{
							"config":
							{
								"attr":
								{
									"id": "plot_selection"
								}
							}
						}
					}
					,"MicroRNA Heterogeneity":
					{
						"config":
						{
							"attr":{}
							,"child_type": "section"
							
						}
						,"Select Graph Type":
						{
							"config":
							{
								"child_type": "select"
								,"attr": 
								{
									"class": "ana_graph ana_mirna_heterogeneity_graph_type"
								}
								,"title":""
								,"content":"Select Draw Type: "
							}
						}
						,"Select sample":
						{
							"config":
							{
								"child_type": "select"
								,"attr": 
								{
									"class": "ana_sample ana_mirna_heterogeneity_sample"
								}
								,"title":""
								,"content":"Select Sample: "
							}
						}
						,"Draw":
						{
							"config":
							{
								"child_type": "button"
								,"title": ""
							}
							,"Draw": 
							{
								"config":
								{
									"attr":
									{
										"id": "ana_mirna_heterogeneity_draw"
									}
									,"bind":
									{
										"click": function()
										{
											cs.analyzer.ana_miRheterogenity_draw();
										}
									}
								}
							}
						}
					}
					,"MicroRNA SequenceLogo":
					{
						"config":
						{
							"attr":{
								"id": "sequence_logo_selection"
							}
							,"child_type": "section"	
						}
						,"Select sample":
						{
							"config":
							{
								"child_type": "select"
								,"attr": 
								{
									"class": "ana_sample ana_mirna_sequence_logo_sample"
								}
								,"title":""
								,"content":"Select Sample: "
							}
						}
						,"Select microRNA":
						{
							"config":
							{
								"child_type": "select"
								,"attr": 
								{
									"class": "ana_mirna ana_mirna_sequence_logo_mirna_list"
								}
								,"title":""
								,"content":"Select microRNA: "
							}
						}
						,"Select matrix":
						{
							"config":
							{
								"child_type": "select"
								,"attr": 
								{
									"class": "ana_matrix ana_mirna_sequence_logo_matrix"
								}
								,"title":""
								,"content":"Select Matrix: "
							}
							,"ppm": "ppm"
							,"pwm": "pwm"
						}
						,"Draw":
						{
							"config":
							{
								"child_type": "button"
								,"title": ""
							}
							,"Draw": 
							{
								"config":
								{
									"attr":
									{
										"id": "ana_mirna_sequence_logo_draw"
									}
									,"bind":
									{
										"click": function()
										{
											cs.analyzer.ana_sequence_logo_draw();
										}
									}
								}
							}
						}
					}
				}
				,"Sample Info":
				{
					"config":
					{
						"attr":
						{
							"id": "summary"
						}
						,"child_type": "section"
					}
					,"Summary":
					{
						"config":
						{
							"attr":
							{
								"id": "summary_content"
							}
						}
					}
				}
				,"New job":
				{
					"config":
					{
						"attr":
						{
							"id": "new_job"
						}
						,"child_type": "section"
						,"t_bind":
						{
							"click": function(){
								cs.analyzer.add_new_job();
							}
						}
					}
					,"Job info":
					{
						"config":
						{
							"attr": 
							{
								"class": "job_info"
							}
							,"content": "<div class='job_info_title'>Subject: </div><input type='text' class='job_info_subject'><br />\
							             <div class='job_info_title'>Description: </div><input type='text' class='job_info_description'><br />\
							             <div class='job_info_title'>GraphType: </div><div class='ana_graph job_info_inline'>\
							             	<select class='job_info_graph_type'></select>\
							             </div><br />\
							             <div class='job_info_title'>Information: </div><textarea class='job_info_information'> </textarea>"
						}
						
					}
					,"Update_btn":
					{
						"config":
						{
							"child_type": "button"
							,"title": ""
						}
						,"Update": 
						{
							"config":
							{
								"attr":
								{
									"class": "job_info_update"
								}
							}
						}
					}
					,"Create_btn":
					{
						"config":
						{
							"child_type": "button"
							,"title": ""
						}
						,"Create": 
						{
							"config":
							{
								"attr":
								{
									"class": "job_info_create"
								}
							}
						}
					}
				}
			}
			,"<span class='header-obj-text'><img class='l1_icons' src='css/icons/png/datasheet.png'>DataSheet</span>":
			{
				"config":
				{
					"attr": 
					{
						"id": "menubar_l1_jbrowse"
						,"class": "menubar_level1"
					}
					,"child_type": "dialog"
				}
				,"Import":
				{
					"config":
					{
						"attr":
						{
							"id": "menu_sheet_export"
							,"class": "menubar_level2"
						}
						,"t_bind": { "click": function(){$("#sc_import").click();} }
						,"t_data": {"disable_dialog":true}
						,"child_type": "section"
					}
				}
				,"Export":
				{
					"config":
					{
						"attr":
						{
							"id": "menu_sheet_export"
							,"class": "menubar_level2"
						}
						,"t_bind": { "click": function(){cs.sc.export_sheet();} }
						,"t_data": {"disable_dialog":true}
						,"child_type": "section"
					}
				}
			}
			,"<span class='header-obj-text'><img class='l1_icons' src='css/icons/png/gbrowser.png'>GenomeBrowser</span>":
			{
				"config":
				{
					"attr":
					{
						"id": "menubar_l1_tables"
						,"class": "menubar_level1"
					}
					,"child_type": "dialog"
				},
				"Tracks selection":
				{
					"config":
					{
						"attr":
						{
							"id": "jb_select_tracks",
							"class": "menubar_level2",
						}
						,"t_bind": { "click": function(){ cs.jb.click_track_select_tab(); } }
						,"t_data": {"disable_dialog":true}
						,"child_type": "section"
					}
				}
				,"Bookmark save":
				{
					"config":
					{
						"attr":
						{
							"id": "new_bookmark"
							,"class": "menubar_level2"
						}
						,"t_bind": { "click": function(){console.log("TEST Bookmark"); cs.jb.add_bookmark();} }
						,"t_data": {"disable_dialog":true}
						,"child_type": "section"
					}
				}
				,"Bookmark load":
				{
					"config":
					{
						"attr":
						{
							"id": "bookmark_list_btn"
							,"class": "menubar_level2"
						}
						,"t_bind": {"click": function(){ $(".bookmark_list").find("input").remove();/*debug*/ }}
						,"child_type": "section"
					}
					,"Bookmarks List":
					{
						"config":
						{
							"attr":
							{
								"id": "bookmark_list"
								,"class": "menubar_level3"
							}
						}
					}
				}
				,"Genomes selection":
				{
					"config":
					{
						"attr":
						{
							"id": "control_change_genome"
							,"class": "menubar_level2"
						}
						,"child_type": "section"
					}
					,"Change Genome":
					{
						"config":
						{
							"attr":
							{
								"id": "genome_changer"
							}
							,"title": ""
						}
					}
				}
			}
			,"<span class='header-obj-text'><img class='l1_icons' src='css/icons/png/plotter.png'>Plotter</span>":
			{
				"config":
				{
					"attr":
					{
						"id": "menubar_l1_plotter"
						,"class": "menubar_level1"
					}
					,"child_type": "button"
				}
				,"Column ++":
				{
					"config":
					{
						"attr": { "id": "menu_plotter_btn_cpp", "class": "menu_plotter_buttons" }
						,"bind": { "click": function(){cs.d3.do_museum_reorder("c", 1);} }
					}
				}
				,"Column --":
				{
					"config":
					{
						"attr": { "id": "menu_plotter_btn_cmm", "class": "menu_plotter_buttons" }
						,"bind": { "click": function(){cs.d3.do_museum_reorder("c", -1);} }
					}
				}
				// + -
				,"Row ++":
				{
					"config":
					{
						"attr": { "id": "menu_plotter_btn_rpp", "class": "menu_plotter_buttons" }
						,"bind": { "click": function(){cs.d3.do_museum_reorder("r", 1);} }
					}
				}
				,"Row --":
				{
					"config":
					{
						"attr": { "id": "menu_plotter_btn_rmm", "class": "menu_plotter_buttons" }
						,"bind": { "click": function(){cs.d3.do_museum_reorder("r", -1);} }
					}
				}
				
								
			}
			,"<span class='header-obj-text'><img class='l1_icons' src='css/icons/png/layout.png'>Layouts</span>":
			{
				"config":
				{
					"attr":
					{
						"id": "menubar_l1_layouts"
						,"class": "menubar_level1"
					}
					,"child_type": "button"
				}
				,"3w":
				{
					"config":
					{
						"attr": { "id": "menu_layout_btn_jb_h_sc_d3", "class": "menu_layout_buttons layouts_btn_3w" }
						,"bind": { "click": function(){cs.layouts("jb_h_sc_d3");} }
					}
				}
				,"3w2":
				{
					"config":
					{
						"attr": { "id": "menu_layout_btn_jb_v_sc_d3", "class": "menu_layout_buttons" }
						,"bind": { "click": function(){cs.layouts("jb_v_sc_d3");} }
					}
				}
				,"vscd3":
				{
					"config":
					{
						"attr": { "id": "menu_layout_btn_sc_v_d3", "class": "menu_layout_buttons" }
						,"bind": { "click": function(){cs.layouts("sc_v_d3");} }
					}
				}
				,"hjbsc":
				{
					"config":
					{
						"attr": { "id": "menu_layout_btn_jb_h_sc", "class": "menu_layout_buttons" }
						,"bind": { "click": function(){cs.layouts("jb_h_sc");} }
					}
				}
				,"hjbd3":
				{
					"config":
					{
						"attr": { "id": "menu_layout_btn_jb_h_d3", "class": "menu_layout_buttons" }
						,"bind": { "click": function(){cs.layouts("jb_h_d3");} }
					}
				}
				,"hscd3":
				{
					"config":
					{
						"attr": { "id": "menu_layout_btn_sc_h_d3", "class": "menu_layout_buttons" }
						,"bind": { "click": function(){cs.layouts("sc_h_d3");} }
					}
				}
				,"1wjb":
				{
					"config":
					{
						"attr": { "id": "menu_layout_btn_jb_a", "class": "menu_layout_buttons" }
						,"bind": { "click": function(){cs.layouts("jb_a");} }
					}
				}
				,"1wsc":
				{
					"config":
					{
						"attr": { "id": "menu_layout_btn_sc_a", "class": "menu_layout_buttons" }
						,"bind": { "click": function(){cs.layouts("sc_a");} }
					}
				}
				,"1wd3":
				{
					"config":
					{
						"attr": { "id": "menu_layout_btn_d3_a", "class": "menu_layout_buttons" }
						,"bind": { "click": function(){cs.layouts("d3_a");} }
					}
				}
			}
			,"<span class='header-obj-text'><img class='l1_icons' src='css/icons/png/snapshot.png'>Snapshot</span>":
			{
				"config":
				{
					"attr":
					{
						"id": "menubar_l1_snapshot"
						,"class": "menubar_level1"
					}
					,"child_type": "dialog"
				},
				"Save":
				{
					"config":
					{
						"child_type": "section",
					},
					"Snapshot setup":
					{
						"config":
						{
							"attr":
							{
								"class": "snapshot_l3"
							}
							, "content": 'Name: <input type="text" id="in_sp_name" class="in_sp_name_c" />'
						}
					},
					"Setup button":
					{
						"config":
						{
							"child_type": "button",
							"title": ""
						},
						"OK":
						{
							"config":
							{
								"attr":
								{
									"id": "btn_sp_ok"
								},
								"bind": {
									"click" : function(e)
									{
										cs.save_snapshot_btn(e);
									}
								}
							}
						}
					}
				},
				"Load":
				{
					"config":
					{
						"attr":
						{
							"id": "snapshot_list_btn"
							,"class": "menubar_level2"
						}
						,"t_bind": {"click": function(){
						//$(".bookmark_list").find("input").remove();/*debug*/ 
							console.log("sn list click");
						}}
						,"child_type": "section"
					}
					,"Snapshot List":
					{
						"config":
						{
							"attr":
							{
								"id": "snapshot_list"
								,"class": "menubar_level3"
							}
						}
					}
				}
			}
			,"<span class='header-obj-text'><img class='l1_icons' src='css/icons/png/control.png'>Control</span>":
			{
				"config":
				{
					"attr":
					{
						"id": "control_panel"
						
					}
					,"t_attr":
					{
						"id": "header-control"
					}
					,"child_type": "dialog"
				}
				//,"Control Panel":
				//{
				//	"config":
				//	{
				//		"attr":
				//		{
				//			"id": "control_panel_general"
				//			,"class": "control_panel_dialog menubar_level2"
				//		}
				//		,"child_type": "section"
				//	}
				//},
				,"Account":
				{
					"config":
					{
						"attr":
						{
							"id": "",
							"class": "menubar_level2"
						},
						"child_type": "section",
					},
					"Account Information":
					{
						"config":
						{
							"attr":
							{
								"class": "account_info"
							},
							"content": '<div class="account_ctx"></div>'
						},
					},
				},
			}
			//,"<span class='header-title'>Visual C-Salt</span>":
			,"<img id='vlogo' src='css/logo.png'><span class='header-title'>sRNA Viewer</span>":
			{
				"config":
				{
					"t_attr":
					{
						"id": "header-title"
					}
					,"nolink": true
				}
			}

		}
		,encode_records: {}
		/**
		 * @brief string to md5 string
		 * @param text string
		 * @return md5 string
		 */
		/// !encode
		,encode : function(text)
		{
			var md5 = $.md5(text);
			if(!this.encode_records[md5])
				this.encode_records[md5] = text;
			return md5;
		}
		,get_menu_config_nice: function(config)
		{
			var nice_config = {};
			if(config)
				this.merge_config(nice_config, config);
			return nice_config;
		}
		,set_dom_config: function(jItem, config, title)
		{
			if(!config)
				return;
			if(title != undefined)
			{
				/// @brief attr
				if(config.t_attr)
				{
					for (var key in config.t_attr)
					{
						jItem.attr(key, config.t_attr[key]);
					}
				}
				/// @brief bind action
				if(config.t_bind)
				{
					for (var key in config.t_bind)
					{
						jItem.bind(key, config.t_bind[key]);
					}
				}
				/// @brief unbind action
				if(config.t_unbind)
				{
					for (var key in config.t_unbind)
					{
						jItem.unbind(key);
					}
				}
				/// @brief addClass
				if(config.t_addClass)
				{
					for (var key in config.t_addClass)
					{
						jItem.addClass(config.t_addClass[key]);
					}
				}
				/// @brief removeClass
				if(config.t_removeClass)
				{
					for (var key in config.t_removeClass)
					{
						jItem.addClass(config.t_removeClass[key]);
					}
				}
				/// @brief data
				if(config.t_data)
				{
					for (var key in config.t_data)
					{
						jItem.data(key, config.t_data[key]);
					}
				}
			}
			else
			{
				/// @brief attr
				if(config.attr)
				{
					for (var key in config.attr)
					{
						jItem.attr(key, config.attr[key]);
					}
				}
				/// @brief bind action
				if(config.bind)
				{
					for (var key in config.bind)
					{
						jItem.bind(key, config.bind[key]);
					}
				}
				/// @brief unbind action
				if(config.unbind)
				{
					for (var key in config.unbind)
					{
						jItem.unbind(key);
					}
				}
				/// @brief addClass
				if(config.addClass)
				{
					for (var key in config.addClass)
					{
						jItem.addClass(config.addClass[key]);
					}
				}
				/// @brief removeClass
				if(config.removeClass)
				{
					for (var key in config.removeClass)
					{
						jItem.addClass(config.removeClass[key]);
					}
				}
				/// @brief data
				if(config.data)
				{
					for (var key in config.data)
					{
						jItem.data(key, config.data[key]);
					}
				}
			}
		}
		,merge_config : function(configA, configB)
		{
			for(var key in configB)
			{
				if(typeof(configA[key]) == "object" || typeof(configA[key]) == "array")
				{
					this.merge_config(configA[key], configB[key]);
				}
				else
				{
					configA[key] = configB[key];
				}
			}
		}
		,_create_menu: function(jItem, items)
		{
			if(! items)
				return;
			if(! items.config)
				return;

			this.set_dom_config(jItem, items.config);
			
			switch(items.config.child_type)
			{
				case "tabs":
					this._create_menu_tabs(jItem, items);
					break;
				case "dialog":
					this._create_menu_dialog(jItem, items);
					break;
				case "accordion":
					this._create_menu_accordion(jItem, items);
					break;
				case "section":
					this._create_menu_section(jItem, items);
					break;
				case "selectable":
					this._create_menu_selectable(jItem, items);
					break;
				case "menu":
					this._create_menu_menu(jItem, items);
					break;
				case "button":
					this._create_menu_button(jItem, items);
					break;
				case "select":
					this._create_menu_select(jItem, items);
					break;
			}
		}
		,_create_menu_tabs: function(jItem, items)
		{
			var jMenu_tabs_main = jItem.append("<div class='tabs'></div>").children().last();
			var jMenu_tabs = jMenu_tabs_main.append("<ul></ul>").children().last();
			
			for(var name in items)
			{
				if(name == "config") continue;
				//copy config
				child_config = this.get_menu_config_nice(items[name].config);
				
				if(!child_config.attr)
					child_config.attr = {};
				if(!(child_config.attr && child_config.attr.id))
					child_config.attr.id = "tabs_id_"+ this.encode(name);
				
				var tmp_pid_pclass = "";
				/// @brief menu tabs
				if(child_config.attr && child_config.attr.pid)
					tmp_pid_pclass += " id='"+child_config.attr.pid+"'";
				if(child_config.attr && child_config.attr.pclass)
					tmp_pid_pclass += " class='"+child_config.attr.pclass+"'";
					
				var jMenu_tabs_child = jMenu_tabs.append("<li></li>").children().last();
				
				// for title action and attr
				this.set_dom_config(jMenu_tabs_child, items[name].config, true);
				
				if(!child_config.nolink)
					var jMenu_tabs_child_content = jMenu_tabs_child.append("<a href='#"+ child_config.attr.id +"' >"+ name +"</a>").children().last();
				else
					var jMenu_tabs_child_content = jMenu_tabs_child.append(name).children().last();
				
				/// @brief menu content
				var jMenu_content = jMenu_tabs_main.append("<div></div>").children().last();
				
				this._create_menu(jMenu_content, items[name]);

			}
			jMenu_tabs_main.tabs();
			
		}
		,_create_menu_dialog: function(jItem, items)
		{
			var thisA = this;
			for(var name in items)
			{
				if(name == "config") continue;
				
				/// @brief menu dialog button
				var jMenu_button = jItem.append("<div class='jMenu_dialog_button' >"+name+"</div>").children().last();
				jMenu_button.button();
				
				// for title action and attr
				this.set_dom_config(jMenu_button, items[name].config, true);
				
				var jMenu_content = jItem.append("<div></div>").children().last();
				jMenu_content.hide()
				
				this._create_menu(jMenu_content, items[name]);
			}
			if( $("#jMenu_dialog").length == 0 )
			{
				jItem.append("<div id='jMenu_dialog'></div>").children().last().hide();
			}
			
			// 如果使用者點擊 dialog外面，dialog 自動消失
			var document_click = function(event){
				var dialog = $("#jMenu_dialog");
				//console.log(dialog.data("display_type"));
				if(dialog.data("display_type") == "opened")
				{
					if( ! thisA.dialog_auto_hide )
						return;
					
					var event_parant_dialog = $(event.target).parents("#jMenu_dialog");
					var is_event_parant_dialog = (event_parant_dialog.context.id == "jMenu_dialog") || (event_parant_dialog.length > 0);
					
					// 點擊事件在 dialog 內，不消失
					if(is_event_parant_dialog)
						return;
					
					// 特殊事件，當點擊在某特定物件時，不關閉 dialog
					for(var key in thisA.dialog_dont_close_obj)
					{
						var obj_id = key;
						var obj_limit = thisA.dialog_dont_close_obj[key];
						
						var obj_parent = $(event.target).parents("#"+obj_id)
						var is_event_parant_dont_close_obj = (obj_parent.context.id == obj_id) || (obj_parent.length > 0);
						
						// 還要檢查limit要和 title一樣 (dialog 一樣)，才不會關閉，limit == ""，就不檢查
						var now_title_name = dialog.prev().find(".ui-dialog-title").html();
						//console.log(obj_limit, now_title_name);
						
						if(is_event_parant_dont_close_obj && (now_title_name == obj_limit || obj_limit==""))
						{
							return;
						}
					}
					//console.log("dialog close")
					dialog.data("display_type", "closed");
					dialog.dialog("destroy");
					//dialog.dialog("close");
					$(document).unbind("click");
					$("#jMenu_dialog").html("");				
				}
				else
				{
					//console.log("dialog opened")
					dialog.data("display_type", "opened");
				}
			};
			
			jItem.find(".jMenu_dialog_button").bind("click", function(){
				if($(this).data("disable_dialog"))
					return;
				
				$("#jMenu_dialog").html("");
				
				$("#jMenu_dialog").attr("title", $(this).find("span").html());
				thisA.remove_menu(jItem);
				
				var tmp = $(".jMenu_dialog_button").next();
				thisA.remove_menu(tmp);

				$(this).next().clone(true).appendTo("#jMenu_dialog");
				
				$("#jMenu_dialog")
				//.html($(this).next().html())
				.dialog({
					open: function(){
						//console.log("opening")
						$(this).children().show();
						$(this).find(".tabs").tabs();
						$(this).find(".menu").menu();
						$(this).find(".accordion").accordion({"heightStyle":"content"});
						$(this).find(".selectable").selectable();
						
						$(document).unbind("click").bind("click", function(e){
							document_click(e);
						});
					}
					,minWidth: 500
					,minHeight: 500
					,close: function() {}
					,focus: function(event, ui) {
						cs.global_focus("dialog");
					}
				})
				.parent()
				.bind("mouseover", function(){$(this).data("mouse_type", "in");})
				.bind("mouseout", function(){$(this).data("mouse_type", "out");})
				.css({ zIndex: 110 })
				.position({
					my: "left top",
		            at: "left bottom",
		            of: this
				});
				
			});
		}
		,_create_menu_accordion: function(jItem, items)
		{
			var jMenu_main = jItem.append("<div class='accordion'></div>").children().last();
			for(var name in items)
			{
				if(name == "config") continue;
				/// @brief menu tabs
				var jMenu_tabs = jMenu_main.append("<h3>"+name+"</h3>").children().last();
				
				// for title action and attr
				this.set_dom_config(jMenu_tabs, items[name].config, true);
				
				/// @brief menu content
				var jMenu_content = jMenu_main.append("<div></div>").children().last();
				this._create_menu(jMenu_content, items[name]);

			}
			jMenu_main.accordion();				
		}
		,_create_menu_section: function(jItem, items)
		{
			var jMenu_main = jItem.append("<div class='section'></div>").children().last();

			for(var name in items)
			{
				if(name == "config") continue;
				
				var jMenu_section = jMenu_main.append("<div></div>").children().last();
				
				// for title action and attr
				this.set_dom_config(jMenu_section, items[name].config, true);
				
				/// @brief menu tabs
				if(items[name].config && items[name].config.title != undefined)
					var jMenu_tabs = jMenu_section.append("<div class='section_title' >"+ items[name].config.title +"</div>").children().last();
				else
					var jMenu_tabs = jMenu_section.append("<div class='section_title' >"+name+"</div>").children().last();
				
				/// @brief menu content
				var jMenu_content = jMenu_section.append("<div class='section_content'></div>").children().last();
				
				if(typeof items[name] == "string")
					jMenu_content.html(items[name]);
				if(items[name].config && items[name].config.content)
					jMenu_content.html(items[name].config.content);
				
				this._create_menu(jMenu_content, items[name]);
			}

		}
		,_create_menu_menu: function(jItem, items)
		{
			var jMenu_main = jItem.append("<ul class='menu'></ul>").children().last();
			for(var name in items)
			{
				if(name == "config") continue;
				/// @brief menu tabs
				var jMenu_tabs = jMenu_main.append("<li></li>").children().last();
				
				// for title action and attr
				this.set_dom_config(jMenu_tabs, items[name].config, true);
				
				jMenu_tabs.append("<a href='#' >"+ name +"</a>").children().last();
				this._create_menu(jMenu_tabs, items[name]);
			}
			jMenu_main.menu();
		}
		,_create_menu_selectable: function(jItem, items)
		{
			var jMenu_main = jItem.append("<ul class='selectable'></ul>").children().last();
			for(var name in items)
			{
				if(name == "config") continue;
				/// @brief menu tabs
				var jMenu_tabs = jMenu_main.append("<li>"+name+"</li>").children().last();
				this._create_menu(jMenu_tabs, items[name]);
			}
			jMenu_main.selectable();
		}
		,_create_menu_select: function(jItem, items)
		{
			var jMenu_main = jItem.append("<select class='select'></select>").children().last();
			for(var name in items)
			{
				if(name == "config") continue;
				/// @brief menu select
				// for title action and attr
				this.set_dom_config(jMenu_main, items[name].config, true);
				var jMenu_tabs = {};
				if(typeof items[name] == "string")
					jMenu_tabs = jMenu_main.append("<option value='"+items[name]+"' >"+name+"</option>").children().last();
				else
					jMenu_tabs = jMenu_main.append("<option>"+name+"</option>").children().last();
				this._create_menu(jMenu_tabs, items[name]);
			}
			//jMenu_main.selectable();
		}
		,_create_menu_button: function(jItem, items)
		{
			var jMenu_main = jItem.append("<div class='buttons'></div>").children().last();
			for(var name in items)
			{
				if(name == "config") continue;
				/// @brief menu tabs
				var jMenu_tabs = jMenu_main.append("<div>"+name+"</div>").children().last();
				this._create_menu(jMenu_tabs, items[name]);
				jMenu_tabs.button();
			}
		}
		,reflash_dialog: function()
		{
			this.jItem.find("jMenu_dialog");
		}
		,remove_menu: function(jItem)
		{
			jItem.find(".accordion").each(function()
			{
				if(!$(this).data("removed"))
					$(this).accordion("destroy").data("removed", true);
			});
			jItem.find(".tabs").each(function()
			{
				if(!$(this).data("removed"))
					$(this).tabs("destroy").data("removed", true);
			});
			jItem.find(".menu").each(function()
			{
				if(!$(this).data("removed"))
					$(this).menu("destroy").data("removed", true);
			});
			jItem.find(".selectable").each(function()
			{
				if(!$(this).data("removed"))
					$(this).selectable("destroy").data("removed", true);
			});
		}
	});
});






