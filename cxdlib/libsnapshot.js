define(["dojo/_base/declare", "dojo/_base/lang", "cxdlib/libsimplelist"], function(declare, lang, SimpleList) {
	return declare("Snapshot", null, {
		file_key: null,
		snapshot_list: null, // A refernce to SimpleList object
		snapshots: {},
		cs: null,

		constructor: function(cs, div_id, file_key)
		{
			this.cs = cs;
			this.snapshot_list = new SimpleList(div_id);

			this.file_key = file_key;
			if (this.file_key)
				this.file_key = file_key.trim();

			var thisA = this;
			this.snapshot_list.controls.touch = function(key)
			{
				thisA.touch_snapshot(key);
			};

			this.snapshot_list.controls.remove = function(key)
			{
				thisA.remove_snapshot(key);
			};

			this.snapshot_list.controls.modify = function(key, new_text)
			{
				thisA.modify_snapshot(key, new_text);
			};

			if (file_key)
			{
				this.remote_load();
			}
		},
		touch_snapshot: function(key)
		{
			var sp = this.snapshots[key].snapshot;
			this.cs.set_snapshot(sp);
		},
		remove_snapshot: function(key)
		{
			delete this.snapshots[key];

			if (this.file_key)
			{
				this.remote_save();
			}
		},
		modify_snapshot: function(key, new_text)
		{
			this.snapshots[key].name = new_text;

			if (this.file_key)
			{
				this.remote_save();
			}
		},
		add: function(name, snapshot, key)
		{
			if (key != undefined)
			{
				// Tell the snapshot to update the lastest key,
				// because there are already some history of snapshots.
				this.snapshot_list.update_key(key);
			}

			key = this.snapshot_list.add(name);
			this.snapshots[key] = { "name": name, "snapshot" : snapshot };

			if (this.file_key)
			{
				this.remote_save();
			}
		},
		update: function(name, snapshot, key)
		{
			// This function won't create new item of snapshot_list if there is a exist snapshot.
			// ! Some parts of this are similar to add(...).

			if (!this.snapshots[key])
			{
				console.log("Update a not-exist snapshot");
				this.snapshot_list.add(name, key);
			}

			this.snapshots[key] = { "name": name, "snapshot": snapshot };
			this.snapshot_list.rename(key, name);

			if (this.file_key)
			{
				this.remote_save();
			}
		},
		readonly: function(key)
		{
			this.snapshot_list.readonly(key);
		},
		remote_load: function()
		{
			var callback = function(data)
			{
				if (data)
				{
					this.set_snapshot(data);
				}
			};

			this.cs.fileio.file_load("snapshots/" + this.file_key, lang.hitch(this, callback));
		},
		remote_save: function()
		{
			var filename = "snapshots/" + this.file_key;
			var content = this.get_snapshot();

			this.cs.fileio.file_save(filename, content);
		},
		get_snapshot: function()
		{
			return JSON.stringify(this.snapshots);
		},
		set_snapshot: function(snapshot)
		{
			snapshot = JSON.parse(snapshot);

			for (var key in snapshot)
			{
				this.add(snapshot[key].name, snapshot[key].snapshot, key);
			}

			return true;
		},
		reset: function()
		{
			this.snapshot_list.reset();
			this.snapshots = {};
		}
	});
});
