define(["dojo/_base/declare", "dojo/_base/lang"], function(declare, lang) {
	return declare("JobController", null, {
		list: null,

		constructor: function()
		{
			this.list = cs.job_list;
		},
		add_job: function(subject, file, description, d3_gtype)
		{
			var job = new Job(subject, file, description, d3_gtype);
			job.init();
			cs.jobq.add_job(job, job.loader);
		},
		get_snapshot: function()
		{
			var snapshot = {};

			for (var job in this.list)
			{
				if (this.list[job].state == "show" || this.list[job].state == "ready")
					snapshot[job] = this.list[job].get_snapshot();
			}

			return JSON.stringify(snapshot);
		},
		set_snapshot: function(snapshot)
		{
			this.reset();

			snapshot = JSON.parse(snapshot);

			for (var job in snapshot)
			{
				var j = new Job(cs);
				j.set_snapshot(snapshot[job]);
				j.init(JSON.parse(snapshot[job]));
			}

			cs.jobq.set_open();

			return true;
		},
		reset: function()
		{
			var is_not_refresh = true;

			// remove all jobs
			for (var job in this.list)
			{
				this.list[job].remove_job(is_not_refresh);
			}

			cs.jobq.reset();
		}
	});
});
