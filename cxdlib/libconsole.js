define(["dojo/_base/declare", "dojo/io-query", "cxdlib/libd2iterator.js", "cxdlib/libsnapshot"], function(declare, ioQuery, D2Iterator, Snapshot) {
	return declare("Console", null, {
		eg: null,
		d3: null,
		jb: null,
		fmt: null,
		dialogs: {},
		_sel_graph: "#sel_graph",
		_sel_analysis: "#sel_analysis",
		_zIndex_base: 101,
		_rand_range: 65536,
		_pz_bgcolor: "rgb(220, 255, 220)",
		_pz_ytitle_bgcolor: "rgb(180, 180, 180)",
		global_config: {},
		csaltdock: "bin/csaltdock-visualizer.php",
		job_list: {},
		_job_container: "#jobs",
		_job_serial_number: 1,
		snapshots: {}, // PREDCATED
		snapshot_list: null, // PREDCATED
		_interval_snapshot_init: 10 * 1000,
		_interval_snapshot: 10 * 60 * 1000,
		_tm_snapshot: null,
		genome_info: {},
		
		constructor: function() {
			console.log("Console Created!");
			this.init();
		},
		init: function(){
			var thisA = this;
			
			///@brief query string
			this.global_config.query_params = ioQuery.queryToObject( window.location.search.slice(1) );
			
			///@brief get current genome
			if (this.global_config.query_params.data)
				this.global_config.current_genome = this.global_config.query_params.data.split("/").pop();

			///@brief get genome list
			//this.global_config.genome_list
			this._get_genome_list(function(d) {
				thisA.global_config.genome_list = JSON.parse(d).datasets;
			});
			
			console.log(this.global_config);
			
			this.user = this.global_config.query_params.user;
			this.account_init(this.user);
			this.account_control();
		},
		update_global_config_query_params: function()
		{
			this.global_config.query_params = ioQuery.queryToObject( window.location.search.slice(1) );
		},
		_get_genome_list: function(callback)
		{
			$.post(this.csaltdock ,{"cmd":"get_jbrowse_config"}, function(d){
				callback(d);
			})
			.fail(function() {
				console.log( "error" );
			})
			.always(function(){
			});;	
		},
		get_job_serial_number: function()
		{
			return this._job_serial_number++;
		},
		layouts: function(type){
			var content_padding = 2;
			var content_height = $(window).height() - $("#menu").height() - content_padding;
			var content_width = $(window).width() - content_padding*2;

			var get_conf = function(h, w, position)
			{
				var option = {
					"height": content_height * h
					,"width": content_width * w
					,"position": position
				}
				return option; 
			}
			switch(type)
			{
				case "jb_h_sc_d3":
					this.dialogs.jb.dialog( get_conf(0.5, 1, {my : "left top", at : "left bottom", of : $("#menu")}));
					this.dialogs.sc.dialog( get_conf(0.5, 0.5, {my : "left top", at : "left bottom", of : $("#jBrowseContainer")}));
					this.dialogs.d3.dialog( get_conf(0.5, 0.5, {my : "left top", at : "right bottom", of : $("#jBrowseContainer")}));
					break;
				case "jb_v_sc_d3":
					this.dialogs.jb.dialog( get_conf(1, 0.5, {my : "left top", at : "left bottom", of : $("#menu")}));
					this.dialogs.sc.dialog( get_conf(0.5, 0.5, {my : "left top", at : "right bottom", of : $("#menu")}));
					this.dialogs.d3.dialog( get_conf(0.5, 0.5, {my : "left top", at : "left bottom", of : $("#SocialCalcContainer")}));
					break;
				case "sc_v_d3":
					this.dialogs.jb.dialog( "close" );
					this.dialogs.sc.dialog( get_conf(1, 0.5, {my : "left top", at : "left bottom", of : $("#menu")}));
					this.dialogs.d3.dialog( get_conf(1, 0.5, {my : "left top", at : "right bottom", of : $("#menu")}));
					break;
				case "jb_h_sc":
					this.dialogs.jb.dialog( get_conf(0.5, 1, {my : "left top", at : "left bottom", of : $("#menu")}) );
					this.dialogs.sc.dialog( get_conf(0.5, 1, {my : "left top", at : "left bottom", of : $("#jBrowseContainer")}));
					this.dialogs.d3.dialog( "close" );
					break;
				case "jb_h_d3":
					this.dialogs.jb.dialog( get_conf(0.5, 1, {my : "left top", at : "left bottom", of : $("#menu")}) );
					this.dialogs.sc.dialog( "close" );
					this.dialogs.d3.dialog( get_conf(0.5, 1, {my : "left top", at : "left bottom", of : $("#jBrowseContainer")}));
					break;
				case "sc_h_d3":
					this.dialogs.jb.dialog( "close" );
					this.dialogs.sc.dialog( get_conf(0.5, 1, {my : "left top", at : "left bottom", of : $("#menu")}));
					this.dialogs.d3.dialog( get_conf(0.5, 1, {my : "left top", at : "left bottom", of : $("#SocialCalcContainer")}));
					break;
				case "jb_a":
					this.dialogs.jb.dialog( get_conf(1, 1, {my : "left top", at : "left bottom", of : $("#menu")}));
					this.dialogs.sc.dialog( "close" );
					this.dialogs.d3.dialog( "close" );
					break;
				case "sc_a":
					this.dialogs.jb.dialog( "close" );
					this.dialogs.sc.dialog( get_conf(1, 1, {my : "left top", at : "left bottom", of : $("#menu")}));
					this.dialogs.d3.dialog( "close" );
					break;
				case "d3_a":
					this.dialogs.jb.dialog( "close" );
					this.dialogs.sc.dialog( "close" );
					this.dialogs.d3.dialog( get_conf(1, 1, {my : "left top", at : "right bottom", of : $("#menu")}));
					break;
			}
			///@brief 讓window內容物也真的 resize
			this.window_inner_resize();
		},
		window_title_colorize: function() {
			this.dialogs.jb.prev().removeClass("ui-widget-header").addClass("jb-ui-widget-header");
			this.dialogs.sc.prev().removeClass("ui-widget-header").addClass("sc-ui-widget-header");
			this.dialogs.d3.prev().removeClass("ui-widget-header").addClass("d3-ui-widget-header");
		},
		window_inner_resize: function() {
			///@biref soclalcalc 內容真正 resize
			if (this.sc)
			{
				this.sc.resize();
			}
			
			if(this.jb && this.jb.jbrowse && this.jb.jbrowse.view && _vp)
			{
				///@brief jbrowse 內容真正 resize
				_vp.emit("resize");
				//$("#jBrowseContainer").css("height", "-=10");
				///@breif jbrowse 因為 dialog 切換的時候，會造成未知的 jbrowse 變成全白色，重新設定jbrowse位置可以解決（重劃內容）
				this.jb.jbrowse.view.setPosition(this.jb.jbrowse.view.getPosition());
			}
		},
		ez_windowize: function() {
			var thisCS = this;
			
			var content_padding = 2;
			var content_height = $(window).height() - $("#menu").height() - content_padding;
			var content_width = $(window).width() - content_padding*2;
			
			var jbptr = $("#jBrowseContainer");
			jbptr.parent().css("overflow", "hidden");
			op = $("#jBrowseContainer").removeClass("top").dialog({
            	height : (content_height * 0.5),
				width : (content_width),
				position : {my : "left top", at : "left bottom", of : $("#menu")},
				focus: function(event, ui) {
					thisCS.global_focus("JB");
					thisCS.window_inner_resize();
				},
				resizeStop: function(event, ui){
					thisCS.window_inner_resize();
				}
			});
			this.dialogs.jb = jbptr;
			
			this.dialogs.sc = 
			$("#SocialCalcContainer").css("overflow", "hidden").removeClass().dialog({
			  height : (content_height * 0.5),
			  width : (content_width * 0.5),
			  position : { my : "left top",  at : "left bottom", of : $("#jBrowseContainer") },
			  focus: function(event, ui) {
				  thisCS.global_focus("SC");
				  thisCS.window_inner_resize();
			  },
			  resizeStop: function(event, ui) {
				  thisCS.window_inner_resize();
			  }
			});
			//this.dialogs.sc = this.dialogs.sc.parent();
	  		this.dialogs.sc = this.dialogs.sc;

			this.dialogs.d3 = 
			$("#D3Container").removeClass().dialog({
			  height : (content_height * 0.5),
			  width : (content_width * 0.5),
			  position : {my : "left top", 
			  			at : "right bottom", 
			  			of : $("#jBrowseContainer"),
			  			//collision: "flip",
			  },
			  focus: function(event, ui) {
				  thisCS.global_focus("D3");
				  thisCS.window_inner_resize();
			  },
			  resizeStop: function(event, ui) {
				  thisCS.window_inner_resize();
			  }
			});
			this.window_title_colorize();
		},
		all_windowize: function(id, conf) {
			//XXX: When zIndex of main view is too high, the menu or popup windows cannot be seen
			//XXX: _vp is a global variable, kind of backdoor to send `resize` event directly.
			var option = {
				zIndex: 55556789,
			};
			var jbptr = $("#jBrowseContainer");
			jbptr.parent().css("overflow", "hidden");
			op = $("#jBrowseContainer").removeClass("top").dialog({
                height : ($(window).height() * 0.55),
                width : "100%",
                position : {my : "left top", at : "left bottom", of : $("#sel_analysis")}
			}).parent().resize(function() {
				_vp.emit("resize");
				$("#jBrowseContainer").css("height", "-=10");
			});

			$("#SocialCalcContainer").dialog();
			/*
			$("#SocialCalcContainer").removeClass("bot").dialog({
				height : ($(window).height() * 0.45),
				width : "65%",
				position : {my : "left top", 
							at : "left bottom", 
							of : $(jbptr),
							collision: "flip",
						},
				focus: function(){
					cs.jb.redraw();
				}
			}).parent().resize(function() {
				//_vp.emit("resize");
				$("#gridContainer").css("width", "+=45");
			});
			*/
			
			$("#D3Container").removeClass("bottom").dialog({
				height : ($(window).height() * 0.45),
				width : "35%",
				resizable : true,
				position : {my : "left top", at : "right top", of : $("#gridContainer")},
				focus: function(){
					cs.jb.redraw();
				}
			}).parent().resize(function() {
				//_vp.emit("resize");
			});
			// FIXME: sort_item can not be moved out when dialog content having overflow-auto

			cs.jb.redraw();
		},
		windowize: function(id, conf, title) {
			var thisA = this;
			console.log(title);
			if(!title) title = id;
			$("#"+id).removeClass().addClass("empty").dialog({
				height : "600",
				width : "1000",
				resizable : true,
				position : {my : "center", at : "center", of : window},
				collision: "flip",
				autoOpen: false,
				title: title,
				focus: function(event, ui) {
					thisA.window_inner_resize();
				},
				resizeStop: function(event, ui){
					thisA.window_inner_resize();
				}
			}).css({
				"float": "none",
			}).parent().css({
				"zIndex": this._zIndex_base,
			});

			if (conf)
				$("#" + id).dialog(conf);
			$("#" + id).dialog("open");
		},
		eg2d3_assign: function(id, graph_type, sheet_info) {
			// FIXME: The selection is still be selected by cotaining the row-0 and col-0.
			//		  Next, change to accept select the mid sheet and link row-0 and col-0.
			var sel_col = {};
			var range;
			var sheet = sheet_info.sheet;

			// Read range from parameter
			sel_col.start = sheet_info.cfrom._cellRef.col;
			sel_col.end = sheet_info.cto._cellRef.col;
			range = sheet.getRange(sheet_info.range);
			console.log(range);

			// get editgrid data XXX: weird that cursour.getRange() has a bug in grid.js
			this._eg2d3(range, id, graph_type, sel_col);
		},
		_sc2d3: function(range, id, graph_type, sel_col) {
			var data = cs.sc.get_range_text(0, 0, sheet_info.row, sheet_info.col);
			//var data = cs.sc_getRangeText(0, 0, sheet_info.row, sheet_info.col);
			data = this.fmt.array2jjson(data);
			console.log(data);
			
			// new d3
			var d3_obj = new D3({"cb_mouseover": null, "cb_mouseout": null});
			// set data and draw
			d3_obj.draw(graph_type, "#"+id+" svg", data);
		},
		d3_copy_out: function() {
			var g_id = "graph_" + this.rand();
			var source_id = "D3ContainerDrawer";
			console.log("d3_copy_out");
			// add div element
			// FIXME: Next, Dont let the new window depend on D3Container, so instead of body.
			$("#D3Container").append(this.fmt.graph_html(g_id));
			// windowized
			this.windowize(g_id);
			$("#"+g_id).html($("#"+source_id).html());
			$("#"+source_id).children("svg").empty();
		},
		rand: function() {
			return new Date().getTime() % this._rand_range;;
		},
		graph_join_cursor: function() {
			console.log("graph_join_cursor");
			var g_id = "graph_" + this.rand();
			// add div element
			var conf = {
				open: function(event, obj){
					cs.d3.dialog_title_embed(g_id, event, obj);
				},
				close: cs.d3.remove_dialog,
				//close: cs.d3.move_to_museum
			};
			$("#D3Container").append(this.fmt.graph_html(g_id));
			// windowized
			this.windowize(g_id, conf);
			// detect select option
			var graph_type = $(this._sel_graph).val();
			var sheet_info = { sheet : cs.eg.get_sheet() };
			this.eg2d3_cursor(g_id, graph_type);
		},
		graph_join_assign: function(sheet_info) {
			var g_id = "D3ContainerDrawer";
			$("#"+g_id).children("svg").empty();
			var graph_type = $("#sel_assign").val();
			this.eg2d3_assign(g_id, graph_type, sheet_info);
		},
		// FIXME: Normalize the `sheet_info` object next time
		graph_join: function(sheet_info, graph_type) {
			var g_id = "D3ContainerDrawer";
			$("#"+g_id).children("svg").empty();

			var sel_col = {};
			var range;
			var sheet = cs.eg.book.sheet;

			// Read range from cursor
			range = sheet.getRange([0, 0, sheet_info.row, sheet_info.col]);

			// get editgrid data XXX: weird that cursour.getRange() has a bug in grid.js
			this._eg2d3(range, g_id, graph_type, sel_col);
		},
		get_sheet_info: function(data_csv)
		{
			var data_arr = this.fmt.csv2array(data_csv);
			var sheet_info = {row: data_arr.length, col: data_arr[0].length, data_arr: data_arr, data_csv: data_csv};

			return sheet_info;
		},
		graph_join_sc: function(sheet_info, graph_type, job_id) {
			var g_id = "graph_" + job_id;

			var conf = {
				open: function(event, obj){
					cs.d3.dialog_title_embed(g_id, event, obj);
				},
				close: cs.d3.remove_dialog,
				resizeStop: cs.d3.auto_calculate_all_d3_legend_size
			};

			$("#D3Container").append(this.fmt.graph_html(g_id));
			this.windowize(g_id, conf, this.job_list[job_id].get_subject());

			var data = sheet_info.data_arr;
			data = cs.sc.filter_plot_zone(data, cs.sc.sheet_tabs[job_id].plot_zone);
			data = this.fmt.array2jjson(data);

			function hl_over(color, job_id) {
				return function(row, col) {

					var graph_selected = cs.d3.d3_graph_objs[cs.d3.current_selected].config;
					if (this != graph_selected)
					{
						return;
					}

					cs.sc.local_save(cs.sc.current_sheet_tab);

					var sel_col = cs.sc.sheet_tabs[job_id].plot_zone.xtitle.horizontal.length + 1;
					cs.sc.set_bgcolor(0, row + 2, 0 + sel_col, row + 2, color);
					cs.sc.set_bgcolor(col + 2, row + 2, col + 2, row + 2, 'rgb(255, 255, 0)');
				}
			}

			function hl_out(color, job_id) {
				return function(row, col) {

					var graph_selected = cs.d3.d3_graph_objs[cs.d3.current_selected].config;
					if (this != graph_selected)
					{
						return;
					}

					cs.sc.local_load(cs.sc.current_sheet_tab);
				}
			}

			var d3_obj = new D3({
				"cb_mouseover": hl_over('rgb(255, 0, 0)', job_id), 
				"cb_mouseout": hl_out('rgb(255, 255, 255)', job_id),
				"x_axis_right_click": this.d3.d3_graph_x_axis_right_click,
				"legend_right_click": this.d3.d3_graph_legend_right_click,
			});
			this.d3.d3_graph_objs[g_id] = d3_obj;

			d3_obj.draw(graph_type, "#" + g_id + " svg", data);
			cs.d3.auto_calculate_d3_legend_size(g_id);
		},
		async_exec: function(bg, data, done) {
			$.ajax({
				url: bg,
				type: "post",
				data: data,
				async: true
			}).done(done);
		},
		sync_exec: function(bg, data, done) {
			$.ajax({
				url: bg,
				type: "post",
				data: data,
				async: false
			}).done(done);
		},
		restore_from_museum: function(event, obj)
		{
			console.log(event, obj);
		},
		global_focus: function(target)
		{
			//console.log(target);
			if (target == "SC") {
				SocialCalc.Vis.focus();
			} else {
				SocialCalc.Vis.blur();
			}
		},
		sc_update_graph_recalc: function()
		{
			cs.sc.update_graph();

			console.log("graph updated");
		},
		reset_job_list: function()
		{
			for (job in this.job_list)
			{
				this.job_list[job].remover();
			}
		},
		get_snapshot: function()
		{
			var snapshot = {};

			snapshot.cs = {};
			snapshot.cs.museum = [];

			for (var job in this.job_list)
			{
				if (cs.d3.is_in_museum(job))
				{
					snapshot.cs.museum.push(job);
				}
			}
			

			snapshot.jobs = this.jobs.get_snapshot();
			snapshot.sc = this.sc.get_snapshot();
			snapshot.d3	= this.d3.get_snapshot();
			snapshot.jb = this.jb.get_snapshot();

			return JSON.stringify(snapshot);
		},
		set_snapshot: function(snapshot)
		{
			snapshot = JSON.parse(snapshot);
			
			this.jobs.set_snapshot(snapshot.jobs);
			this.sc.set_snapshot(snapshot.sc);
			this.d3.set_snapshot(snapshot.d3);
			this.jb.set_snapshot(snapshot.jb);

			for (var job in snapshot.cs.museum)
			{
				cs.d3.move_to_museum_by_job_id(snapshot.cs.museum[job]);
				$("#graph_" + snapshot.cs.museum[job]).dialog("close");
			}
		},
		save_snapshot_btn: function(e)
		{
			var name = $(".in_sp_name_c").last().val();
			if (name == "") return;

			var snapshot = this.get_snapshot();
			
			this.sp.add(name, snapshot);

			// To trigger the dialog close
			$(e.target).remove();
		},
		account_init: function(id)
		{
			var html = '\
				<div class="account_info_title">Account: </div><input id="ac_id" class="account_item" type="text" value="' + id + '" readonly /><br /> \
				<div class="account_info_title">Current password: </div><input id="ac_cpw" class="account_item" type="password"/><br /> \
				<div class="account_info_title">New password: </div><input id="ac_npw" class="account_item" type="password" /><br /> \
				<div class="account_info_title">Confirm new password: </div><input id="ac_cnpw" class="account_item" type="password" /><br /> \
				<br /> \
				<div id="ac_msg"></div> \
				<button id="ac_ok">OK</button> \
			';

			$(".account_ctx").append(html);
		},
		account_control: function()
		{
			$target = $(".account_ctx").last();
			$target.children("#ac_ok").click(function()
			{
				$target = $(this).parents(".account_ctx");

				var data = {};
				data["id"] = $target.find("#ac_id").val();
				data["cpw"] = $target.find("#ac_cpw").val(); // current password
				data["npw"] = $target.find("#ac_npw").val(); // new password
				data["cnpw"] = $target.find("#ac_cnpw").val(); // confirm new password

				var done = function(msg)
				{
					$target.find("#ac_msg").html(msg);
				};

				cs.async_exec("account.php", data, done);
			});
		},
		schedule_snapshot_routine_DEPRECATED: function()
		{
			var thisA = this;

			setTimeout(function() { thisA.save_auto_snapshot(); thisA.schedule_snapshot_routine(); }, thisA._interval_snapshot);
		},
		schedule_snapshot_routine: function()
		{
			var thisA = this;

			setTimeout(function() { thisA.save_auto_snapshot_init(); }, thisA._interval_snapshot_init); // Save for first time
			thisA._tm_snapshot = setInterval(function() { thisA.save_auto_snapshot(); }, thisA._interval_snapshot);
		},
		save_auto_snapshot_init: function()
		{
			var display_text = "Auto save. " + (new Date()).toString();

			if (!this.sp.snapshots[0])
			{
				this.sp.add(display_text, this.get_snapshot(), 0);
			}
			// Do nothing if there is snapshot[0] which was saved from last time.
			//else
			//{
			//	this.sp.update(display_text, this.get_snapshot(), 0);
			//}

			this.sp.readonly(0);
		},
		save_auto_snapshot: function()
		{
			var display_text = "Auto save. " + (new Date()).toString();

			this.sp.update(display_text, this.get_snapshot(), 0);
		},
		load_auto_snapshot: function()
		{

		},
		change_genome: function(key)
		{
			clearInterval(this._tm_snapshot);

			// ! Parameter key is for this.genome_info
			this.save_auto_snapshot();
			var url = this.genome_info[key].url;
			document.location.href = url;
		},
		jb_done: function()
		{
			cs.jb.append_db_select();
		},
		sc2jb: function()
		{
			var cell;
			var text;

			if (cs.sc.spreadsheet.editor.range.hasrange)
			{
				return;
			}
			else
			{
				cell = cs.sc.spreadsheet.editor.ecell.coord;
			}

			if (!cs.sc.spreadsheet.sheet.cells[cell])
				return;

			text = cs.sc.spreadsheet.sheet.cells[cell].datavalue;

			cs.jb.jbrowse.nameStore.query({name: text}).then(function(nameMatches){
				if (nameMatches.total != 0)
				{
					cs.jb.jbrowse.navigateTo(text);
					cs.jb.jbrowse.locationBox.setDisplayedValue(text);
				}
				else
				{
					var text_fuzzy = text + "-*";
					cs.jb.jbrowse.nameStore.query({name: text_fuzzy}).then(function(nameMatches2)
					{
						if (nameMatches2.total > 0)
						{
							//text_fuzzy = nameMatches2[0].name;
							//cs.jb.jbrowse.navigateTo(text_fuzzy);
							//cs.jb.jbrowse.locationBox.setValue(text_fuzzy);
							
							cs.jb.jbrowse.goButton.set('disabled',false);
							cs.jb.jbrowse.locationBox._startSearch(text);
						}
						else
						{
							cs.jb.jbrowse.navigateTo(text);
							cs.jb.jbrowse.locationBox.setValue(text);
						}
					});
				}
			});
		}
	});
});
