define(["dojo/_base/declare", "dojo/_base/lang"], function(declare, lang) {
	return declare("D2Iterator", null, {
		row_ : [],
		col_ : [],
		p_r_ : 0,
		p_c_ : 0,

		constructor: function(row, col)
		{
			this.row_ = row;
			this.col_ = col;
		},
		has_next: function()
		{
			return this.p_r_ < this.row_.length && this.p_c_ < this.col_.length;
		},
		next: function()
		{
			var retval = { r : this.row_[this.p_r_], c : this.col_[this.p_c_++] };
			
			if (this.p_c_ >= this.col_.length)
			{
				this.p_c_ = 0;
				this.p_r_ = this.p_r_ + 1;
			}

			return retval;
		}
	});
});
