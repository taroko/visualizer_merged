define(["dojo/_base/declare", "dojo/_base/lang", "cxdlib/libfmt"], function(declare, lang, fmt) {
	return declare("FileIO", null, {

		constructor: function()
		{
			console.log("FileIO");
		},
		// Javascript end, to export a input-context file with download window
		file_export: function(filename, context)
		{
			// for text-based file (csv)
			// Data URIs
			var html = '<a id="hide_a"></a>';
			$("body").append(html);
			var html_id = "#hide_a";
			$(html_id)
				.attr("download", filename)
				.attr("href", "data:text/csv;charset=utf8," + encodeURIComponent(context));

			$(html_id).get(0).click();
			$("body").find(html_id).remove();
		},
		// Javascript end, to import a text-based file with <input type="files"> tag
		file_import: function(id, thisX, after_importing)
		{
			// for text-based file
			var file = $("#" + id)[0].files[0];

			if (file)
			{
				var fr = new FileReader();
				fr.onload = (
					function(ff)
					{
						return function(evt)
						{
							after_importing(thisX, evt.target.result, file);
						};
					}
				)(file);

				fr.readAsText(file);
			}
		},
		// Php end, to load a file
		file_load: function(fn, after_loading)
		{
			cs.sync_exec(
				"fileloader.php",
				{ filename: fn },
				after_loading
			);
		},
		file_save: function(fn, ct, after_saving)
		{
			cs.sync_exec(
				"filesaver.php",
				{ filename: fn, content: ct },
				after_saving
			);
		},
	});
});
