define(["dojo/_base/declare", "cxdlib/libsimplelist"], function(declare, SimpleList) {
	return declare("Format", null, {
		csv2array: function(data) {
			data = data.split("\n");
			// Remove the last element if it's empty
			if (data[data.length - 1] == "") 
				data.pop();

			for (var i = 0; i < data.length; i++) {
				data[i] = data[i].split(",");
				// Trim off the blank
				for (var j = 0; j < data[i].length; j++) {
					data[i][j] = data[i][j].trim();
				}
			}

			return data;
		},
		array2jjson: function(data) {
			var ret = [];
			for (var i = 1; i < data.length; i++) {
				var obj = {};
				for (var j = 0; j < data[0].length; j++) {
					obj[data[0][j]] = data[i][j];
				}
				ret.push(obj);
			}
			return ret;
		},
		csv2jjson: function(data)
		{
			return this.array2jjson(this.csv2array(data));
		},
		make_option_html: function(list, name_type, value_type, def)
		{
			if(!name_type) name_type = "key";
			if(!value_type) value_type = "value";
			var html = "";
			for(var key in list)
			{
				var tmp = {"key":key, "value": list[key]};
				html += "<option value='"+ tmp[value_type] +"' "
				if(tmp[value_type] == def) html += "selected='selected'";
				html += ">";
				html += tmp[name_type];
				html += "</option>";
			}
			return html;
		},
		// FIXME: Can be removed, old version
		job_html: function(id, subject, description) {
			var html = '<div class="jobs"> <h2><a href="#" id="' + id + 's">' + subject + '</a></h2><button id="' + id + '" disabled>Waiting</button> </div>';
			return html;
		},
		job_html2: function(id, subject, description) {
			//var html = '<li class="ui-state-default"><div id=""><b>[' + subject + ']</b><button id="' + id + '" disabled>Waiting</button><button id="' + id + '_del">Delete</button><br/>Description</div></li>';
			var html = '<li class="ui-state-default">';
			html += '<div id="' + id + '">';
			html += '<div id="' + id + '_subject" class="jque_subject">' + subject + '</div>';
			html += '<button class="img_btn" id="' + id + '_status" disabled>Waiting</button>';
			html += '<button class="img_btn" id="' + id + '_copy" disabled ><div class="job_btn_copy" title="Copy"></div></button>';
			html += '<button class="img_btn" id="' + id + '_del"><div class="job_btn_del" title="Delete"></div></button>';
			html += '<button class="img_btn" id="' + id + '_info"><div class="job_btn_info" title="Information"></div></button>';
			html += '<div id="' + id + '_description" class="jque_description">' + description + '</div>';
			html += '</div>';
			html += '</li>';
			return html;
		},
		graph_html: function(id) {
			var html = '<div id="' + id + '"><svg height="100%" width="100%"></svg></div>';
			return html;
		},
		tab_html: function(id, sheet_name)
		{
			var tab_html = '<span id="sh_' + id + '" style="padding: 0px 10px; width: auto; text-align: center; position: relative; top: -2px; height: 19px; line-height: 1.5em;" class="sc_tabs">' + sheet_name + '</div>';
			return tab_html;
		},
		widget: function(type, container_id)
		{
			if (type == "SimpleList")
			{
				return new SimpleList(container_id);
			}
		},
	});
});
