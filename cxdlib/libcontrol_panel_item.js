define(["dojo/_base/declare"], function(declare) {
	return declare("ControlPanelItem", null,
	{
		config: {}
		,parent_id: ""
		,item_id: ""
		,item_type: ""
		,constructor: function(parent_id, item_id, item_type, config)
		{
			if(config) this.config = config;
			if(parent_id) this.parent_id = parent_id;
			if(item_id) this.item_id = item_id;
			if(item_type) this.item_type = item_type;
			
			this.append_item();
			
		}
		,check_make_item: function()
		{
			if(this.parent_id == "") return false;
			if(this.item_id == "") return false;
			if(this.item_type == "") return false;
			
			if(!this.config.attr) this.config.attr = {};
			
			this.config.attr.id = this.item_id;
			this.config.attr.type = this.item_type;
			
			return true;
		}
		,append_item: function()
		{
			if( !this.check_make_item() )
			{
				console.log("make item failur");
				return;
			}
			var jItem = $("#" + this.parent_id).append("<input />").children().last();
			
			for(var key in this.config.attr)
			{
				jItem.attr(key, this.config.attr[key]);
			}
			
			if(this.config.bind)
			{
				for(var key in this.config.bind)
				{
					jItem.bind(key, this.config.bind[key]);
				}
			}
			if(this.config.unbind)
			{
				for(var key in this.config.unbind)
				{
					jItem.unbind(key, this.config.unbind[key]);
				}
			}
		}
	});
});